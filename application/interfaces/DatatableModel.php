<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

interface DatatableModel {
	
	/**
	 * @ return
	 * 		Expressions / Columns to append to the select created by the Datatable library.
	 * 		Associative array where the key is the sql alias and the value is the sql expression
	 */
	public function appendToSelectStr();
	
    /**
     * @return
     *      String table name to select from
     */
    public function fromTableStr();
    
    /**
     * @return
     *     Associative array of joins.  Return NULL or empty array  when not joining
     */
    public function joinArray();
    
	/**
	 * 
	 *@return
	 * 	Static where clause to be appended to all search queries.  Return NULL or empty array
	 * when not filtering by additional criteria
	 */
    public function whereClauseArray($filter);
}
