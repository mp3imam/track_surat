<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {

    protected $CI;

    public function __construct()
    {   
        $this->CI =& get_instance();
    }

    public function adminsu_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('_shared/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('_shared/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('_shared/adminsu/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('_shared/footer', $data, TRUE);
            return $this->CI->load->view('_shared/template', $this->template);
        }
    }

    public function adminkb_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('_shared/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('_shared/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('_shared/adminkb/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('_shared/footer', $data, TRUE);
            return $this->CI->load->view('_shared/template', $this->template);
        }
    }

    public function adminmk_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('_shared/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('_shared/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('_shared/adminmk/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('_shared/footer', $data, TRUE);
            return $this->CI->load->view('_shared/template', $this->template);
        }
    }

    public function adminsg_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('_shared/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('_shared/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('_shared/adminsg/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('_shared/footer', $data, TRUE);
            return $this->CI->load->view('_shared/template', $this->template);
        }
    }

    public function admintl_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('_shared/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('_shared/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('_shared/admintl/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('_shared/footer', $data, TRUE);
            return $this->CI->load->view('_shared/template', $this->template);
        }
    }

    public function auth_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('_shared/unauth/header', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('_shared/unauth/footer', $data, TRUE);
            return $this->CI->load->view('_shared/unauth/template', $this->template);
        }
    }

}
