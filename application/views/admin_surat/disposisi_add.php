<?php
defined('BASEPATH') OR exit('No direct script access allowed');
foreach ($surat as $key => $tamp) {
  $id_update      = $tamp['id'];
  $no_reg         = $tamp['no_reg'];
  $divisi_bagian  = $tamp['divisi_bagian'];
  $keterangan     = $tamp['keterangan'];
  $nama_rek       = $tamp['nama_rek'];
  $no_rek         = $tamp['no_rek'];
  $tanggal_masuk  = $tamp['created_date'];
}

?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
  </section>
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-body">
        <i class="fa fa-pencil-square"></i> Disposisi Surat
      </div>
      <!-- /.box-body -->
      <?php echo form_open('admin_surat/post_disposisi_mk/'.$id_update); ?>
        <br>
        <label>No Register :</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-check"></i></span>
          <input type="text" name="id_mk" class="form-control" value="<?=$no_reg?>" readonly>
        </div>
        <br>
        <label>Divisi Bagian :</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-check"></i></span>
          <input type="text" name="id_mk" class="form-control" value="<?=$divisi_bagian?>" readonly>
        </div>
        <br>
        <label>Keterangan :</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-check"></i></span>
          <input type="text" name="id_mk" class="form-control" value="<?=$keterangan?>" readonly>
        </div>
        <br>
        <label>Nama Rekening :</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-check"></i></span>
          <input type="text" name="id_mk" class="form-control" value="<?=$nama_rek?>" readonly>
        </div>
        <br>
        <label>No Rekening :</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-check"></i></span>
          <input type="text" name="id_mk" class="form-control" value="<?=$no_rek?>" readonly>
        </div>
        <br>
        <label>Tanggal Masuk :</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          <input type="text" name="id_mk" class="form-control" value="<?=$tanggal_masuk?>" readonly>
        </div>
        <br>
        <label>Pilih Maker :</label>
        <div class="input-group">
          <input name="id_surat" type="hidden" value="<?=$id_update?>">
          <span class="input-group-addon"><i class="fa fa-plus"></i></span>
          <select name="id_mk" id="makers" class="form-control select2" required>
          </select>
        </div>
        <br>
        </div>
        <fieldset style="text-align:right;">
          <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
        </fieldset> 
      </form>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- Select2 -->
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>

<script type="text/javascript">
  $(document).ready(function() {
      var makers = '<?php echo $makers; ?>';
      makers = $.parseJSON(makers);

      //Initialize Select2 Elements
      $('#makers').html('').select2({ data: makers });
      $('.select2').select2();
    });
</script>