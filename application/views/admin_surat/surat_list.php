<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Surat List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>

			<div class="box-body" style="overflow-x:scroll">
				<table class="table table-striped table-bordered table-hover dt-responsive" cellspacing="0" id="Table"width="100%">
					<thead>
						<tr>
							<th>No Registrasi</th>
							<th>Dari Divisi</th>
							<th>Perihal</th>
							<th>Nama Rekening</th>
							<th>No Rekening</th>
							<th>Tanggal Masuk</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
	</section>
	<!-- /.content -->
</div>

<div class="modal fade" id="modal-konfirmasi">
</div>

<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
	var oTable;
    $(document).ready(function() {
      	$('#Table thead tr').clone(true).appendTo( '#Table thead' );
    	$('#Table thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );
	});

    $(document).ready(function() {
        oTable = $('#Table').DataTable({
			<?php if ($this->ion_auth->is_adminsu()) { ?>
				"buttons": [
					{
						text: 'Add',
						action: function ( e, dt, node, config ) {
							location.href = '<?php echo site_url("admin_surat/surat_add/") ?>';
						}
					}, {
						text: 'Excel',
						action: function ( e, dt, node, config ) {
							location.href = '<?php echo site_url("admin_surat/surat_pdf/") ?>';
						}
					}

				],
				"dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			<?php }else{ ?>
				"dom": 'frt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			<?php } ?>
			orderCellsTop: true,
        	fixedHeader: true,
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "processing": true,
            "serverSide": true,
			"searching": true,
            "order": [[13, 'desc']],
            "ajax": {
            type: "POST",
            url: "<?php echo site_url('admin_surat/get_surat_list') ?>",
            data: function ( d ) {
                return $.extend( {}, d, {
                    "<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
                });
            },
            },
            "cache": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "columns": [{
                data: "a.no_reg",
                orderable: true
			}, {
                data: "a.divisi_bagian",
                orderable: true
			}, {
                data: "a.keterangan",
                orderable: true
			}, {
                data: "a.nama_rek",
                orderable: true
			}, {
                data: "a.no_rek",
                orderable: true
			}, {
                data: "a.created_date",
                orderable: true
			}, {
                data: "c.nameuser",
                visible: false
			}, {
                data: "b.nm_mk",
                visible: false
			}, {
                data: "b.tgl_mk",
                visible: false
			}, {
                data: "b.nm_sg",
                visible: false
			}, {
                data: "b.nm_tl",
                visible: false
			}, {
                data: "b.reject_by",
                visible: false
			}, {
                data: "b.reject_date",
                visible: false
			}, {
                data: "a.id",
                className: "text-center",
                "mRender": function(data, type, row) {
                	var htmlBtnAct = "";
                    var nm_mk = row.b.nm_mk;
                    var tgl_mk = row.b.tgl_mk;
                    var nm_sg = row.b.nm_sg;
                    var nm_tl = row.b.nm_tl;
					var rjc = row.b.reject_by;
						if(typeof rjc == 'undefined' || !rjc || rjc.length === 0 || rjc === "" || !/[^\s]/.test(rjc) || /^\s*$/.test(rjc) || rjc.replace(/\s/g,"") === ""){
							if(typeof nm_mk == 'undefined' || !nm_mk || nm_mk.length === 0 || nm_mk === "" || !/[^\s]/.test(nm_mk) || /^\s*$/.test(nm_mk) || nm_mk.replace(/\s/g,"") === ""){
								<?php if ($this->ion_auth->is_adminkb()) { ?>
									htmlBtnAct += "<btn type='button' class='btn btn-xs btn-success' data-toggle='modal' data-target= '#modal-konfirmasi' onclick='get_disposisi("+row.a.id+",\""+row.a.no_reg+"\",\""+row.a.divisi_bagian+"\",\""+row.a.keterangan+"\",\""+row.a.nama_rek+"\",\""+row.a.no_rek+"\",\""+row.a.created_date+"\");'>Disposisi</btn>";
								<?php }else{ ?>
									htmlBtnAct += "<btn type='button' class='btn btn-xs btn-warning'>Belum Disposisi</btn>";
								<?php } ?>
							}else{
								if(typeof tgl_mk == 'undefined' || !tgl_mk || tgl_mk.length === 0 || tgl_mk === "" || !/[^\s]/.test(tgl_mk) || /^\s*$/.test(tgl_mk) || tgl_mk.replace(/\s/g,"") === ""){
									htmlBtnAct += "<btn type='button' class='btn btn-xs btn-info'>Progress in "+row.b.nm_mk+"</btn>";
								}else{
									if(typeof nm_sg == 'undefined' || !nm_sg || nm_sg.length === 0 || nm_sg === "" || !/[^\s]/.test(nm_sg) || /^\s*$/.test(nm_sg) || nm_sg.replace(/\s/g,"") === ""){
										<?php if ($this->ion_auth->is_adminsg()) { ?>
											htmlBtnAct += "<btn type='button' class='btn btn-xs btn-success' data-toggle='modal' data-target= '#modal-konfirmasi' onclick='get_cheker("+row.a.id+",\""+row.a.no_reg+"\",\""+row.a.divisi_bagian+"\",\""+row.a.keterangan+"\",\""+row.a.nama_rek+"\",\""+row.a.no_rek+"\",\""+row.a.created_date+"\",\""+row.b.nm_mk+"\");'>Approve Cheker</btn>";
										<?php }else{ ?>
											htmlBtnAct += "<btn type='button' class='btn btn-xs btn-info'>Progress in Cheker</btn>";
										<?php } ?>
									}else{
										htmlBtnAct += "<btn type='button' class='btn btn-xs btn-info'>Progress in Cheker</btn>";

									}

								}
							}
						}else{
							htmlBtnAct += "<btn type='button' class='btn btn-xs btn-danger'>Ditolak "+row.b.reject_by+"</btn>";
						}

					return htmlBtnAct;
                }
            }],
            bFilter: false
        });

    });

	<?php if ($this->ion_auth->is_adminkb()) { ?>
		function disposisi_mk(id_surat,id_mk) {
	        window.location.href = "<?php echo site_url("admin_surat/post_disposisi_mk/") ?>" + id_surat+"/"+id_mk;
	    }

		function get_disposisi(id_,no_reg,divisi_bagian,keterangan,nama_rek,no_rek,created_date)
		{
			var id_mk = null;
			var divModal = 	
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
							'<span aria-hidden="true">&times;</span></button>'+
							'<h4 class="modal-title">Disposisi Ke Maker</h4>'+
						'</div>'+
	
			            '<div class="modal-body">'+
							'<div class="box-body" style="white-space: nowrap;">'+
								'<table class="table table-striped table-hover dt-responsive nowrap" cellspacing="0" id="Table"width="100%">'+
									'<thead>'+
										'<tr>'+
											'<td>No Register</td>'+
											'<td>'+no_reg+'</td>'+
										'<tr>'+
											'<td>Dari Divisi Bagian</td>'+
											'<td>'+divisi_bagian+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Keterangan</td>'+
											'<td>'+keterangan+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Nama Rekening</td>'+
											'<td>'+nama_rek+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>No Rekening</td>'+
											'<td>'+no_rek+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Tanggal Surat Masuk</td>'+
											'<td>'+created_date+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Surat ini akan Disposisikan untuk Maker</td>'+
											'<td><select name="id_mk" id="makers" class="form-control select2" style="width: 100%;"></select></td>'+
										'</tr>'+
									'</thead>'+
									'<tbody>'+
									'</tbody>'+
								'</table>'+
							'</div>'+
			            '</div>'+

						'<div class="modal-footer" id="divButton">'+
						'<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>'+
						'</div>'+
					'</div>'+
				'</div>';

			$('#modal-konfirmasi').html(divModal);

	    	var makers = '<?php echo $makers; ?>';
			makers = $.parseJSON(makers);
			$('#makers').html('').select2({ data: makers });
			$('.select2').select2();
			$("#makers").on("select2:select", function (e) {
				var divButton = null;
				var id_mk = $(e.currentTarget).val();
				if (id_mk != 0){
					divButton = '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button><button type="button" class="btn btn-success pull-right" onclick="disposisi_mk('+id_+','+id_mk+')")>Disposisi</button>';					
				}else{
					divButton = '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>';
				}
				$('#divButton').html(divButton);				
			});

		}
	<?php } ?>	

	<?php if ($this->ion_auth->is_adminsg()) { ?>
		function cheker_approve(id_surat,id_mk) {
	        window.location.href = "<?php echo site_url("admin_surat/post_cheker_approve/") ?>" + id_surat+"/"+id_mk;
	    }

		function KonfirmasiTolak(id_surat,alasan) {
			var id_sg 	= prompt("Berikan Alasan Penolakan :", "");
			var alasan 	= prompt("Berikan Alasan Penolakan :", "");
			if (alasan != null) {
		    	window.location.href = "<?php echo site_url("admin_surat/Tolak_Surat/") ?>"+id_surat+"/"+alasan;
			}
		}

		function get_cheker(id_,no_reg,divisi_bagian,keterangan,nama_rek,no_rek,created_date, nm_mk)
		{
			var divModal = 	
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
							'<span aria-hidden="true">&times;</span></button>'+
							'<h4 class="modal-title">Disposisi Ke Maker</h4>'+
						'</div>'+
	
			            '<div class="modal-body">'+
							'<div class="box-body" style="white-space: nowrap;">'+
								'<table class="table table-striped table-hover dt-responsive nowrap" cellspacing="0" id="Table"width="100%">'+
									'<thead>'+
										'<tr>'+
											'<td>No Register</td>'+
											'<td>'+no_reg+'</td>'+
										'<tr>'+
											'<td>Dari Divisi Bagian</td>'+
											'<td>'+divisi_bagian+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Keterangan</td>'+
											'<td>'+keterangan+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Nama Rekening</td>'+
											'<td>'+nama_rek+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>No Rekening</td>'+
											'<td>'+no_rek+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Tanggal Surat Masuk</td>'+
											'<td>'+created_date+'</td>'+
										'</tr>'+
										'<tr>'+
											'<td>Nama Maker</td>'+
											'<td>'+nm_mk+'</td>'+
										'</tr>'+
									'</thead>'+
									'<tbody>'+
									'</tbody>'+
								'</table>'+
							'</div>'+
			            '</div>'+

						'<div class="modal-footer" id="divButton">'+
						'<button type="button" class="btn btn-danger pull-left" onclick="KonfirmasiTolak('+id_+')")>Reject</button>'+
						'<button type="button" class="btn btn-success pull-right" onclick="cheker_approve('+id_+','+id_mk+')")>Approve</button>'+
						'</div>'+
					'</div>'+
				'</div>';

			$('#modal-konfirmasi').html(divModal);
		}
	<?php } ?>	
</script>
