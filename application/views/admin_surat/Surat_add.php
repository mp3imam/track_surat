<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">
<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
  </section>
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-body">
        <i class="fa fa-pencil-square"></i> Add Surat 
      </div>
      <!-- /.box-body -->
    <?php echo form_open('admin_surat/post_surat_add'); ?>
          
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <input type="text" name="dari_divisi" class="form-control" placeholder="Dari Divisi" required>
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <input type="text" name="nama_rekening" class="form-control" placeholder="Nama Rekening" required>
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <input type="text" name="no_rekening" class="form-control" placeholder="No Rekening" required>
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <input type="text" name="keterangan" class="form-control" placeholder="Keterangan" required>
        </div>
        <br>
      <fieldset style="text-align:right;">                
        <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
      </fieldset> 

  </form>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script>
    $(function () {
        $('#area').select2();
    });
</script>