<?php
defined('BASEPATH') OR exit('No direct script access allowed');
foreach ($temp as $key => $tamp) {
    $id_       		= $tamp['id_'];
    $mut_type_name 	= $tamp['mut_type_name'];
    $cluster_name 	= $tamp['cluster_name'];
    $time_motion 	= $tamp['time_motion'];
  }
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Edit Cluster
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_cluster_edit'); ?>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="mut_type_name" class="form-control" 
                  value="<?=$mut_type_name?>" placeholder="Mut Type Name" disabled>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="hidden" name="id_update" class="form-control" value="<?=$id_?>" required>
                  <input type="text" name="cluster_name" class="form-control" 
                  value="<?=$cluster_name?>" placeholder="Cluster Name" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="time_motion" class="form-control" 
                  value="<?=$time_motion?>" placeholder="Time Motion" required>
              </div>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
