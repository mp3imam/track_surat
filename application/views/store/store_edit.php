<?php
defined('BASEPATH') OR exit('No direct script access allowed');

foreach ($stores as $key => $store_edit) {
	$id_       		 = $store_edit['id_'];
	$region_name 	 = $store_edit['region_name'];
	$area_name 		 = $store_edit['area_name'];
	$id_cluster 	 = $store_edit['id_cluster'];
	$dist_name 		 = $store_edit['dist_name'];
	$mut_type_name 	 = $store_edit['mut_type_name'];
	$id_cluster 	 = $store_edit['id_cluster'];
	$store_name 	 = $store_edit['store_name'];
	$outlet_code 	 = $store_edit['outlet_code'];
	$store_residence = $store_edit['store_residence'];
	$residence_checked = strtoupper($store_residence) == 'RESIDENTIAL' ? 'checked' : '';
	$residence_unchecked = strtoupper($store_residence) == 'STORE' ? 'checked' : '';
	$store_addr 	 = $store_edit['store_address'];
  }

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Edit Store
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_store_edit'); ?>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <input type="text" name="region_name" class="form-control" 
                  value="<?=$region_name?>" placeholder="Region Name" disabled>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="area_name" class="form-control" 
                  value="<?=$area_name?>" placeholder="Area Name" disabled>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="dist_name" class="form-control" 
                  value="<?=$dist_name?>" placeholder="Distributor Name" disabled>
              </div>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <input type="text" name="dist_name" class="form-control" 
                  value="<?=$mut_type_name?>" placeholder="MUT Type Name" disabled>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
				  <select name="cluster" id="id_cluster" class="form-control">
                	<option>- Pilih Cluster -</option>
					<?php 
						foreach ($clusters as $key => $cluster) {
							if ($cluster['id_'] == $id_cluster) {
								echo "<option value ='".$cluster['id_']."' selected='selected'>".$cluster['cluster_name']."</option>";
							} else {
								echo "<option value ='".$cluster['id_']."'>".$cluster['cluster_name']."</option>";
							}
						} 
					?>
				</select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="store_name" class="form-control" 
				  value="<?=$store_name?>" placeholder="Store Name" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="outlet_code" class="form-control" 
				  value="<?=$outlet_code?>" placeholder="Code Outlet / Conses" required>
              </div>
              <br>
              <div class="input-group">
					<span class="input-group-addon">
						<input type="radio" value="RESIDENTIAL" name="store_residence" <?= $residence_checked; ?> required>
					</span>
				<input type="text" class="form-control" value="RESIDENTIAL" readonly>
              </div>
              <div class="input-group">
                        <span class="input-group-addon">
                          <input type="radio" value="PASAR" name="store_residence" <?= $residence_unchecked; ?> required>
                        </span>
                    <input type="text" class="form-control" value="PASAR" readonly>
			  </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="store_address" class="form-control" 
				  value="<?=$store_addr?>" placeholder="Alamat Toko">
              </div>
              <br>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 
		<input type="hidden" name="id_store" value="<?= $id_ ?>">
	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	function tampil_cluster(){ 
     kddi = document.getElementById("type_cluster").value; 
     $.ajax({
         url:"<?php echo base_url().ADD_INDEXPHP;?>store/pilih_cluster/"+kddi+"",
         success: function(response){
         $("#cluster").html(response);
         },
         dataType:"html"
     });
     return false;
    }

    function tampil_area(){ 
     kddi = document.getElementById("id_region").value; 
     $.ajax({
         url:"<?php echo base_url().ADD_INDEXPHP;?>store/pilih_area/"+kddi+"",
         success: function(response){
         $("#area").html(response);
         },
         dataType:"html"
     });
     return false;
    }

    function tampil_distributor(){ 
     kddi = document.getElementById("area").value; 
     $.ajax({
         url:"<?php echo base_url().ADD_INDEXPHP;?>/store/pilih_distributor/"+kddi+"",
         success: function(response){
         $("#distributor").html(response);
         },
         dataType:"html"
     });
     return false;
    }
</script>
