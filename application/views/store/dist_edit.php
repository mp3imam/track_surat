<?php
defined('BASEPATH') OR exit('No direct script access allowed');
foreach ($temp as $key => $tamp) {
    $area_name 		= $tamp['area_name'];
    $region_name 	= $tamp['region_name'];
    $id_ 			= $tamp['id_'];
    $id_area		= $tamp['id_area'];
    $id_mp			= $tamp['id_mp'];
    $id_bp			= $tamp['id_bp'];
    $bp_name		= $tamp['bp_name'];
    $dist_code		= $tamp['dist_code'];
    $dist_name		= $tamp['dist_name'];
  }
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Edit dist
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_dist_edit'); ?>
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
	                <input type="text" name="region_name" class="form-control"
                  value="<?=$region_name?>" disabled>
	            </div>
	            <br>
	            <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
	                <input type="text" name="area_name" class="form-control"
                  value="<?=$area_name?>" disabled>
	            </div>
	            <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="hidden" name="id_area" class="form-control" value="<?=$id_area?>" required>
                  <input type="hidden" name="cek_bp" class="form-control" value="<?=$id_bp?>" required>
				  <select name="id_bp" id="id_bp" class="form-control">
                	<option>- Pilih Distributor -</option>
					<?php 
						foreach ($bpname as $key => $bp) {
							if ($bp['id_area'] == $id_area && $bp['id_bp'] == $id_bp) {
								echo "<option value ='".$bp['id_bp']."' selected='selected'>".$bp['bp_name']."</option>";
							} else {
								echo "<option value ='".$bp['id_bp']."'>".$bp['bp_name']."</option>";
							}
						} 
					?>
				</select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="hidden" name="id_update" class="form-control" value="<?=$id_?>" required>
                  <input type="text" name="dist_name" class="form-control" 
                  value="<?=$dist_name?>" placeholder="dist Name" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="dist_code" class="form-control"
                  value="<?=$dist_code?>" placeholder="dist_code" required>
              </div>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
