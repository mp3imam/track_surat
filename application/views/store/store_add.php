<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Add Store
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_Store_add', array('id'=>'frmStore')); ?>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <select name="region" id="id_region" class="form-control" onchange="tampil_area()" required>
                  <option>- Choose Region -</option>
                    <?php foreach ($region as $key => $r) {
                      echo "<option value ='".$r['id_']."'>".$r['region_name']."</option>";
                    } ?>
                </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <?php 
                      $styleus = 'id="area" class="form-control" onchange="tampil_distributor()"';
                      echo form_dropdown('id_area', array('Pilih Area'=>'- Choose Area -'), '', $styleus);
                  ?>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <?php 
                      $styleus = 'id="distributor" class="form-control" ';
                      echo form_dropdown('id_dist', array('Pilih Distributor'=>'- Choose Distributor -'), '', $styleus);
                  ?>
              </div>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <select name="id_mut_type" id="type_cluster" class="form-control" onchange="tampil_cluster()" required>
                	<option>- Pilih Type Store -</option>
                  	<?php foreach ($temp as $key => $r) {
                  		echo "<option value ='".$r['id_']."'>".$r['mut_type_name']."</option>";
                  	} ?>
                </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <?php 
                      $styleus = 'id="cluster" class="form-control" ';
                      echo form_dropdown('id_cluster', array('Pilih Cluster'=>'- Pilih Cluster -'), '', $styleus);
                  ?>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="store_name" class="form-control" placeholder="Store Name" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="outlet_code" class="form-control" placeholder="Code Outlet / Conses" required>
              </div>
              <br>
              <div class="input-group">
                        <span class="input-group-addon">
                          <input type="radio" value="RESIDENTIAL" name="store_residence" required>
                        </span>
                    <input type="text" class="form-control" value="RESIDENTIAL" readonly>
              </div>
              <div class="input-group">
                        <span class="input-group-addon">
                          <input type="radio" value="PASAR" name="store_residence" required>
                        </span>
                    <input type="text" class="form-control" value="PASAR" readonly>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="store_address" class="form-control" placeholder="Alamat Toko">
              </div>
              <br>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit" onclick="submit_store()"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	function submit_store() {
		$('form').submit(function() {
			$("#submit").prop('disabled', true);
		});
	};
	
	function tampil_cluster(){ 
     kddi = document.getElementById("type_cluster").value; 
     $.ajax({
         url:"<?php echo  base_url().ADD_INDEXPHP;?>store/pilih_cluster/"+kddi+"",
         success: function(response){
         $("#cluster").html(response);
         },
         dataType:"html"
     });
     return false;
    }

    function tampil_area(){ 
     kddi = document.getElementById("id_region").value; 
     $.ajax({
         url:"<?php echo  base_url().ADD_INDEXPHP;?>store/pilih_area/"+kddi+"",
         success: function(response){
         $("#area").html(response);
         },
         dataType:"html"
     });
     return false;
    }

    function tampil_distributor(){ 
     kddi = document.getElementById("area").value; 
     $.ajax({
         url:"<?php echo  base_url().ADD_INDEXPHP;?>store/pilih_distributor/"+kddi+"",
         success: function(response){
         $("#distributor").html(response);
         },
         dataType:"html"
     });
     return false;
    }
</script>
