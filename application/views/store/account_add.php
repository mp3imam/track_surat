<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Add account
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_account_add'); ?>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="account_name" class="form-control" placeholder="account Name" required>
              </div>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
