<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Store List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div id="divView" class="col-lg-4 col-xs-6">
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped table-condensed" style="margin-bottom: 0;">
					<tr>
						<td style="padding-bottom: 0;">
							<div class="col-sm-3 col-lg-2">
								<div class="form-group">
									<label for="muttype"><?php echo "MUT Type";?></label>
									<select id="muttype" class="form-control" style="width: 100%">
										<option value="" selected="true">-- Select --</option>
										<option value="1">Family Grocery</option>
										<option value="2">General Trade</option>
										<option value="3">Ice Cream</option>
									</select>
								</div>
							</div>
							<div class="col-sm-3 col-lg-3">
								<div class="form-group">
									<label for="distname"><?php echo "Distributor";?></label>
									<input class="form-control clearable" id="distname">
								</div>
							</div>
							<div class="col-sm-3 col-lg-3">
								<div class="form-group">
									<label for="storename"><?php echo "Store Name";?></label>
									<input class="form-control clearable" id="storename">
								</div>
							</div>
							<div class="col-sm-3 col-lg-2">
								<div class="form-group">
									<label for="qrcode"><?php echo "Qr Code";?></label>
									<input class="form-control clearable" id="qrcode">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<input id="btnSearch" type="button" class="btn btn-default" value="<?php echo "Search";?>" style="margin-top:25px; width: 100%;">
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="box-body" style="overflow-x:scroll;white-space: nowrap;">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="120%" cellspacing="0" id="storeTable">
					<thead>
						<tr>
							<th style="width: 50px;"><?php echo 'BP Code';?></th>
							<th style="width: 50px;"><?php echo 'MUT Type';?></th>
							<th style="width: 150px;"><?php echo 'Cluster Name';?></th>
							<th style="width: 200px;"><?php 'ID Distributor';?></th>
							<th style="width: 200px;"><?php echo 'Distributor Name';?></th>
							<th style="width: 200px;"><?php 'ID Area';?></th>
							<th style="width: 100px;"><?php echo 'Account Name';?></th>
							<th style="width: 80px;"><?php echo 'QR Code';?></th>
							<th style="width: 200px;"><?php echo 'ID Store';?></th>
							<th style="width: 200px;"><?php 'ID Store';?></th>
							<th style="width: 200px;"><?php echo 'Store Name';?></th>
							<th style="width: 80px;"><?php echo 'Outlet Code';?></th>
							<th style="width: 100px;"><?php echo 'Store Residence';?></th>
							<th style="width: 200px;"><?php echo 'Store Address';?></th>
							<th style="width: 30px;"></th>
							<th style="width: 30px;">History QRCode</th>
							<th style="width: 30px;">Register QRCode</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
			<!-- Modal dialog popup !-->
				<div class="modal fade" id="modal-konfirmasi">
				</div>

				<div id="confirmationModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Confirm To <span class="msgModal"></span></h4>
							</div>
							<div class="modal-body">
								<p><?php echo "Are you sure you want to Reset Qrcode Store Name ";?>&nbsp;<span class="msgModal"></span>?</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="width: 100px;"><?php echo "No";?></button>
								<button type="button" class="btn btn-primary" id="confirmModal" style="width: 100px;"><?php echo "Yes";?></button>
							</div>
						</div>
					</div>
				</div>
			<!-- End Modal dialog popup !-->
			</div>			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
	var oTable;

    $(document).ready(function() {
      	$('#storeTable thead tr').clone(true).appendTo( '#storeTable thead' );
    	$('#storeTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );
	    });

	function reset_qrcode(id_,id_dist,id_area,last_qrcode) {
    	window.location.href = "<?php echo site_url("store/reset_qrcode/") ?>" + id_+"<?php echo "/"; ?>"+ id_dist+"<?php echo "/"; ?>"+ id_area+"<?php echo "/"; ?>"+last_qrcode;
	}

    $(document).ready(function() {
        
        oTable = $('#storeTable').DataTable({
			"buttons": [
				{
					text: 'Add',
					action: function ( e, dt, node, config ) {
						location.href = '<?php echo site_url("store/store_add/") ?>';
					}
				},
				{
					text: 'Export to Excel',
					action: function ( e, dt, node, config ) {
						location.href = "<?php echo site_url("store/export_excel_store") ?>?" + "<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&mtype=" + $('#muttype').val() + "&dist=" + $('#distname').val() + "&store=" + $('#storename').val();
					}
				}
			],
			orderCellsTop: true,
        	fixedHeader: true,
            "dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "processing": true,
            "serverSide": true,
			"searching": true,
			"order": [[0, 'desc'],[1, 'desc'],[2, 'desc'],[3, 'desc'],[4, 'desc'],[6, 'desc']],
            "ajax": {
            type: "POST",
            url: "<?php echo site_url('store/get_store_list') ?>",
            data: function ( d ) {
                return $.extend( {}, d, {
                    "<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>",
					"muttype": $("#muttype").val(),
					"distname": $("#distname").val(),
					"qrcode": $("#qrcode").val(),
					"storename": $("#storename").val()
                });
            },
            },
            "cache": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "columns": [{data: "h.bp_code",
                orderable: true
			},{
                data: "b.mut_type_name",
                orderable: true
			}, {
                data: "c.cluster_name",
                orderable: true
			}, {
                data: "a.id_dist",
                visible: false
			}, {
                data: "d.dist_name",
                orderable: true
			}, {
                data: "d.id_area",
                visible: false
			}, {
                data: "e.account_name",
                orderable: true
			}, {
                data: "a.id_qrcode",
                orderable: true
			}, {
                data: "a.id_",
                visible: true
			}, {
                data: "j.id_store",
                visible: false
			}, {
                data: "a.store_name",
                orderable: true
			}, {
                data: "a.outlet_code",
                orderable: true
			}, {
                data: "a.store_residence",
                orderable: true
			}, {
                data: "a.store_address",
                orderable: true
			}, {
                data: "a.id_",
                className: "text-center",
                "mRender": function(data, type, row) {
	                    var htmlBtnAct = "<a title='Edit' href='<?php echo site_url("store/store_edit/") ?>" + data + "' class='btn btn-xs btn-info m-t-15 waves-effect'>Edit Store</a>";
                    	return htmlBtnAct;
                	}
			}, {
                data: "a.id_",
                className: "text-center",
                "mRender": function(data, type, row) {
                    var str = row.j.id_store; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = "---- Not Yet  ----";
                		return htmlBtnAct;
                	}
                	else{
	                    var htmlBtnAct = "<btn title='View History QRCode' data-toggle='modal' data-target= '#modal-konfirmasi' onclick='load_qrcode(" + data + ")' class='btn btn-xs btn-success m-t-15 waves-effect'>View History QRCode</btn>";
                    	return htmlBtnAct;
                	}
                }
			}, {
                data: "a.id_",
                className: "text-center",
                "mRender": function(data, type, row) {
                    var str = row.a.id_qrcode; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = "---- Not Register ----";
                		return htmlBtnAct;
                	}
                	else{
	                    var htmlBtnAct = "<btn type='button' class='btn btn-xs btn-danger' data-toggle='modal' data-target= '#modal-konfirmasi' onclick='get_store("+row.a.id_+",\""+row.a.store_name+"\",\""+row.a.id_dist+"\",\""+row.d.id_area+"\",\""+row.a.id_qrcode+"\");'>Reset QRCode</btn>&nbsp; ";
 
                    	return htmlBtnAct;
                	}
                }
			}, {
                data: "a.id_",
				visible: false
            }]
        });
    });

	// Searching
	$('#btnSearch').on('click', function(e) {
		e.preventDefault();
		oTable.draw();
	});
	function get_store(id, store_name,id_dist,id_area,last_qrcode)
	{
		var divModal = '<div class="modal-dialog">';
		divModal	+= '<div class="modal-content">';
		divModal	+= '<div class="modal-header">';
		divModal	+= '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
		divModal	+= '<span aria-hidden="true">&times;</span></button>';
		divModal	+= '<h4 class="modal-title">Konfirmasi Reset QRCode</h4>';
		divModal	+= '</div>';
		divModal	+= '<div class="modal-body">';
		divModal	+= '<input type="hidden" value="'+id+'">';
		divModal	+= '<input type="hidden" value="'+id_dist+'">';
		divModal	+= '<input type="hidden" value="'+id_area+'">';
		divModal	+= '<input type="hidden" value="'+last_qrcode+'">';
		name_store  = 	store_name.replace(/,/g," ");
		divModal 	+= '<h3>Reset QRCode dari Store Name '+ store_name +'?</h3>';
		divModal	+= '<div class="modal-footer">';
		divModal	+= '<button type="button" class="btn btn-danger pull-left"'; 
		divModal	+= 'onclick="reset_qrcode('+id+',\''+id_dist+'\',\''+id_area+'\',\''+last_qrcode+'\');">Yes</button>';
		divModal	+= '<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '<!-- End Modal dialog popup !-->';

		$('#modal-konfirmasi').html(divModal);
    }
    
	function view_HistoryQrcode()
	{
		var divModal = '<div class="modal-dialog">';
		divModal	+= '<div class="modal-content">';
		divModal	+= '<div class="modal-header">';
		divModal	+= '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
		divModal	+= '<span aria-hidden="true">&times;</span></button>';
		divModal	+= '<h4 class="modal-title">History Reset QRCode</h4>';
		divModal	+= '</div>';
		divModal	+= '<div class="modal-body">';
		divModal	+= '<table id="tbl_hist_qrcode" class="table table-bordered">';
		divModal	+= '<tbody>';
		divModal	+= '<tr>';
		divModal	+= '<th>ID Store</th>';
		divModal	+= '<th>Store Name</th>';
		divModal	+= '<th>Distributor Name</th>';
		divModal	+= '<th>Area Name</th>';
		divModal	+= '<th>Last Qrcode</th>';
		divModal	+= '<th>Date Reset</th>';
		divModal	+= '<th>Reset By</th>';
		divModal	+= '</tr>';
		divModal	+= '</tbody>';
		divModal	+= '</table>';
		divModal	+= '<button type="button" class="btn btn-success pull-right" data-dismiss="modal">Close</button> <br><br>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '<!-- End Modal dialog popup !-->';

		$('#modal-konfirmasi').html(divModal);
    }

	function load_qrcode(id_store)
	{

		// Load modal popup
		$('#tbl_hist_qrcode').html(view_HistoryQrcode());

		$.ajax({
			type: "POST",
			url: "<?php echo  base_url().ADD_INDEXPHP;?>store/get_viewQrcode/"+id_store,
			data: {
				"<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
			},
			dataType: "json",
			cache: false,
			success: function(data)
			{
				var res = data;

				//Try to get tbody first with jquery children. works faster!
				var tbody = $('#tbl_hist_qrcode').children('tbody');
				//Then if no tbody just select your table 
				var table = tbody.length ? tbody : $('#tbl_hist_qrcode');

				var resObj = '';
				for (var item in res.result_viewQrcode) {
					resObj = res.result_viewQrcode[item];
					//Add row
					table.append('<tr><td>'+ resObj.id_store +'</td><td>'+ resObj.store_name +'</td><td>'+ resObj.dist_name +'</td><td>'+ resObj.area_name +'</td><td>'+ resObj.last_qrcode +'</td><td>'+ resObj.tgl_reset +'</td><td>'+ resObj.reset_by +'</td></tr>');
				}
			}
		});
	}

</script>
