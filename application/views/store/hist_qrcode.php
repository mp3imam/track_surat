<!DOCTYPE html>
<html>
<head>
<style>
/* The Modal (background) */
.modal {
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}
</style>
</head>
<body>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close"></span>
      <h1 align="center">History QrCode</h1>
    </div>
    <div class="modal-body">
          <table id="example1" class="table table-bordered table-striped" width="100%">
            <thead>
            <tr>
                <td>No</td>
                <td>Id Store</td>
                <td>Area Name</td>
                <td>Distributor Name</td>
                <td>Store Name</td>
                <td>QrCode</td>
                <td>Date Reset</td>
                <td>Reset By <?php print_r($records) ?></td>
            </tr>
            </thead>
            <tbody>
              <?php $nomor=1; foreach ($records as $key => $history_data) {
                if (empty($history_data)){
                ?>  
                  <td class="right">Anda Belum mempunyai history QrCode</td>
              <?php }else{?>
                <tr>
                  <td class="right"><?php echo $nomor; ?></td>
                  <td class="right"><?php echo $history_data['id_store']; ?></td>
                  <td class="right"><?php echo $history_data['area_name']; ?></td>
                  <td class="right"><?php echo $history_data['dist_name']; ?></td>
                  <td class="right"><?php echo $history_data['store_name']; ?></td>
                  <td class="right">
                    <img src="https://api.qrserver.com/v1/create-qr-code/?data=<?=$history_data['last_qrcode'];?>&amp;size=50x50" alt="" title="" />
                    <br><?php echo $history_data['last_qrcode']; ?>
                    </td>
                  <td class="right"><?php echo $history_data['tgl_reset']; ?></td>
                  <td class="right"><?php echo $history_data['reset_by']; ?></td>
                </tr>
              <?php } $nomor++; } ?> 
        
            </tbody>
            <tfoot>
           
            </tfoot>
          </table>
    </div>
    <div class="modal-footer">
      <h2 align="right">Morzel Digital</h2>
    </div>
  </div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

</body>
</html>