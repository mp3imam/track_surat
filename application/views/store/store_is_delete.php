<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-trash"></i> Menghapus Store menjadi is Delete
			</div>
			<!-- /.box-body -->
    <?php echo form_open('store/post_Store_is_delete/0', array('id'=>'frmStore')); ?>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <textarea name="id_store0" rows="5" class="form-control" placeholder="Masukan Delete Store Id"></textarea>
        </div>
      <fieldset style="text-align:right;">
          <button class="btn btn-block btn-danger" name="submit" id="submit" onclick="submit_store()">Delete</button>
      </fieldset> 
    </div>
    </form>
    <div class="box">
      <div class="box-body">
        <i class="fa fa-pencil-square"></i> Mengembalikan store yg telah di delete
      </div>
      <!-- /.box-body -->
    <?php echo form_open('store/post_Store_is_delete/1', array('id'=>'frmStore')); ?>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <textarea name="id_store1" rows="5" class="form-control" placeholder="Masukan Store Id yang ingin di Restore"></textarea>
        </div>
      <fieldset style="text-align:right;">
          <button class="btn btn-block btn-success" name="submit" id="submit" onclick="submit_store()">Restore</button>
      </fieldset> 
    </div>
	 </form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
  function submit_store() {
    $('form').submit(function() {
      $("#submit").prop('disabled', true);
      $("#submit1").prop('disabled', true);
    });
  };
</script>