<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Distributor List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" cellspacing="0" id="distTable">
					<thead>
						<tr>
							<th style="width: 150px;"><?php echo 'Region Name';?></th>
							<th style="width: 200px;"><?php echo 'Area Name';?></th>
							<th style="width: 350px;"><?php echo 'BP Name';?></th>
							<th style="width: 350px;"><?php echo 'Distributor Name';?></th>
							<th style="width: 350px;"><?php echo 'Distributor Code';?></th>
							<th style="width: 30px;"></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
var oTable;

    $(document).ready(function() {
      	$('#distTable thead tr').clone(true).appendTo( '#distTable thead' );
    	$('#distTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );
	    });

    $(document).ready(function() {
        
        oTable = $('#distTable').DataTable({
			"buttons": [
				{
					text: 'Add',
					action: function ( e, dt, node, config ) {
						location.href = '<?php echo site_url("store/dist_add/") ?>';
					}
				},
				{
					text: 'Export to Excel',
					action: function ( e, dt, node, config ) {
						location.href = "<?php echo site_url("store/export_excel_dist") ?>?" + "<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&";
					}
				}
			],
			orderCellsTop: true,
        	fixedHeader: true,
            "dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "processing": true,
            "serverSide": true,
			"searching": true,
            "order": [[2, 'asc']],
            "ajax": {
            type: "POST",
            url: "<?php echo site_url('store/get_dist_list') ?>",
            data: function ( d ) {
                return $.extend( {}, d, {
                    "<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
                });
            },
            },
            "cache": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "columns": [{
                data: "c.region_name",
                orderable: true
			}, {
                data: "b.area_name",
                orderable: true
			}, {
                data: "e.bp_name",
                orderable: true
			}, {
                data: "a.dist_name",
                orderable: true
			}, {
                data: "a.dist_code",
                orderable: true
			}, {
                data: "a.id_",
                className: "text-center",
                "mRender": function(data, type, row) {
                    var htmlBtnAct = "<a title='Edit' href='<?php echo site_url("store/dist_edit/") ?>" + data + "' class='btn btn-xs btn-info m-t-15 waves-effect'>Edit</a>&nbsp;";
                    return htmlBtnAct;
                }
			}, {
                data: "a.id_",
				visible: false
            }],
            bFilter: false
        });

    });
</script>
