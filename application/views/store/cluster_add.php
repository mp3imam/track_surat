<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Add Cluster
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_cluster_add'); ?>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <select name="id_mut_type" id="cluster" class="form-control" required>
                	<option>- Pilih Type Cluster -</option>
                  	<?php foreach ($temp as $key => $r) {
                  		echo "<option value ='".$r['id_']."'>".$r['mut_type_name']."</option>";
                  	} ?>
                </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="cluster_name" class="form-control" placeholder="Cluster Name" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="number" max="100" name="time_motion" class="form-control" placeholder="Time Motion" required>
              </div>
              <br>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	function tampil_area(){ 
     kddi = document.getElementById("region").value; 
     $.ajax({
         url:"<?php echo  base_url().ADD_INDEXPHP;?>location/pilih_area/"+kddi+"",
         success: function(response){
         $("#area").html(response);
         },
         dataType:"html"
     });
     return false;
    }
</script>
