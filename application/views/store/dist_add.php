<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square"></i> Add Distributor
			</div>
			<!-- /.box-body -->
		<?php echo form_open('store/post_dist_add'); ?>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <select name="id_region" id="region" class="form-control" onChange="tampil_area()" required>
                	<option>- Pilih Region -</option>
				<?php foreach ($temp as $key => $r) {
					echo "<option value ='".$r['id_']."'>".$r['region_name']."</option>";
				} ?>
                </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
	                <?php 
	                    $styleus = 'id="area" class="form-control" onChange="tampil_bp()" ';
	                    echo form_dropdown('id_area', array('Pilih Area'=>'- Pilih Area -'), '', $styleus);
	                ?>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <?php 
                      $styleus = 'id="bp" class="form-control" ';
                      echo form_dropdown('id_bp', array('Pilih Business Partner'=>'- Pilih Business Partner -'), '', $styleus);
                  ?>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="dist_name" class="form-control" placeholder="Distributor Name" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="text" name="dist_code" class="form-control" placeholder="Distributor Code" required>
              </div>
              <br>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	function tampil_area(){ 
     kddi = document.getElementById("region").value; 
     $.ajax({
         url:"<?php echo  base_url().ADD_INDEXPHP;?>location/pilih_area/"+kddi+"",
         success: function(response){
         $("#area").html(response);
         },
         dataType:"html"
     });
     return false;
    }

    function tampil_bp(){ 
       kddi = document.getElementById("area").value; 
       $.ajax({
           url:"<?php echo  base_url().ADD_INDEXPHP;?>store/pilih_bp/"+kddi+"",
           success: function(response){
           $("#bp").html(response);
           },
           dataType:"html"
       });
       return false;
    }

</script>
