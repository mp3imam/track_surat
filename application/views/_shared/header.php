<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="<?php echo $lang; ?>">
    <head>
        <meta charset="<?php echo $charset; ?>">
        <title><?php echo $title; ?></title>
<?php if ($mobile === FALSE): ?>
        <!--[if IE 8]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
<?php else: ?>
        <meta name="HandheldFriendly" content="true">
<?php endif; ?>
<?php if ($mobile == TRUE && $mobile_ie == TRUE): ?>
        <meta http-equiv="cleartype" content="on">
<?php endif; ?>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="google" content="notranslate">
        <meta name="robots" content="noindex, nofollow">
<?php if ($mobile == TRUE && $ios == TRUE): ?>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="<?php echo $title; ?>">
<?php endif; ?>
<?php if ($mobile == TRUE && $android == TRUE): ?>
        <meta name="mobile-web-app-capable" content="yes">
<?php endif; ?>
        <!-- <link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqElEQVRYR+2WYQ6AIAiF8W7cq7oXd6v5I2eYAw2nbfivYq+vtwcUgB1EPPNbRBR4Tby2qivErYRvaEnPAdyB5AAi7gCwvSUeAA4iis/TkcKl1csBHu3HQXg7KgBUegVA7UW9AJKeA6znQKULoDcDkt46bahdHtZ1Por/54B2xmuz0uwA3wFfd0Y3gDTjhzvgANMdkGb8yAyY/ro1d4H2y7R1DuAOTHfgAn2CtjCe07uwAAAAAElFTkSuQmCC"> -->
        <link rel="icon" href='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAACbklEQVQ4jZ2RTUhUURiG33PPPXPv3Pkzm0xzUAtLiDGRQnERChUVKIq4qEUUQVQ7yVWtZtFOW7RzUy0iITAVC1eVElJoRA1ESiWmOIrOjzV/9965P6eFTDlqIL2rw+H7nu97v5cAQCgUEmpctU+5jaq15URr9/1rq9ilaCgUEg4rwaGmM4faS8o8ATVjt7XUt06NTQ4t7wZABnqHBxtaKjocEqXePU4oLobP05HY2momo2eM2WRcG45Gsi9u9V+O7AQQmUjqZJlSxStBcTMAQLCx3A/Ab1u8MhHNnl2aS8QeVw4+uXS7q3ubhfaTHe9VzW4/Ulviyn9qGkVOVcEYoLgoSgNupcjvqgkeOOV8PjE4UWABAAZ6R67XNZb2Vgf3ezgAXWMYH/kIpUj8W0gdWPmR0m1JfnSl+9zNAgAAjPaPPjvRfLDTX+oCoQxvxuZh1h6D4HTCr1BYHJBFgq+vPuQ6LzRI+T4h/2i70db1bnwxsh7LglsGmk5XQAiHIWbSAAFy0QSSK3GYJjc3W/gDICB8NW6+nP6UxtuJJWhpFc3nq2DOzIIRAiIISBmA5BBZQQpbryofr4emapgML4BmF8EoBQiQ1G3sVURszXIbAAB8PgWs4SiM6Do8ZcVY1yyUl7jz6/IdLeTldggIeAWIhgEQ4KduIeBlmwvtfwIkp+RjlOCXDixMzSDybQXVRQzFMqClstBS2W3bigDQ1/PwImH8bjoW98niRrIGFeGRKLImoFoAETZmcQ7h3p0H88iRhZ6+qy0iAHBCOGBbHDC/j4eTGc2Me1yOfYZuC19ehwsmRueWDQ7bBKcFt/hv/QYli+iGghaRIQAAAABJRU5ErkJggg==' />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,700italic">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/font-awesome/css/font-awesome.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/ionicons/css/ionicons.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/dataTables.bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/adminlte/css/adminlte.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/adminlte/css/skins/skin-blue.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/adminlte/css/skins/skin-purple.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/animsition/animsition.min.css'); ?>">
<?php if ($this->router->fetch_class() == 'groups' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.css'); ?>">
<?php endif; ?>
        <link rel="stylesheet" href="<?php echo base_url($frameworks_dir . '/domprojects/css/dp.min.css'); ?>">
<?php if ($mobile === FALSE): ?>
        <!--[if lt IE 9]>
            <script src="<?php echo base_url($plugins_dir . '/html5shiv/html5shiv.min.js'); ?>"></script>
            <script src="<?php echo base_url($plugins_dir . '/respond/respond.min.js'); ?>"></script>
        <![endif]-->
<?php endif; ?>
	<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.mask.min.js'); ?>"></script>

    </head>
        <body class="hold-transition skin-blue fixed sidebar-mini">
<?php if ($mobile === FALSE): ?>
        <div class="wrapper animsition">
<?php else: ?>
        <div class="wrapper">
<?php endif; ?>
