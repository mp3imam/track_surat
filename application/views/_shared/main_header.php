<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
            <header class="main-header">
                <!-- left title -->
                <a href="<?php echo site_url('/'); ?>" class="logo">
                    <span class="logo-mini"><b>T</b>RACK SURAT</span>
                    <span class="logo-lg"><b>T</b>RACK SURAT 1.0</span>
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <a href="#" style="float: left; padding: 15px 15px; text-decoration: none; color: #fff; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; ">
                        TRACK SURAT
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Menu -->
                            <li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo base_url($images_dir . '/avatar.png'); ?>" class="user-image" alt="User Image">
									<span class="hidden-xs"><?php echo $user_login['firstname']." ",$user_login['lastname']; ?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
									<img src="<?php echo base_url($images_dir . '/avatar.png'); ?>" class="img-circle" alt="User Image">
					
									<p>
										<?php echo $user_login['firstname']." ",$user_login['lastname']. " - ". $user_login["company"]; ?>
										<small></small>
									</p>
									</li>
									<!-- Menu Body -->
									<!-- Menu Footer-->
									<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo site_url('auth/logout/admin'); ?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
									</li>
								</ul>
							</li>
                        </ul>
                    </div>
                </nav>
            </header>
