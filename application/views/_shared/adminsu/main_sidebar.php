<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<aside class="main-sidebar">
	<section class="sidebar">
		<!-- Sidebar menu -->
		<ul class="sidebar-menu">
			<li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
			<li class="treeview <?=active_link_controller('dashboard')?>">
				<a href="<?php echo site_url('/'); ?>">
					<i class="fa fa-home"></i><span>Dashboard</span>
				</a>
			</li>
			<li class="header text-uppercase">Master Surat</li>
			<li class="<?=active_link_controller('Admin_Surat'); ?>">
				<a href="<?php echo site_url('Admin_Surat'); ?>">
					<i class="fa fa-bookmark"></i> <span>Arsip Persuratan</span>
				</a>
			</li>
			<li class="header text-uppercase">Master User</li>
			<li class="<?=active_link_controller('User_login'); ?>">
				<a href="<?php echo site_url('User_login'); ?>">
					<i class="fa fa-bookmark"></i> <span>User Login</span>
				</a>
			</li>
		</ul>
	</section>
</aside>
