<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">User Login List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>
			<div class="box-body with-border">
				<div id="receiveMessage" class="alert alert-danger alert-dismissible" style="display: none;">
					<button  id="closeReceiveMessage" type="button" class="close">×</button>
					<h4 id="aMessageHead" ><i class="icon fa fa-ban"></i> Failed!</h4>
					<p id="aMessageBody">
					</p>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" cellspacing="0" id="Table">
					<thead>
						<tr>
							<th style="width: 350px;">Username</th>
							<th style="width: 350px;">Name</th>
							<th style="width: 350px;">Jabatan</th>
							<th style="width: 30px;">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
	</section>
	<!-- /.content -->
</div>

<div class="modal fade" id="modal-konfirmasi">
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
	function reset_password(id_) {
        window.location.href = "<?php echo site_url("user_login/reset_password/") ?>" + id_;
    }

	var oTable;

    $(document).ready(function() {
      	$('#Table thead tr').clone(true).appendTo( '#Table thead' );
    	$('#Table thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );
	    });

    $(document).ready(function() {
        oTable = $('#Table').DataTable({
        	<?php if ($this->ion_auth->is_adminsu()) { ?>
				"buttons": [
					{
						text: 'Add',
						action: function ( e, dt, node, config ) {
							location.href = '<?php echo site_url("user_login/user_add/") ?>';
						}
					}, {
						text: 'Excel',
						action: function ( e, dt, node, config ) {
							location.href = '<?php echo site_url("user_login/user_pdf/") ?>';
						}
					}

				],
				"dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			<?php }else{ ?>
				"dom": 'frt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			<?php } ?>
			orderCellsTop: true,
        	fixedHeader: true,
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "processing": true,
            "serverSide": true,
			"searching": true,
            "order": [[3, 'asc']],
            "ajax": {
            type: "POST",
            url: "<?php echo site_url('user_login/get_user_list') ?>",
            data: function ( d ) {
                return $.extend( {}, d, {
                    "<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
                });
            },
            },
            "cache": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "columns": [{
                data: "a.username",
                orderable: true
			}, {
                data: "a.nameuser",
                orderable: true
			}, {
                data: "a.jabatan",
                orderable: true
			}, {
                data: "a.id",
                className: "text-center",
                "mRender": function(data, type, row) {
                    var htmlBtnAct = "<a title='Edit' href='<?php echo site_url("user_login/user_edit/") ?>" + data + "' class='btn btn-xs btn-info m-t-15 waves-effect'>Edit</a>";
                    htmlBtnAct += "&nbsp;<a title='Delete' href='#' class='btn btn-xs btn-warning m-t-15 waves-effect' data-toggle='modal' data-target='#modal-konfirmasi' onclick='get_user_login("+data+",\""+row.a.username+"\")'>Reset Password</a>";
					return htmlBtnAct;
                }
			}, {
                data: "a.id",
				visible: false
            }],
            bFilter: false
        });

    });

	function get_user_login(id_,user_name)
	{
		var divModal = '<div class="modal-dialog">';
		divModal	+= '<div class="modal-content">';
		divModal	+= '<div class="modal-header">';
		divModal	+= '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
		divModal	+= '<span aria-hidden="true">&times;</span></button>';
		divModal	+= '<h4 class="modal-title">Konfirmasi Reset Password User Login</h4>';
		divModal	+= '</div>';
		divModal	+= '<div class="modal-body">';
		divModal	+= '<input type="hidden" value="'+id_+'">';
		divModal 	+= '<h3>Reset Password '+ user_name +'?</h3>';
		divModal	+= '<div class="modal-footer">';
		divModal	+= '<button type="button" class="btn btn-warning pull-left"'; 
		divModal	+= 'onclick="reset_password('+id_+');">Yes</button>';
		divModal	+= '<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '<!-- End Modal dialog popup !-->';

		$('#modal-konfirmasi').html(divModal);
    }
</script>
