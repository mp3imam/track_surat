<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">
<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
  </section>
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-body">
        <i class="fa fa-pencil-square"></i> Add User Login
      </div>
      <!-- /.box-body -->
    <?php echo form_open('user_login/post_user_add'); ?>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <select name="type" id="type" name="type" class="form-control select2" required>
              <option value="SuperAdmin">Admin</option>;
              <option value="Maker">Maker</option>;
              <option value="Cheker">Cheker</option>;
              <option value="Teller">Teller</option>;
            </select>
          </div>
          <br>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-plus"></i></span>
            <input type="text" name="nameuser" class="form-control" placeholder="Masukan Nama User" required>
        </div>
          <br>

      <fieldset style="text-align:right;">                
        <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
      </fieldset> 

  </form>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script>
    $(function () {
        $('#area').select2();
    });
</script>