<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Maker Jurnal List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>
			<div class="box-body" style="overflow-x:scroll">
				<table class="table table-striped table-bordered table-hover dt-responsive" cellspacing="0" id="Table"width="100%">
					<thead>
						<tr>
							<th>Username</th>
							<th>Nama Maker</th>
							<th>Tanggal Maker</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
	</section>
	<!-- /.content -->
</div>

<div class="modal fade" id="modal-konfirmasi">
</div>

<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
	var oTable;
    $(document).ready(function() {
      	$('#Table thead tr').clone(true).appendTo( '#Table thead' );
    	$('#Table thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );
	});

    $(document).ready(function() {
        oTable = $('#Table').DataTable({
			<?php if ($this->ion_auth->is_adminsu()) { ?>
				"buttons": [
					{
						text: 'Add',
						action: function ( e, dt, node, config ) {
							location.href = '<?php echo site_url("admin_maker/surat_add/") ?>';
						}
					}, {
						text: 'PDF',
						action: function ( e, dt, node, config ) {
							location.href = '<?php echo site_url("admin_maker/surat_pdf/") ?>';
						}
					}

				],
				"dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			<?php }else{ ?>
				"dom": 'frt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			<?php } ?>
			orderCellsTop: true,
        	fixedHeader: true,
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "processing": true,
            "serverSide": true,
			"searching": true,
            "order": [[0, 'desc']],
            "ajax": {
            type: "POST",
            url: "<?php echo site_url('admin_maker/get_maker_list') ?>",
            data: function ( d ) {
                return $.extend( {}, d, {
                    "<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
                });
            },
            },
            "cache": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "columns": [{
                data: "a.user_mk",
                orderable: true
			}, {
                data: "a.nm_mk",
                orderable: true
			}, {
                data: "a.tgl_mk",
                orderable: true
			}, {
                data: "a.id",
                className: "text-center",
                "mRender": function(data, type, row) {
	                    var htmlBtnAct = "<a title='Edit' href='<?php echo site_url("store/store_edit/") ?>" + data + "' class='btn btn-xs btn-info m-t-15 waves-effect'>Edit Store</a>";
                    	return htmlBtnAct;
                	}
            }],
            bFilter: false
        });

    });
</script>
