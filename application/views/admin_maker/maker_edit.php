<?php
defined('BASEPATH') OR exit('No direct script access allowed');
foreach ($id_edit as $key => $tamp) {
  $id_update  = $tamp['id_update'];
  $nameuser = $tamp['nameuser'];
}
?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
  </section>
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-body">
        <i class="fa fa-pencil-square"></i> Edit User Login
      </div>
      <!-- /.box-body -->
      <?php echo form_open('user_login/post_edit/'.$id_update); ?>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-plus"></i></span>
          <input type="text" name="nameuser" class="form-control" 
          value="<?=$nameuser?>" placeholder="Nama User" required>   
        </div>
        <br>
        </div>
        <fieldset style="text-align:right;">
          <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
        </fieldset> 
      </form>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- Select2 -->
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var temp = '<?php echo $areanya; ?>';
    temp = $.parseJSON(temp);

    /*var areanya = '<?php echo $temp; ?>';*/

    $('#area').select2({ data: temp });
    $('#area').val([<?php echo $temp; ?>]).trigger('change');
    /*$('#temp').select2({ data: areanya });*/
    });
</script>