<h1>Edit Group</h1>
<p><?php echo lang('edit_group_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open(current_url());?>

      <p>
            Group Name <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            Group Description <br />
            <?php echo form_input($group_description);?>
      </p>

      <p>SUBMIT <?php echo form_submit('submit', lang('edit_group_submit_btn'));?></p>

<?php echo form_close();?>