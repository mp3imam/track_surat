<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
            <div class="login-logo">
                <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAL30lEQVR4nO2aa3BU5RnHf+85u9lsLuYeAkggYAhXgzcujlRFEKsVpDOlamsVb3REvIBO7Ywz0g/2QytQFVulU6DS2hYrigVajLFY6wWCBUwgXCQJuZHL5r67ye6ec95+WEM2m2x2szmbOh3/M5kk5zznOc//ub3nfc4RRIgXH389VzP0l5DcCNQCG9ZvWfVmpNd/XSEiEfrFY9vzLVJ8KKUcG3BYCilXrXvl/t/HyLZRgRJOIJD8pXmpPPjMtSy8dQqAkEJs3bR2+8LYmxk7DOmAYPK33zMLe6KVlNR44uJUgDhD8vbGtb+/YnTMNR8hHTAYeYtVBeBUaRNerw6AgAyk8f7mNduuHB2TzcWgDhiKPMDCW6aQkmEHQAgBkG4IUbRxzY6bRsNoM6EGHwhFXvPpKKrfX3E2C1NmZNLTrXHdLVPQNYPWJrcdwT23zF2RsmDG8g8PHtujjTqbKNBvFQhF3tXlYefmEnJyL+E7P5jZLxsAkHD4n+c5dLAKJAAnpZDPOTOqd2/YsMEYLTLR4CKTIdNeCE5+3oCjwUlzvZOCwjH9tQgYn5dK7mXpNNV1SbfTly0QK23u1LtunrvCu+TK79YUHXnbNZrEIoWA8DUP0NHSze7tx5ESVj01r7f2B8AwJF98Vsexj+tkV2dPr5AEjgp4D6kcFoZ+/InfrKoUCBlLcpFAvPj467m6YXwWTL6provaynYKF1yKqvp5aD4DQ8reJXBIGIakotxBWckF6s93oGtBlSDoAnEc5HHguNQ5lqx5T6zeutodA54hITat3f6ulNweHPn9fz7BuRMO8qZl8O07Z150QjTQfDp1VR3UV7bTdMFJS4MLl9M7mKgOnAVxBEGRYVPeevqFH8W0dMSmx3Z0SkMmr3pqPkkptosnHA0u3tlxnG6Xjxtuz2f23HGm3tjt9NHS6MTR4MLR4MTR6KKt0YVu9FWFELRLQzy77pV7fx2rchGb1m4vl5Jpi5ZPJf/y7H7p3dLo4ugntVy9cAKpmQnm3VUy6C7E0CWtzW4aajr4sqyZmop2v5Gw2yK67n7s5cc85hnhh9i0ZsdKKeRfVIuCNCRTL89m8YoChBJ9ypuF6i/bKHqr3HA7fQrIdyc0Jn135ZsrdTPvoR4oeefE0muWp0vJPCn9qd/Z4WHK9Ewz7xMVUtLtTJmeKc6WNus+nzG9K8mbc+Dwnr1m3kMB6MqqXo8Qxb0Hz5U1I42RlZwQCkKEXy3CISXDzrJ7Z6tWq6pLxMMbH93+3IiVBkAB2LBhgyYs2kohRCXA+MmpIyoBRbVhsaWgWO2mGJk1Nolb7pqhKkJIYMMLa7Y9aIpiAjZD6zY/2IrB7UIIV9XpFo5+XBuVQqGoqNYEpAHFu8s4ceSCKYZOyk/nhmX5AkAI5Tcb12z7jhl6++XogZJ3mm+ee0eZgDtrKtrFmEuTSc0YXhRVix2hWOl2edn/xyNcqO5g1pwUdK8L3dsd8Y/h86B7ezC8HpAGisVK9rhkpC6pP9+hIMQdS65ZXlxUsqfONAcAvHf4ndM3z12hIeWiylMtTJ6egT0xLmKFimpDKCrWOAvlR2txdvYwbpzAbtMxfN5h/Hj8TvD1oPW4kD4PanwiEyan0dnWg6PBZUWIFbcuWLbnH4febYnWAYPOA9ZtuffnUshdXo/O/j+dxNMT+c5Wyr5VKjc/C4D62u5o7bsIzduNz90BAhYtn8r4vFQpIEPXlb//6vHfjgmvYXCE7HSvPfxagssW94mUFOZNTee2H84KuQHqp1BRscSlAHCmtI69rx+O1rbhoE0gdlhE50+H+7AUciS2eutqt6qoy4QiHJVnWvmsqDIihdLQkYY/Cyblj8FiDTt3NQNpEvmk10h6frgXhg3pprXbFyIplgLr0u9NZ+rs7LBKFdWKak0GYP8bRzh1tIa0mZOZsHT+cO0jza6Sbve3qtZunbZufcDxlvON/Ou3fwdoWL9l1dgQqga3NZzAupdXfWQgHkVC8e7TNNU7wyo1dB+G7s/E+YsLUBSFtpMVOM83DMe2fiTbQpAHyJh4sQXkDOsGROAAgKe23LcVwSuaZrDvjTLcg29l+0H3uZGGj/TsZOYvLgAJ1X/7F+6GyBr2rGwrGQl95FtDkB8pIi7QJI/3SQEHnR0e9v/pBLoe7lFZonmdGIaXeTcVkD97HJpXo+qvxXRVDf1wNDPLSk6iICcBOnvCk+/NjGgQsSv3fr7XWDLn1n1qnHpXV7snxdXpYXIEGyapexGKytTLJ9DucNNU107H6WqsifHYx6QPkE+zq8RZFOJVONeu0+jyk6vaVcSZfZ9y6oNjNJ+7wMSr8oG+7Gj8tBSA9w7v+VmknGAYGQDw1NbVDqlzG0L0nPxPA59GuDLoPhfg5da7r2bujVORhkFt0WEa/n28d4oM9EVYSjjVonOhqy/yVnWgqYGlES2i2vH8cu2O2xQp9wLMWzSJuTdMjEiTYrGhWhIpPVRF8e7jGIZB6rRJTFg6j/SkuLDdPhCBMgBfbHoDgPVbVg2LU1SL9NMv37ev9+9DH1TxjzfL0XzhI2FoHnSfi9nzJrLigQXY7FbaT1VRv/9j0mx+u4fq9r1oCyJvG8G8csRPKZY4C2dLm9j16lHqz3eElTd0D5rPxcT8bFb+eCH2RBstZ2s5fbA0om4fnPY2VTAu2Rq1/SN2QP5dS0nJSqKlycVbvztG0e7TtDuGnmxL3Yvm6yRr7CUsu3ceCDj77zKaWv0D4MHIf7h1P28/u4ODz+/k3F/eB/rIKyNgMWIHWDNSyL37NsbOn4miqJw62sDOl0r4285SKssdIZdLaWhovi7G56WTNy0HzevDWXUhZOSD1ZhBHsAyssv9EKpK1rWFpM7Op+lQGW0nK6k600rVmVbi7VYum5VFQWE243JT+jVLaWgYupfc/CwqyxvQHW0h037i9xZf/N8s8mCSA3phTU5g/OK55FxXSNuJKtrKK+huaqOspJ6yknqSUmxMuzybqXPGkJGdCPh7Qny8v4bjBiEUqubNIA8mO6AXaryNzKsKyLyqgJ6WDtpPVtJ2qgpnh5sjH9Vw5KMaMnMSKSgcQ25+JhXl/j2CNb7/4CXW5CFGDghEfEYKOQvnMG3JlRiNDmqOnqPu5Pmv3ghV8PGBCgBUq4WJV1x28bpg8hV/LsJZ3wz4Nz/feujbptgXcwdAQFfPy8GSk0XydVfSda4O19nzdDvaSUxLYvpNV5CUeQkweORtVoXw+9DhI+YOCN7StnbrKBaVSXPySF9w2QD5UGl/qUkRD0ZMxzWDkQ8+HojRqPlgxEx1KPJVu4oo+8N7A+T/F+QhBiUggJnZVrq1vmf7wMh/Uds84JpIyH+4dT+t1U2AuU3QVP8KATO+GmYkWc1N+0gm0tHA1AzoneQAZCZAs9v//BpIPmB+N6y0NyviwTDNAWl2lW5N0Oz2ky+p99HpkQMi30skFPnMBGjtMcuq8DDFAYEkazp1mt3yIvnSnQcQQvSLYCjyyTZIsYFHB5fPDMvCY8QOCLfU9TauXnzw6j40KZjyff/mJpB8Rrxfpvd3oBNi1QRH7IDhrvMdAatAYNoHfJ+FqkB2AnR4+srha90ER9rtW3v8aZ8R7yevG9DS0z8DYtUER7wMRvrSInh6G9ztXT4/aQBHz9ekB2x8ZNsEFLEZyc0IkgeTifaNzWBLncvnT3v3CMhvfHR7/9mRpAtBsaoazzzx4gOng+VDOuAr8seA9HAj70jf2EQyvQ21BEbdBP2Bu0PXles3PrKtcP2v768JPB06AxSxGUgnwcDI1MDS37FKhb9rjdb0NtImaEwO+jxAA8VhBbeShiI2AisDT4d2gD/tMTJ9QxZKOPKJ47MRoq/mA58Eh4Oom6AFjEwNpToOYOkgp81FcORn3L2kX8OLVTePEANG1KFXAUExfJU+EX4i1DpI2o+9JPZb2iGhCYTDH2cp5IHg0yEzQFWNZ3RduR63kqZU20KJ8fazO0yw0jz09qZgSGjR0dcPkA+l6IkXHziNIQuBN4FO0ywcfXRKIXfpQpvzk5cfiu7rz2/wDb7B/y3+CzWh0lXi6lthAAAAAElFTkSuQmCC' />
            </div>

            <div class="login-box-body">
                <p class="login-box-msg"><?php echo lang('auth_sign_session'); ?></p>
                <?php echo $message;?>

                <?php echo form_open('auth/login');?>
                    <div class="form-group has-feedback">
                        <?php echo form_input($identity);?>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <?php echo form_input($password);?>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?><?php echo lang('auth_remember_me'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <?php echo form_submit('submit', lang('auth_login'), array('class' => 'btn btn-primary btn-block btn-flat'));?>
                        </div>
                    </div>
                <?php echo form_close();?>

<?php if ($auth_social_network == TRUE): ?>
                <div class="social-auth-links text-center">
                    <p>- <?php echo lang('auth_or'); ?> -</p>
                    <?php echo anchor('#', '<i class="fa fa-facebook"></i>' . lang('auth_sign_facebook'), array('class' => 'btn btn-block btn-social btn-facebook btn-flat')); ?>
                    <?php echo anchor('#', '<i class="fa fa-google-plus"></i>' . lang('auth_sign_google'), array('class' => 'btn btn-block btn-social btn-google btn-flat')); ?>
                </div>
<?php endif; ?>
<?php if ($forgot_password == TRUE): ?>
                <?php echo anchor('#', lang('auth_forgot_password')); ?><br />
<?php endif; ?>
<?php if ($new_membership == TRUE): ?>
                <?php echo anchor('#', lang('auth_new_member')); ?><br />
<?php endif; ?>
            </div>
