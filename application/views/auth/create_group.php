<h1>Create Group</h1>
<p><?php echo lang('create_group_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_group");?>

      <p>
            Group Name: <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            Group Description: <br />
            <?php echo form_input($description);?>
      </p>

      <p>SUBMIT <?php echo form_submit('submit', lang('create_group_submit_btn'));?></p>

<?php echo form_close();?>