<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/clearable//input.clearable.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"> Aktifitas MUT Detail List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>

			<div class="box-body">
				<table class="table table-striped table-condensed" style="margin-bottom: 0;">
					<tr>
						<td style="padding-bottom: 0;">
							<div class="col-sm-4 col-lg-3">
								<div class="form-group">
									<label for="das-storename"><?php echo "Store Name";?></label>
									<input class="form-control clearable" id="das-storename">
								</div>
							</div>
							<div class="col-sm-2 col-lg-2">
								<div class="form-group">
									<label for="das-bulan"><?php echo "Bulan";?></label>
									<select id="das-bulan" class="form-control select2" data-placeholder="Filter by bulan"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-md-2 col-lg-2">
								<div class="form-group">
									<label for="das-tahun"><?php echo "Tahun";?></label>
									<select id="das-tahun" class="form-control select2" data-placeholder="Filter by tahun"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-sm-2">
								<button id="btnSearch" type="button" class="btn btn-default" style="margin-top:25px; width: 100%;">Search</button>
							</div>
						</td>
					</tr>
				</table>
			</div>

			<div class="box-body" style="overflow-x:scroll;white-space: nowrap;">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="120%" cellspacing="0" id="graphdtlTable">
					<thead>
						<tr>
							<th style="width: 30px;"><?php echo 'Tanggal Login';?></th>
							<th style="width: 150px;"><?php echo 'Business Partner';?></th>
							<th style="width: 150px;"><?php echo 'MUT Name';?></th>
							<th style="width: 50px;"><?php echo 'MUT Type';?></th>
							<th style="width: 150px;"><?php echo 'Store Name';?></th>
							<th style="width: 50px;"><?php echo 'ID QRCode';?></th>
							<th style="width: 50px;"><?php echo 'Cluster Name';?></th>
							<th style="width: 100px;"><?php echo 'Area';?></th>
							<th style="width: 30px;"><?php echo 'Status';?></th>
							<th style="width: 50px;"><?php echo 'Jam Check In';?></th>
							<th style="width: 50px;"><?php echo 'Jam Check Out';?></th>
							<th style="width: 50px;"><?php echo 'Durasi Kerja';?></th>
							<th style="width: 50px;"><?='Foto1'?></th>
							<th style="width: 50px;"><?php echo 'Foto2';?></th>
							<th style="width: 50px;"><?php echo 'Foto3';?></th>
							<th style="width: 50px;"><?php echo 'Foto4';?></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		<input id="hdParam" type="hidden" value="<?php echo $param; ?>">
	</section>
	<!-- /.content -->
</div>

<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/clearable/input.clearable.js') ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
	// add months
	var month_data = '<?php echo $mut_months; ?>';
	month_data = $.parseJSON(month_data);
	// add years
	var year_data = '<?php echo $mut_years; ?>';
	year_data = $.parseJSON(year_data);
	//Initialize Select2 Elements
	$('#das-bulan').html('').select2({ data: month_data });
	$('#das-tahun').html('').select2({ data: year_data });
	$('.select2').select2();

	var oTable;

    $(document).ready(function() {
      	$('#graphdtlTable thead tr').clone(true).appendTo( '#graphdtlTable thead' );
    	$('#graphdtlTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );

	$(document).ready(function() {
		oTable = $('#graphdtlTable').DataTable({
			"buttons": [
				{
					text: 'Export to Excel',
					action: function ( e, dt, node, config ) {
						location.href = "<?php echo site_url("report/export_excel_detail/" . $param) ?>?" + "<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&mtype=" + "&das-storename=" + $('#das-storename').val() + "&das-bulan=" + $('#das-bulan').val() + "&das-tahun=" + $('#das-tahun').val();
					}
				}
			],
			"dom": 'Brt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			"lengthMenu": [[10], [10]],
			"processing": true,
			"serverSide": true,
			"searching": true,
			"order": [[0, 'asc'],[1, 'asc'],[2, 'asc'],[3, 'asc'],[4, 'asc'],[5, 'asc'],[6, 'asc'],[7, 'asc'],[8, 'asc'],[9, 'asc'],[10, 'asc'],[11, 'asc'],[12, 'asc']],
			"ajax": {
			type: "POST",
			url: "<?php echo site_url('report/get_graph_detail_list/') ?>"+$('#hdParam').val(),
			data: function ( d ) {
				return $.extend( {}, d, {
					"<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>",
					"das-storename": $("#das-storename").val(),
					"das-bulan": $("#das-bulan").val(),
					"das-tahun": $("#das-tahun").val(),
				});
			},
			},
			"cache": false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"columns": [
			{
				data: "tanggal",
				orderable: true
			}, {
				data: "b.bp_name",
				orderable: true
			}, {
				data: "mut_name",
				orderable: true
			}, {
				data: "mut_type_name",
				orderable: true
			}, {
				data: "store_name",
				orderable: true
			}, {
				data: "id_qrcode",
				orderable: false
			}, {
				data: "cluster_name",
				orderable: false
			}, {
				data: "area_name",
				orderable: false
			}, {
				data: "status_remarks",
				orderable: true
			}, {
				data: "jam_checkin",
				orderable: false
			}, {
				data: "jam_checkout",
				orderable: false
			}, {
				data: "durasi",
				orderable: false
			}, {
				data: "foto1",
				orderable: false,
				"mRender": function(data, type, row) {
				    if (data != "") {
				        data = "<?php echo $store_dir; ?>" + data;
                    } else {
				        data = "<?php echo base_url($images_dir) ?>/NO_Clip_Art.png";
                    }

					var htmlBtnAct = "<img src='"+ data +"' style='height:80px; width:100px' alt='' border='0'>";
					return htmlBtnAct;
				}
			}, {
				data: "foto2",
				orderable: false,
				"mRender": function(data, type, row) {
                    if (data != "") {
                        data = "<?php echo $store_dir; ?>" + data;
                    } else {
                        data = "<?php echo base_url($images_dir) ?>/NO_Clip_Art.png";
                    }

                    var htmlBtnAct = "<img src='"+ data +"' style='height:80px; width:100px' alt='' border='0'>";
					return htmlBtnAct;
				}
			}, {
				data: "foto3",
				orderable: false,
				"mRender": function(data, type, row) {
                    if (data != "") {
                        data = "<?php echo $store_dir; ?>" + data;
                    } else {
                        data = "<?php echo base_url($images_dir) ?>/NO_Clip_Art.png";
                    }

                    var htmlBtnAct = "<img src='"+ data +"' style='height:80px; width:100px' alt='' border='0'>";
					return htmlBtnAct;
				}
			}, {
				data: "foto4",
				orderable: false,
				"mRender": function(data, type, row) {
                    if (data != "") {
                        data = "<?php echo $store_dir; ?>" + data;
                    } else {
                        data = "<?php echo base_url($images_dir) ?>/NO_Clip_Art.png";
                    }

                    var htmlBtnAct = "<img src='"+ data +"' style='height:80px; width:100px' alt='' border='0'>";
					return htmlBtnAct;
				}
			}],
			bFilter: false
		});		
	});

	// Searching
	$('#btnSearch').on('click', function(e) {
		e.preventDefault();
		oTable.draw();
	});
</script>
