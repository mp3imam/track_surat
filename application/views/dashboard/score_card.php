<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<!-- /.header -->
	<section class="content">
		<div class="row">
			<div class="col-lg-4 col-xs-6">
				<div class="box">
					<div class="box-body">
						<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
							<tbody>
								<tr>
									<th colspan="2" style="text-align:center;">TOP 5 BP</th>
								</tr>
								<tr>
								<th style="text-align:center;">NAMA BP</th>
								<th style="text-align:center;">NILAI AKTIFITAS MUT</th>
								</tr>
								<tr>
									<td>Sanjayatama Lestari</td>
									<td style="text-align:center;">70%</td>
								</tr>
								<tr>
									<td>Gondari</td>
									<td style="text-align:center;">55%</td>
								</tr>
								<tr>
									<td>Techno</td>
									<td style="text-align:center;">40%</td>
								</tr>
								<tr>
									<td>Mitra Andal Sejati</td>
									<td style="text-align:center;">38%</td>
								</tr>
								<tr>
									<td>Wahana Inti Sejati</td>
									<td style="text-align:center;">35%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xs-6">
				<div class="box">
					<div class="box-body">
						<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
							<tbody>
								<tr>
									<th colspan="2" style="text-align:center;">TOP 5 MUT</th>
								</tr>
								<tr>
								<th style="text-align:center;">NAMA MUT</th>
								<th style="text-align:center;">NILAI AKTIFITAS MUT</th>
								</tr>
								<tr>
									<td>Doni Wijaya</td>
									<td style="text-align:center;">89%</td>
								</tr>
								<tr>
									<td>Sutrisno</td>
									<td style="text-align:center;">86%</td>
								</tr>
								<tr>
									<td>Zulfan</td>
									<td style="text-align:center;">82%</td>
								</tr>
								<tr>
									<td>Suhanda</td>
									<td style="text-align:center;">80%</td>
								</tr>
								<tr>
									<td>Wasto</td>
									<td style="text-align:center;">78%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-xs-6">
				<div class="box">
					<div class="box-body">
						<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
							<tbody>
								<tr>
									<th colspan="2" style="text-align:center;">TOP 5 AREA</th>
								</tr>
								<tr>
								<th style="text-align:center;">NAMA AREA</th>
								<th style="text-align:center;">NILAI AKTIFITAS MUT</th>
								</tr>
								<tr>
									<td>JMC</td>
									<td style="text-align:center;">60%</td>
								</tr>
								<tr>
									<td>Jambi</td>
									<td style="text-align:center;">53%</td>
								</tr>
								<tr>
									<td>Pontianak</td>
									<td style="text-align:center;">46%</td>
								</tr>
								<tr>
									<td>Maluku</td>
									<td style="text-align:center;">38%</td>
								</tr>
								<tr>
									<td>NTB</td>
									<td style="text-align:center;">35%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-xs-6">
				<div class="box">
					<div class="box-body">
						<table class="table table-striped table-bordered table-hover dt-responsive nowrap">
							<tbody>
								<tr>
									<th colspan="2" style="text-align:center;">TOP 5 AREA OFFICER</th>
								</tr>
								<tr>
								<th style="text-align:center;">NAMA AREA OFFICER</th>
								<th style="text-align:center;">NILAI AKTIFITAS MUT</th>
								</tr>
								<tr>
									<td>Barry Azis</td>
									<td style="text-align:center;">98%</td>
								</tr>
								<tr>
									<td>Riri Handika Macan</td>
									<td style="text-align:center;">97%</td>
								</tr>
								<tr>
									<td>Abdul Rahman</td>
									<td style="text-align:center;">96%</td>
								</tr>
								<tr>
									<td>Zainudin Boer</td>
									<td style="text-align:center;">95%</td>
								</tr>
								<tr>
									<td>Johaness Lakburiawal</td>
									<td style="text-align:center;">94%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<div class="form-group">
					<select class="form-control select2" multiple="multiple" data-placeholder="Filter by category"
							style="width: 100%;">
					<option>MUT GT</option>
					<option>MUT FG</option>
					<option>MUT IC</option>
					</select>
				</div>
			</div>
			<div class="col-lg-2 col-xs-2">
				<div class="form-group">
					<select class="form-control select2" data-placeholder="Filter by bulan"
							style="width: 100%;">
					<option>Januari</option>
					<option>Februari</option>
					<option>Maret</option>
					<option>April</option>
					<option>Mei</option>
					<option>Juni</option>
					<option>Juli</option>
					<option>Agustus</option>
					<option>September</option>
					<option>Oktober</option>
					<option>November</option>
					<option>Desember</option>
					</select>
				</div>
			</div>
			<div class="col-lg-2 col-xs-2">
				<div class="form-group">
					<select class="form-control select2" data-placeholder="Filter by tahun"
							style="width: 100%;">
					<option>2016</option>
					<option>2017</option>
					<option>2018</option>
					</select>
				</div>
			</div>
			<div class="col-lg-2 col-xs-2">
				<button type="button" class="btn btn-block btn-default btn-flat pull-right" style="margin-right: 5px;">Search</button>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<!-- Select2 -->
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>

<script type="text/javascript">
	$(function () {
		//Initialize Select2 Elements
		$('.select2').select2();
	});
</script>
