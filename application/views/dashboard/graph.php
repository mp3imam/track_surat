<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">
<style type="text/css">
	#target {
		width: 200px;
		height: 100px;
	}
	#target_2 {
		width: 200px;
		height: 100px;
	}
	#target_3 {
		width: 200px;
		height: 100px;
	}
</style>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Small boxes (Stat box) -->
			<div class="row">
				<div id="divGt" class="col-lg-4 col-xs-6">
					<div id="gtLoadMsg" style="text-align: center;margin-bottom:30px;display:none">
						<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />
					</div>
				</div>
				<!-- ./col -->
				<div id="divFg" class="col-lg-4 col-xs-6">
					<div id="fgLoadMsg" style="text-align: center;margin-bottom:30px;display:none">
						<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />
					</div>
				</div>
				<!-- ./col -->
				<div id="divIc" class="col-lg-4 col-xs-6">
					<div id="icLoadMsg" style="text-align: center;margin-bottom:30px;display:none">
						<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />
					</div>
				</div>
				<!-- ./col -->
			</div>
			<div class="row">
				<div class="col-md-4">
					<a href="<?php echo site_url("ReportSummaryCico/report_summary_cico_mut") ?>" class="small-box-footer" target="_blank">
						<div class="box box-solid">
							<div class="box-header">
								<h3 class="box-title text-black">Registrasi Toko</h3>
							</div>
							<!-- /.box-header -->
							<div id="divRegisToko" class="box-body">
								<div id="regisTokoLoadMsg" style="text-align: center;margin-bottom:30px;display:none">
									<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</a>
				</div>
				<!-- /.col -->
				<div class="col-md-4">
					<a href="<?php echo site_url("ReportSummaryCico/report_summary_cico_mut") ?>" class="small-box-footer" target="_blank">
					<div class="box box-solid">
						<div class="box-header">
							<h3 class="box-title text-black">Aktifitas</h3>
						</div>
						<!-- /.box-header -->
						<div id="divActivity" class="box-body">
							<div id="activityLoadMsg" style="text-align: center;margin-bottom:30px;display:none">
								<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
					</a>
				</div>
				<!-- /.col -->
				<div class="col-md-4" style="display:none">
					<a href="<?php echo site_url("ReportSummaryCico/report_summary_cico_mut") ?>" class="small-box-footer" target="_blank">
					<div class="box box-solid">
						<div class="box-header">
							<h3 class="box-title text-black">Time Motion</h3>
						</div>
						<!-- /.box-header -->
						<div id="divTimeMotion" class="box-body">
							<div id="timeMotionLoadMsg" style="text-align: center;margin-bottom:30px;display:none">
								<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
					</a>
				</div>
				<!-- /.col -->
			</div>

		<div class="box">
			<div class="box-body">
				<i class="fa fa-search"></i> Search Filter
			</div>
			<!-- /.box-body -->
			<div class="row">
				<div class="col-md-2">
					<div class="box box-solid">
						<div class="form-group">
							<select id="das-muttype" class="form-control select2" multiple="multiple" data-placeholder="Filter by category"
									style="width: 100%;">
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<select id="das-bparea" class="form-control select2" multiple="multiple" data-placeholder="Filter by area"
								style="width: 100%;">
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<select id="das-bulan" class="form-control select2" data-placeholder="Filter by bulan"
								style="width: 100%;">
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<select id="das-tahun" class="form-control select2" data-placeholder="Filter by tahun"
								style="width: 100%;">
						</select>
					</div>
				</div>
				<div class="col-md-2 col-xs-2">
					<button id="btnSearch" type="button" class="btn btn-block btn-info pull-left" style="margin-right: 5px;">Search</button>
				</div>
				<div class="col-md-2 col-xs-2">
					<button id="btnDownload" type="button" class="btn btn-block btn-info pull-right" style="margin-right: 5px;">Download</button>
				</div>
			</div>
			<div class="row">
				<!-- Modal dialog popup !-->
				<div class="modal fade" id="modal-fg">
				</div>
				<div class="modal fade" id="modal-gt">
				</div>
				<div class="modal fade" id="modal-ic">
				</div>
				<!-- End Modal dialog popup !-->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>

<!-- Jquery Pie Chart -->
<script src="<?php echo base_url($plugins_dir . '/chart.js/Chart.min.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>

<script type="text/javascript">

	function load_modal_graph(id_mut_type)
	{
		var mut_type = '';
		var modalid = '';
		switch(id_mut_type)
		{
			case 1:
				mut_type = 'MUT FG';
				modalid = 'tbl-modal-fg';
				break;
			case 2:
				mut_type = 'MUT GT';
				modalid = 'tbl-modal-gt';
				break;
			default:
				mut_type = 'MUT IC';
				modalid = 'tbl-modal-ic';
				break;
		}
		
		var divModal = '<div class="modal-dialog">';
		divModal	+= '<div class="modal-content">';
		divModal	+= '<div class="modal-header">';
		divModal	+= '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
		divModal	+= '<span aria-hidden="true">&times;</span></button>';
		divModal	+= '<h4 class="modal-title">'+ mut_type +'</h4>';
		divModal	+= '</div>';
		divModal	+= '<div class="modal-body">';
		divModal	+= '<table id="'+ modalid +'" class="table table-bordered">';
		divModal	+= '<tbody>';
		divModal	+= '<tr>';
		divModal	+= '<th>BP Area</th>';
		divModal	+= '<th>Alokasi</th>';
		divModal	+= '<th>Current</th>';
		divModal	+= '</tr>';
		divModal	+= '</tbody>';
		divModal	+= '</table>';
		divModal	+= '</div>';
		divModal	+= '<div class="modal-footer">';
		divModal	+= '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>';
		divModal	+= '<button type="button" class="btn btn-success pull-right" onclick="download_alokasi_area_graph('+ id_mut_type +')" >Download</button>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '</div>';
		divModal	+= '<!-- End Modal dialog popup !-->';

		return divModal;
	}

	function download_alokasi_area_graph(type_mut) {
    	window.location.href = "<?php echo site_url("Dashboard/export_alokasi_area_graph/") ?>" + type_mut;
	}

	function load_alokasi_graph()
	{
		// Load modal popup
		$('#modal-fg').html(load_modal_graph(1));
		$('#modal-gt').html(load_modal_graph(2));
		$('#modal-ic').html(load_modal_graph(3));

		// Ajax start animation progress
		$('#gtLoadMsg').show();
		$('#fgLoadMsg').show();
		$('#icLoadMsg').show();

		$.ajax({
			type: "POST",
			url: "<?php echo  base_url().ADD_INDEXPHP;?>dashboard/graph_alokasi_view",
			data: {
				"<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
			},
			dataType: "json",
			cache: false,
			success: function(data)
			{
				var res = data;

				// Load modal popup data
				
				//Try to get tbody first with jquery children. works faster!
				var tbody = $('#tbl-modal-fg').children('tbody');
				//Then if no tbody just select your table 
				var table = tbody.length ? tbody : $('#tbl-modal-fg');

				var resObj = '';
				for (var item in res.result_areas_fg) {
					resObj = res.result_areas_fg[item];
					//Add row
					table.append('<tr><td>'+ resObj.bp_area +'</td><td style="text-align:center;">'+ resObj.total_mut_fg +'</td><td style="text-align:center;">'+ resObj.total_current_fg +'</td></tr>');
				}

				//Try to get tbody first with jquery children. works faster!
				tbody = null;
				var tbody = $('#tbl-modal-gt').children('tbody');
				//Then if no tbody just select your table 
				table = null;
				var table = tbody.length ? tbody : $('#tbl-modal-gt');

				resObj = '';
				for (var item in res.result_areas_gt) {
					resObj = res.result_areas_gt[item];
					//Add row
					table.append('<tr><td>'+ resObj.bp_area +'</td><td style="text-align:center;">'+ resObj.total_mut_gt +'</td><td style="text-align:center;">'+ resObj.total_current_gt +'</td></tr>');
				}

				//Try to get tbody first with jquery children. works faster!
				tbody = null;
				var tbody = $('#tbl-modal-ic').children('tbody');
				//Then if no tbody just select your table 
				table = null;
				var table = tbody.length ? tbody : $('#tbl-modal-ic');

				var resObj = '';
				for (var item in res.result_areas_ic) {
					resObj = res.result_areas_ic[item];
					//Add row
					table.append('<tr><td>'+ resObj.bp_area +'</td><td style="text-align:center;">'+ resObj.total_mut_ic +'</td><td style="text-align:center;">'+ resObj.total_current_ic +'</td></tr>');
				}

				var divGt_load = '<!-- small box -->';
				divGt_load += '<div class="small-box bg-aqua">';
				divGt_load += '<div class="inner">';
				divGt_load += '<h2>MUT GT</h2>';
				divGt_load += '<p>Alokasi: ' + res.total_mut_gt + '</p>';
				if (parseInt(res.total_mut_gt) > parseInt(res.total_current_gt)) {
					divGt_load += '<p>Current: <span class="text-red text-bold">' + res.total_current_gt + '</span></p>';
				}
				else {
					divGt_load += '<p>Current: <span class="text">' + res.total_current_gt + '</span></p>';
				}
				divGt_load += '</div>';
				divGt_load += '<div class="icon">';
				divGt_load += '<i class="ion ion-bag"></i>';
				divGt_load += '</div>';
				divGt_load += '<a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-gt">More info <i class="fa fa-arrow-circle-right"></i></a>';
				divGt_load += '</div>';

				$('#divGt').html(divGt_load);

				var divFg_load = '<!-- small box -->';
				divFg_load += '<div class="small-box bg-green">';
				divFg_load += '<div class="inner">';
				divFg_load += '<h2>MUT FG</h2>';
				divFg_load += '<p>Alokasi: '+ res.total_mut_fg +'</p>';
				if (parseInt(res.total_mut_fg) > parseInt(res.total_current_fg)) {
					divFg_load += '<p>Current: <span class="text-red text-bold">' + res.total_current_fg + '</span></p>';
				}
				else {
					divFg_load += '<p>Current: <span class="text">' + res.total_current_fg + '</span></p>';
				}
				divFg_load += '</div>';
				divFg_load += '<div class="icon">';
				divFg_load += '<i class="ion ion-bag"></i>';
				divFg_load += '</div>';
				divFg_load += '<a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-fg">More info <i class="fa fa-arrow-circle-right"></i></a>';
				divFg_load += '</div>';

				$('#divFg').html(divFg_load);

				var divIc_load = '<!-- small box -->';
				divIc_load += '<div class="small-box bg-light-blue">';
				divIc_load += '<div class="inner">';
				divIc_load += '<h2>MUT IC</h2>';
				divIc_load += '<p>Alokasi: '+ res.total_mut_ic +'</p>';
				if (parseInt(res.total_mut_ic) > parseInt(res.total_current_ic)) {
					divIc_load += '<p>Current: <span class="text-red text-bold">' + res.total_current_ic + '</span></p>';
				}
				else {
					divIc_load += '<p>Current: <span class="text">' + res.total_current_ic + '</span></p>';
				}
				divIc_load += '</div>';
				divIc_load += '<div class="icon">';
				divIc_load += '<i class="ion ion-icecream"></i>';
				divIc_load += '</div>';
				divIc_load += '<a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-ic">More info <i class="fa fa-arrow-circle-right"></i></a>';
				divIc_load += '</div>';

				$('#divIc').html(divIc_load);
				
				// Ajax stop animation progress
				$('#gtLoadMsg').hide();
				$('#fgLoadMsg').hide();
				$('#icLoadMsg').hide();
				//alert(res.msg);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('Data tidak dapat diproses..! Error is occurs');
				// Ajax stop animation progress
				$('#gtLoadMsg').hide();
				$('#fgLoadMsg').hide();
				$('#icLoadMsg').hide();
			}
		});
	}

	function load_pie_graph()
	{
		// Ajax start animation progress
		$('#pieRegisToko').hide();
		$('#regisTokoLoadMsg').show();
		$('#pieActivity').hide();
		$('#activityLoadMsg').show();
		$('#pieTimeMotion').hide();
		$('#timeMotionLoadMsg').show();

		// Registrasi Toko, Aktifitas & Time Motion
		$.ajax({
			type: "POST",
			url: "<?php echo  base_url().ADD_INDEXPHP;?>dashboard/graph_pie_view",
			data: {
				"<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>",
				"idmuttype": $('#das-muttype').val(),
				"idarea": $('#das-bparea').val(),
				"regisdate": $('#das-bulan').val() + $('#das-tahun').val()
			},
			dataType: "json",
			cache: false,
			success: function(data)
			{
				var res = data;

				// Registrasi Toko
				var regisData = {
					labels: ["Comply(" + res.total_regis_comply + "%)", "Uncomply" + "(" + res.total_regis_uncomply + "%)"],
					datasets: [{
						label: "Status",
						data: [res.total_regis_comply, res.total_regis_uncomply],
						backgroundColor: ["#144FFF", "#ED855C"]
					}]
				};
				
				var divRegis_load = '<div id="regisTokoLoadMsg" style="text-align: center;margin-bottom:30px;display:none">';
				divRegis_load += '<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />';
				divRegis_load += '</div>';
				divRegis_load += '<canvas id="pieRegisToko" height="200"></canvas>';
				
				$('#divRegisToko').html(divRegis_load);

				var regisCtx = $("#pieRegisToko");
				var chartInstance = new Chart(regisCtx, {
					type: 'pie',
					data: regisData,
					options: {
						title: {
							display: true,
							fontsize: 12,
							text: ''
						},
						legend: {
							display: true,
							position: 'right',
							labels: {
								generateLabels: function(chart) {
									var data = chart.data;
									if (data.labels.length && data.datasets.length) {
										return data.labels.map(function(label, i) {
											var meta = chart.getDatasetMeta(0);
											var ds = data.datasets[0];
											var arc = meta.data[i];
											var custom = arc && arc.custom || {};
											var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
											var arcOpts = chart.options.elements.arc;
											var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
											var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
											var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

											// We get the value of the current label
											var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

											return {
												// Instead of `text: label,`
												// We add the value to the string
												text: label,
												fillStyle: fill,
												strokeStyle: stroke,
												lineWidth: bw,
												hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
												index: i
											};
										});
									} else {
										return [];
									}
								}
							}
						}
					}
				});

				// Aktifitas
				var ActivityData = {
					labels: ["Comply" + "("+res.total_activity_comply+"%)", "Uncomply"+ "("+res.total_activity_uncomply+"%)"
							, "Meeting"+ "("+res.total_activity_meeting+"%)", "Persiapan Visit"+ "("+res.total_activity_persiapan_visit+"%)"
							, "Toko Tutup"+ "("+res.total_activity_toko_tutup+"%)", "Cuti"+ "("+res.total_activity_cuti+"%)", "Izin"+ "("+res.total_activity_izin+"%)", "Sakit"+ "("+res.total_activity_libur+"%)"],
					datasets: [{
						label: "Status",
						data: [res.total_activity_comply, res.total_activity_uncomply, res.total_activity_meeting, res.total_activity_persiapan_visit, res.total_activity_toko_tutup, res.total_activity_cuti, res.total_activity_izin, res.total_activity_libur],
						backgroundColor: ["#144FFF", "#ED855C", "#8b8e99", "#c69c2f", "#477c62", "#ed1750", "#f4386a", "#f7799a"]
					}]
				};
				
				var divActivity_load = '<div id="activityLoadMsg" style="text-align: center;margin-bottom:10px;display:none">';
				divActivity_load += '<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />';
				divActivity_load += '</div>';
				divActivity_load += '<canvas id="pieActivity" height="200"></canvas>';

				$('#divActivity').html(divActivity_load);

				var activityCtx = $("#pieActivity");
				var chartInstance = new Chart(activityCtx, {
					type: 'pie',
					data: ActivityData,
					options: {
						title: {
							display: true,
							fontsize: 12,
							text: ''
						},
						legend: {
							display: true,
							position: 'right',
							labels: {
								generateLabels: function(chart) {
									var data = chart.data;
									if (data.labels.length && data.datasets.length) {
										return data.labels.map(function(label, i) {
											var meta = chart.getDatasetMeta(0);
											var ds = data.datasets[0];
											var arc = meta.data[i];
											var custom = arc && arc.custom || {};
											var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
											var arcOpts = chart.options.elements.arc;
											var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
											var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
											var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

											// We get the value of the current label
											var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

											return {
												// Instead of `text: label,`
												// We add the value to the string
												text: label,
												fillStyle: fill,
												strokeStyle: stroke,
												lineWidth: bw,
												hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
												index: i
											};
										});
									} else {
										return [];
									}
								}
							}
						}
					}
				});

				// Time Motion
				var TimeMotionData = {
					labels: ["Comply", "Uncomply"],
					datasets: [{
						label: "Status",
						data: [20, 80],
						backgroundColor: ["#144FFF", "#ED855C"]
					}]
				};
				
				var divTimeMotion_load = '<div id="timeMotionLoadMsg" style="text-align: center;margin-bottom:30px;display:none">';
				divTimeMotion_load += '<img src="<?php echo base_url($images_dir . '/loading.gif'); ?>" />';
				divTimeMotion_load += '</div>';
				divTimeMotion_load += '<canvas id="pieTimeMotion" height="200"></canvas>';

				$('#divTimeMotion').html(divTimeMotion_load);

				var timeMotionCtx = $("#pieTimeMotion");
				var chartInstance = new Chart(timeMotionCtx, {
					type: 'pie',
					data: TimeMotionData,
					options: {
						title: {
							display: true,
							fontsize: 12,
							text: ''
						},
						legend: {
							display: true,
							position: 'bottom',
							labels: {
								generateLabels: function(chart) {
									var data = chart.data;
									if (data.labels.length && data.datasets.length) {
										return data.labels.map(function(label, i) {
											var meta = chart.getDatasetMeta(0);
											var ds = data.datasets[0];
											var arc = meta.data[i];
											var custom = arc && arc.custom || {};
											var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
											var arcOpts = chart.options.elements.arc;
											var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
											var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
											var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

											// We get the value of the current label
											var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

											return {
												// Instead of `text: label,`
												// We add the value to the string
												text: label,
												fillStyle: fill,
												strokeStyle: stroke,
												lineWidth: bw,
												hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
												index: i
											};
										});
									} else {
										return [];
									}
								}
							}
						}
					}
				});
				
				// Ajax stop animation progress
				$('#regisTokoLoadMsg').hide();
				$('#pieRegisToko').show();
				$('#activityLoadMsg').hide();
				$('#pieActivity').show();
				$('#timeMotionLoadMsg').hide();
				$('#pieTimeMotion').show();
				//alert(res.msg);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				// Debugging the JSON object in console
				//console.log(xhr.responseText);
				//alert('Tidak ada data yang dapat diproses..!');
				// Ajax stop animation progress
				$('#regisTokoLoadMsg').hide();
				$('#activityLoadMsg').hide();
				$('#timeMotionLoadMsg').hide();
			}
		});
	}

	function search_pie_graph()
	{
		load_pie_graph();
	}

	function download() {
	    location.href = "<?php echo site_url("dashboard/export_excel_dashboard") ?>?" + "<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&das-muttype=" + $('#das-muttype').val() + "&das-bparea=" + $('#das-bparea').val() + "&das-bulan=" + $('#das-bulan').val()+ "&das-tahun=" + $('#das-tahun').val();
	}

	$(function() {
		// Delay time
		setTimeout(load_alokasi_graph, 1000);
		setTimeout(load_pie_graph, 2000);

		// Search
		$('#btnSearch').on('click', function(e)
		{
			e.preventDefault();
			search_pie_graph();
		});

		// Search
		$('#btnDownload').on('click', function(e)
		{
			e.preventDefault();
			download();
		});
		
		// add data mut category
		var ctg_data = '<?php echo $mut_types; ?>';
		ctg_data = $.parseJSON(ctg_data);
		// add data bp area
		var area_data = '<?php echo $bp_areas; ?>';
		area_data = $.parseJSON(area_data);
		// add months
		var month_data = '<?php echo $mut_months; ?>';
		month_data = $.parseJSON(month_data);
		// add years
		var year_data = '<?php echo $mut_years; ?>';
		year_data = $.parseJSON(year_data);
		//Initialize Select2 Elements
		$('#das-muttype').html('').select2({ data: ctg_data });
		$('#das-bparea').html('').select2({ data: area_data });
		$('#das-bulan').html('').select2({ data: month_data });
		$('#das-tahun').html('').select2({ data: year_data });
		$('.select2').select2();
	});
</script>
