<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<!-- /.header -->
	<section class="content">
		<div class="box">
			<div class="box-body">
				<div id="chart-container">MUT Chart will render here</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<!-- Fusion Chart -->
<script src="<?php echo base_url($plugins_dir . '/fusioncharts/js/fusioncharts.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/fusioncharts/js/themes/fusioncharts.theme.zune.js'); ?>"></script>

<script type="text/javascript">
	FusionCharts.ready(function () {
		var salesChart = new FusionCharts({
		type: 'MSColumn2D',
		renderAt: 'chart-container',
		width: '90%',
		height: '450',
		dataFormat: 'json',
		dataSource: {
			"chart": {
				"caption": "<?php echo 'Diagram Report MUT GT - Compliance By Visited'; ?>",
				"subcaption": "<?php echo '03 Mar 2018' . ' - ' . '31 Mar 2018'; ?>",
				"captionFontSize": "14",
				"subcaptionFontSize": "14",
				"subcaptionFontBold": "0",
				"xaxisname": "Area",
				"yaxisname": "Total",
				"showvalues": "1",
				"numberprefix": "",
				/*"legendBgAlpha": "0",
				"legendBorderAlpha": "0",
				"legendShadow": "0",*/
				"showborder": "0",
				"bgcolor": "#ffffff",
				/*"baseFontColor": "#ffffff",*/
				"valueFontColor": "#ffffff",
				"showalternatehgridcolor": "0",
				"showplotborder": "0",
				"showcanvasborder": "0",
				"legendshadow": "0",
				"plotgradientcolor": "",
				"showCanvasBorder": "0",
				"showAxisLines": "1",
				"showAlternateHGridColor": "0",
				"divlineAlpha": "100",
				"divlineThickness": "1",
				"divLineDashed": "1",
				"divLineDashLen": "1",
				"lineThickness": "3",
				"flatScrollBars": "1",
				"scrollheight": "10",
				"numVisiblePlot": "12",
				"showHoverEffect": "1",
				"theme": "fint",
				"showsum": "1",
				//"unescapeLinks": "1",       // Link Chart
				"formatNumberScale" : "0"   // Remove format number 10000 = 1k
			},
			"categories": [
					{
						"category": 
							[
								{'test': 'test123'}
							]
					}
				],
			"dataset": [
				{
					"seriesname": "Compliance",
					"color": "2a3f54",
					"data": 
						[
							{
								"label": "Jan",
								"value": "40"
							}, 
							{
								"label": "Feb",
								"value": "30"
							},
							{
								"label": "Mar",
								"value": "20"
							}, 
							{
								"label": "Apr",
								"value": "15"
							}, 
							{
								"label": "May",
								"value": "46"
							}, 
							{
								"label": "Jun",
								"value": "55"
							}, 
							{
								"label": "Jul",
								"value": "68"
							}, 
							{
								"label": "Aug",
								"value": "62"
							}, 
							{
								"label": "Sep",
								"value": "76"
							}, 
							{
								"label": "Oct",
								"value": "87"
							}, 
							{
								"label": "Nov",
								"value": "90"
							}, 
							{
								"label": "Dec",
								"value": "73"
							}
						]
				},
				{
					"seriesname": "Non Compliance",
					"color": "0094ba",
					"data": 
						[
							{
								"label": "Jan",
								"value": "2"
							}, 
							{
								"label": "Feb",
								"value": "10"
							},
							{
								"label": "Mar",
								"value": "20"
							}, 
							{
								"label": "Apr",
								"value": "1"
							}, 
							{
								"label": "May",
								"value": "4"
							}, 
							{
								"label": "Jun",
								"value": "5"
							}, 
							{
								"label": "Jul",
								"value": "6"
							}, 
							{
								"label": "Aug",
								"value": "7"
							}, 
							{
								"label": "Sep",
								"value": "6"
							}, 
							{
								"label": "Oct",
								"value": "8"
							}, 
							{
								"label": "Nov",
								"value": "9"
							}, 
							{
								"label": "Dec",
								"value": "4"
							}
						]
				}
			]
		}
	})
		.render();
	});
</script>
