<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/clearable//input.clearable.css') ?>" />

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"> Stock Control Summary List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>

			<div class="box-body">
				<table class="table table-striped table-condensed" style="margin-bottom: 0;">
					<tr>
						<td style="padding-bottom: 0;">
							<div class="col-sm-4 col-lg-3">
								<div class="form-group">
									<label for="das-bparea"><?php echo "Area";?></label>
									<select id="das-bparea" class="form-control select2" multiple="multiple" data-placeholder="Filter by area"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-sm-4 col-lg-3">
								<div class="form-group">
									<label for="das-mutname"><?php echo "Distributor";?></label>
									<input class="form-control clearable" id="das-distname">
								</div>
							</div>
							<div class="col-sm-2 col-lg-2">
								<div class="form-group">
									<label for="das-bulan"><?php echo "Bulan";?></label>
									<select id="das-bulan" class="form-control select2" data-placeholder="Filter by bulan"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-md-2 col-lg-2">
								<div class="form-group">
									<label for="das-tahun"><?php echo "Tahun";?></label>
									<select id="das-tahun" class="form-control select2" data-placeholder="Filter by tahun"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-sm-2">
								<button id="btnSearch" type="button" class="btn btn-default" style="margin-top:25px; width: 100%;">Search</button>
							</div>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="box-body" style="overflow-x:scroll;white-space: nowrap;">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="120%" cellspacing="0" id="graphscTable">
					<thead>
						<tr>
							<th rowspan="2"></th>
							<th rowspan="2"></th>
							<th rowspan="2"></th>
							<!-- <th colspan="3" style="text-align:center">Minggu Ke-1</th>
							<th colspan="3" style="text-align:center">Minggu Ke-2</th>
							<th colspan="3" style="text-align:center">Minggu Ke-3</th>
							<th colspan="3" style="text-align:center">Minggu Ke-4</th>
							<th colspan="3" style="text-align:center">Minggu Ke-5</th> -->
						<tr>
						<tr>
							<th style="width: 30px;"><?php echo 'No';?></th>
							<th style="width: 50px;"><?php echo 'Product Code';?></th>
							<th style="width: 150px;"><?php echo 'Product Description';?></th>
							<th style="width: 30px;"><?php echo 'Target [Minggu Ke-1]';?></th>
							<th style="width: 30px;"><?php echo 'Tersedia [Minggu Ke-1]';?></th>
							<th style="width: 30px;"><?php echo 'so_week Order [Minggu Ke-1]';?></th>
							<th style="width: 30px;"><?php echo 'Target [Minggu Ke-2]';?></th>
							<th style="width: 30px;"><?php echo 'Tersedia [Minggu Ke-2]';?></th>
							<th style="width: 30px;"><?php echo 'so_week Order [Minggu Ke-2]';?></th>
							<th style="width: 30px;"><?php echo 'Target [Minggu Ke-3]';?></th>
							<th style="width: 30px;"><?php echo 'Tersedia [Minggu Ke-3]';?></th>
							<th style="width: 30px;"><?php echo 'so_week Order [Minggu Ke-3]';?></th>
							<th style="width: 30px;"><?php echo 'Target [Minggu Ke-4]';?></th>
							<th style="width: 30px;"><?php echo 'Tersedia [Minggu Ke-4]';?></th>
							<th style="width: 30px;"><?php echo 'so_week Order [Minggu Ke-4]';?></th>
							<th style="width: 30px;"><?php echo 'Target [Minggu Ke-5]';?></th>
							<th style="width: 30px;"><?php echo 'Tersedia [Minggu Ke-5]';?></th>
							<th style="width: 30px;"><?php echo 'so_week Order [Minggu Ke-5]';?></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/clearable/input.clearable.js') ?>"></script>

<script type="text/javascript">
	var oTable;

	$(document).ready(function() {
		// add data bp area
		var area_data = '<?php if ($this->ion_auth->is_adminbp()) { echo $bp_areas; }else{echo $ao_areas;} ?>';
		area_data = $.parseJSON(area_data);
		// add months
		var month_data = '<?php echo $mut_months; ?>';
		month_data = $.parseJSON(month_data);
		// add years
		var year_data = '<?php echo $mut_years; ?>';
		year_data = $.parseJSON(year_data);
		//Initialize Select2 Elements
		$('#das-bparea').html('').select2({ data: area_data });
		$('#das-bulan').html('').select2({ data: month_data });
		$('#das-tahun').html('').select2({ data: year_data });
		$('.select2').select2();

		oTable = $('#graphscTable').DataTable({
			"buttons": [
				{
					text: 'Export to Excel',
					action: function ( e, dt, node, config ) {
						location.href = "<?php echo site_url("report/export_excel_graphsummary") ?>?" + "<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?> ?>&das-bparea=" + $('#das-bparea').val() + "&das-mutname=" + $('#das-mutname').val() + "&das-bulan=" + $('#das-bulan').val() + "&das-tahun=" + $('#das-tahun').val();
					}
				}
			],
			orderCellsTop: true,
        	fixedHeader: true,
			"dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			"lengthMenu": [[10, 20, 30], [10, 20, 30]],
			"processing": true,
			"serverSide": true,
			"searching": true,
			"order": [[0, 'asc']],
			"ajax": {
			type: "POST",
			url: "<?php echo site_url('report/get_stock_control_summary_list') ?>",
			data: function ( d ) {
				return $.extend( {}, d, {
					"<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>",
					"das-bparea": $("#das-bparea").val(),
					"das-mutname": $("#das-mutname").val(),
					"das-bulan": $("#das-bulan").val(),
					"das-tahun": $("#das-tahun").val()
				});
			},
			},
			"cache": false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"columns": [
			{
				data: null,
				orderable: false
			}, {
				data: "product_code",
				orderable: true
			}, {
				data: "sku",
				orderable: true
			}, {
				data: "$.target_1",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.tersedia_1",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.so_week_1",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.target_2",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.tersedia_2",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.so_week_2",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.target_3",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.tersedia_3",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.so_week_3",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.target_4",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.tersedia_4",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.so_week_4",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.target_5",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.tersedia_5",
				orderable: false,
				className: "text-right"
			}, {
				data: "$.so_week_5",
				orderable: false,
				className: "text-right"
			}],
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                var index = iDisplayIndex +1;
                $('td:eq(0)',nRow).html(index);
                return nRow;
            },
			bFilter: false,
			columnDefs: [
				{ targets: [1,2], orderable: false, searchable: false }
			]
		});
	});

	// Searching
	$('#btnSearch').on('click', function(e) {
		e.preventDefault();
		oTable.draw();
	});
</script>
