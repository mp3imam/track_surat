<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/css/select2.min.css'); ?>">

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/clearable//input.clearable.css') ?>" />

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"> Aktifitas MUT Summary List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>

			<div class="box-body">
				<table class="table table-striped table-condensed" style="margin-bottom: 0;">
					<tr>
						<td style="padding-bottom: 0;">
							<?php if ($this->ion_auth->is_adminho()) { ?>
							<div class="col-sm-2 col-lg-2">
								<div class="form-group">
									<label for="das-bp"><?php echo "BP";?></label>
									<select id="das-bp" class="form-control select2" multiple="multiple" data-placeholder="BP"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<?php } ?>
							<div class="col-sm-2 col-lg-2">
								<div class="form-group">
									<label for="das-bparea"><?php echo "Area";?></label>
									<select id="das-bparea" class="form-control select2" multiple="multiple" data-placeholder="Area"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-sm-2 col-lg-2">
								<div class="form-group">
									<label for="mut_code"><?php echo "MUT Code";?></label>
									<input class="form-control clearable" id="mut_code">
								</div>
							</div>
							<div class="col-sm-1 col-lg-1">
								<div class="form-group">
									<label for="type"><?php echo "Type";?></label>
									<select id="type" class="form-control select2" multiple="multiple" data-placeholder="Type"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-sm-2 col-lg-2">
								<div class="form-group">
									<label for="das-bulan"><?php echo "Bulan";?></label>
									<select id="das-bulan" class="form-control select2" data-placeholder="Filter by bulan"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-md-2 col-lg-2">
								<div class="form-group">
									<label for="das-tahun"><?php echo "Tahun";?></label>
									<select id="das-tahun" class="form-control select2" data-placeholder="Filter by tahun"
											style="width: 100%;">
									</select>
								</div>
							</div>
							<div class="col-sm-1">
								<button id="btnSearch" type="button" class="btn btn-default" style="margin-top:25px; width: 100%;">Search</button>
							</div>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="box-body" style="overflow-x:scroll;white-space: nowrap;">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="120%" cellspacing="0" id="graphsumTable">
					<thead>
						<tr>
							<td colspan="12"></td>
							<td>Holiday : <span id="holiday"></span></td>
							<td>Working Days : <span id="working_day"></span></td>
						</tr>
						<tr>
							<th colspan="13" bgcolor="cyan"><?php echo 'Attendance';?></th>
							<th colspan="11" bgcolor="cyan"><?php echo 'Store Compliance';?></th>
							<th colspan="5" bgcolor="cyan"><?php echo '';?></th>
						</tr>
						<tr>
										<!-- Attendance -->
							<th style="width: 30px;"><?php echo 'No';?></th>
							<th style="width: 200px;"><?php echo 'Tipe MUT';?></th>
							<th style="width: 30px;"><?php echo 'BP';?></th>
							<th style="width: 150px;"><?php echo 'Area';?></th>
							<th style="width: 80px;"><?php echo 'Username';?></th>
							<th style="width: 200px;"><?php echo 'MUT Name';?></th>
							<th style="width: 50px;"><?php echo 'Hadir';?></th>
							<th style="width: 50px;"><?php echo 'Meeting';?></th>
							<th style="width: 30px;"><?php echo 'Cuti';?></th>
							<th style="width: 50px;"><?php echo 'Sakit';?></th>
							<th style="width: 50px;"><?php echo 'Libur';?></th>
							<th style="width: 30px;"><?php echo 'Total Kehadiran (Hadir + Meeting)';?></th>
							<th style="width: 30px;"><?php echo 'Compliance (%)';?></th>
										<!-- Store Compliance -->
							<th style="width: 50px;"><?php echo 'Target';?></th>
							<th style="width: 50px;"><?php echo 'Actual';?></th>
							<th style="width: 50px;"><?php echo 'Meeting';?></th>
							<th style="width: 50px;"><?php echo 'Toko Tutup';?></th>
							<th style="width: 50px;"><?php echo 'Persiapan Visit';?></th>
							<th style="width: 50px;"><?php echo 'Cuti';?></th>
							<th style="width: 50px;"><?php echo 'Sakit';?></th>
							<th style="width: 50px;"><?php echo 'Libur';?></th>
							<th style="width: 50px;"><?php echo 'No Report';?></th>
							<th style="width: 30px;"><?php echo 'Compliance (%)';?></th>
							<th style="width: 30px;"></th>
										<!-- Incentive Verification -->
							<th style="width: 30px;">Day</th>
							<th style="width: 30px;">Compliance</th>
							<th style="width: 30px;">Target Store</th>
							<th style="width: 30px;">Comply</th>
							<th style="width: 30px;">Compliance (%)</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url($plugins_dir . '/select2/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/clearable/input.clearable.js') ?>"></script>

<script type="text/javascript">
	var oTable;

    $(document).ready(function() {
      	$('#graphsumTable thead tr').clone(true).appendTo( '#graphsumTable thead' );
    	$('#graphsumTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );
	    });

	$(document).ready(function() {
		// add data bp area
		var bp_data = '<?php echo $bp; ?>';
		bp_data = $.parseJSON(bp_data);

		// add data bp area
		var area_data = '<?php echo $areas; ?>';
		area_data = $.parseJSON(area_data);

		// add months
		var type_data = '<?php echo $type; ?>';
		type_data = $.parseJSON(type_data);

		// add months
		var month_data = '<?php echo $mut_months; ?>';
		month_data = $.parseJSON(month_data);

		// add years
		var year_data = '<?php echo $mut_years; ?>';
		year_data = $.parseJSON(year_data);

		//Initialize Select2 Elements
		$('#das-bp').html('').select2({ data: bp_data });
		$('#das-bparea').html('').select2({ data: area_data });
		$('#type').html('').select2({ data: type_data });
		$('#das-bulan').html('').select2({ data: month_data });
		$('#das-tahun').html('').select2({ data: year_data });
		$('.select2').select2();

		oTable = $('#graphsumTable').DataTable({
			"buttons": [
				{
					text: 'Export to Excel',
					action: function ( e, dt, node, config ) {
						location.href = "<?php echo site_url("report/export_excel_summary") ?>?" + "<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&das-bparea=" + $('#das-bparea').val() + "&das-bp=" + $('#das-bp').val() + "&type=" + $('#type').val() + "&das-bulan=" + $('#das-bulan').val() + "&das-tahun=" + $('#das-tahun').val();
					}
				}
			],
			"dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
			"lengthMenu": [[10, 20, 30], [10, 20, 30]],
			"processing": true,
			"serverSide": true,
			"searching": true,
			"order": [[0, 'asc']],
			"ajax": {
			type: "POST",
			url: "<?php echo site_url('report/get_graph_summary_list') ?>",
			data: function ( d ) {
				return $.extend( {}, d, {
					"<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>",
					"das-bp": $("#das-bp").val(),
					"das-bparea": $("#das-bparea").val(),
					"mut_code": $("#mut_code").val(),
					"type": $("#type").val(),
					"das-bulan": $("#das-bulan").val(),
					"das-tahun": $("#das-tahun").val()
				});
			},
			},
			"cache": false,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"columns": [
			{
				data: null,
				orderable: false
			}, {
				data: "mut_type_name",
				orderable: true
			}, {
				data: "a.bp_name",
				orderable: true
			}, {
				data: "area_name",
				orderable: true
			}, {
				data: "mut_code",
				orderable: true
			}, {
				data: "mut_name",
				orderable: true
			}, {
				data: "hadir_hari",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.actual; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "meeting_hari",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.meeting; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "cuti_hari",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.cuti; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "izin_hari",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.izin; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "libur_hari",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.libur; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}else{
                		return data;
                	}
                },
			}, {
				data: "$.day_incentive",
				orderable: false,
				className: "text-right",
			}, {
				data: "$.Compliance",
				orderable: false,
				className: "text-right",
			}, {
				data: "total_target",
				orderable: false,
				className: "text-right",
			}, {
				data: "actual",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.hadir_hari; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "meeting",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.meeting_hari; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "toko_tutup",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.toko_tutup; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			},{
				data: "persiapan_visit",
				orderable: false,
				className: "text-right",
				"mRender": function(data, type, row) {
					var str = row.persiapan_visit;
					if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
						return htmlBtnAct;
					}
					else{
						return data;
					}
				},
			}, {
				data: "cuti",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.cuti_hari; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "izin",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.izin_hari; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "libur",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.libur_hari; 
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "uncomply",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.uncomply;
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "$.percent",
				orderable: false,
				className: "text-right",
                "mRender": function(data, type, row) {
                    var str = row.persen;
                	if(typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === ""){
						var htmlBtnAct = 0;
                		return htmlBtnAct;
                	}
                	else{
                		return data;
                	}
                },
			}, {
				data: "id_user_mut",
				orderable: false,
				className: "text-center",
				"mRender": function(data, type, row) {
					var htmlBtnAct = "<a title='Detail' href='<?php echo site_url("report/graph_detail/") ?>" + data + "' class='btn btn-xs btn-info m-t-15 waves-effect'>Detail</a>&nbsp;";
					return htmlBtnAct;
				}
			}, {
				data: "$.day_incentive",
				orderable: false,
				className: "text-right",
			}, {
				data: "$.Compliance",
				orderable: false,
				className: "text-right",
			}, {
				data: "total_target",
				orderable: false,
				className: "text-right",
			},
				{
				data: "$.actual_store",
				orderable: false,
				className: "text-right",
			},{
				data: "CONCAT(persen, ' %')",
				orderable: false,
				className: "text-right",
			}
			],
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                var index = iDisplayIndex +1;
                $('td:eq(0)',nRow).html(index);
                return nRow;
            },
            "drawCallback":function(){
				$.get("<?php echo $hp_holiday; ?>" +$('#das-bulan').val()+ "/" + $('#das-tahun').val(), function(data){
					$('#holiday').html(data.holiday);
					$('#working_day').html(data.working_days);
				});            	
            },
			bFilter: false,
		});
	});


	// Searching
	$('#btnSearch').on('click', function(e) {
		e.preventDefault();
		oTable.draw();
	});
</script>
