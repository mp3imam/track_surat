<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/datatables/buttons/buttons.dataTables.min.css'); ?>">

<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
		<?php echo $breadcrumb; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mapping Business Partner List</h3>
				<div class="box-tools pull-right">
				</div>
			</div>
			<div class="box-body" style="overflow-x:scroll;white-space: nowrap;">
				<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="120%" cellspacing="0" id="mapPartnerTable">
					<thead>
						<tr>
							<th style="width: 150px;"><?php echo 'Business Partner Name';?></th>
							<th style="width: 100px;"><?php echo 'Region Name';?></th>
							<th style="width: 150px;"><?php echo 'Area Name';?></th>
							<th style="width: 30px;"><?php echo 'Total MUT GT';?></th>
							<th style="width: 30px;"><?php echo 'Total MUT FG';?></th>
							<th style="width: 30px;"><?php echo 'Total MUT IC';?></th>
							<th style="width: 30px;">action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>

<script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url($plugins_dir . '/datatables/buttons/dataTables.buttons.min.js'); ?>"></script>

<script type="text/javascript">
var oTable;

    $(document).ready(function() {
      	$('#mapPartnerTable thead tr').clone(true).appendTo( '#mapPartnerTable thead' );
    	$('#mapPartnerTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search "/>' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
	            }
	        } );
	    } );

        oTable = $('#mapPartnerTable').DataTable({
			"buttons": [
				{
					text: 'Add',
					action: function ( e, dt, node, config ) {
						location.href = '<?php echo site_url("business_partner/mapping_partner_add/") ?>';
					}
				}
			],
			orderCellsTop: true,
        	fixedHeader: true,
            "dom": 'Bfrt<"col-sm-3"i><"col-sm-2"l><"col-sm-7"p>',
            "lengthMenu": [[10, 20, 30], [10, 20, 30]],
            "processing": true,
            "serverSide": true,
			"searching": true,
            "order": [[0, 'asc'],[2, 'asc']],
            "ajax": {
            type: "POST",
            url: "<?php echo site_url('business_partner/get_mapping_partner_list') ?>",
            data: function ( d ) {
                return $.extend( {}, d, {
                    "<?php echo $this->security->get_csrf_token_name(); ?>" : "<?php echo $this->security->get_csrf_hash(); ?>"
                });
            },
            },
            "cache": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "columns": [{
                data: "b.bp_name",
                orderable: true
			}, {
				data: "c.region_name",
				orderable: true
			}, {
				data: "d.area_name",
				orderable: true
			}, {
				data: "a.total_mut_gt",
				orderable: true
			}, {
				data: "a.total_mut_fg",
				orderable: true
			}, {
				data: "a.total_mut_ic",
				orderable: true
			}, {
                data: "a.id_",
                className: "text-center",
                "mRender": function(data, type, row) {
                    var htmlBtnAct = "<a title='Edit' href='<?php echo site_url("business_partner/mapping_partner_edit/") ?>" + data + "' class='btn btn-xs btn-info m-t-15 waves-effect'>Edit</a>&nbsp;";
                    return htmlBtnAct;
                }
			}, {
                data: "a.id_",
				visible: false
            }],
            bFilter: false
        });

    });
</script>
