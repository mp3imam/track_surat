<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
  </section>
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-body">
        <i class="fa fa-plus"></i> Add Mapping Partner
      </div>
      <!-- /.box-body -->
    <?php echo form_open('business_partner/post_mapping_partner_add'); ?>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                <select name="id_region" id="region" class="form-control" onChange="tampil_area()" required>
                  <option>- Pilih Region -</option>
          <?php 
            foreach ($temp as $key => $r) {
              echo "<option value ='".$r['id_']."'>".$r['region_name']."</option>";
            } 
          ?>
                </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <?php 
                      $styleus = 'id="area" class="form-control" onChange="tampil_bp()" ';
                      echo form_dropdown('id_area', array('Pilih Area'=>'- Pilih Area -'), '', $styleus);
                  ?>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <select name="id_bp" id="bp" class="form-control" required>
                  <option>- Pilih Business Partner -</option>
          <?php foreach ($temp_bp as $key => $bp) {
            echo "<option value ='".$bp['id_']."'>".$bp['bp_name']."</option>";
          } ?>
                  </select>
                </div>
            <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="number" max="900" name="total_mut_fg" class="form-control" placeholder="Total MUT FG" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="number" max="900" name="total_mut_gt" class="form-control" placeholder="Total MUT GT" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-plus"></i></span>
                  <input type="number" max="900" name="total_mut_ic" class="form-control" placeholder="Total MUT IC" required>
              </div>
              <br>
    </div>

      <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 

  </form>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<script type="text/javascript">
  function tampil_area(){ 
     kddi = document.getElementById("region").value; 
     $.ajax({
         url:"<?php echo base_url().ADD_INDEXPHP;?>location/pilih_area/"+kddi+"",
         success: function(response){
         $("#area").html(response);
         },
         dataType:"html"
     });
     return false;
    }
</script>
