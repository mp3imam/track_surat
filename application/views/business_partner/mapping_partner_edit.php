<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<section class="content-header">
		<?php echo $pagetitle; ?>
	</section>
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<i class="fa fa-pencil-square-o"></i> Edit Mapping Partner
			</div>
			<!-- /.box-body -->
		<?php echo form_open('business_partner/post_mapping_partner_edit'); ?>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                <select name="id_region" id="region" class="form-control" onChange="tampil_area()" required>
                	<option>- Pilih Region -</option>
				<?php foreach ($temp as $key => $r) {
					if ($mapping_partner[0]['id_region'] == $r['id_']) {
						echo "<option value ='".$r['id_']."' selected='selected'>".$r['region_name']."</option>";
					} else {
						echo "<option value ='".$r['id_']."'>".$r['region_name']."</option>";
					}
				} ?>
                </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                    <select name="id_area" id="area" class="form-control" required>
                    <option>- Pilih Area -</option>
            <?php foreach ($temp_area as $key => $area) {
              if ($mapping_partner[0]['id_area'] == $area['id_']) {
                echo "<option value ='".$area['id_']."' selected='selected'>".$area['area_name']."</option>";
              } else {
                echo "<option value ='".$area['id_']."'>".$area['area_name']."</option>";
              }
            } ?>
                    </select>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
	                <select name="id_bp" id="bp" class="form-control" required>
                	<option>- Pilih Business Partner -</option>
					<?php foreach ($temp_bp as $key => $bp) {
						if ($mapping_partner[0]['id_bp'] == $bp['id_']) {
							echo "<option value ='".$bp['id_']."' selected='selected'>".$bp['bp_name']."</option>";
						} else {
							echo "<option value ='".$bp['id_']."'>".$bp['bp_name']."</option>";
						}
					} ?>
	                </select>
	              </div>
	          <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                  <input type="number" max="900" name="total_mut_fg" value="<?php echo $mapping_partner[0]['total_mut_fg']; ?>" class="form-control" placeholder="Total MUT FG" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                  <input type="number" max="900" name="total_mut_gt" value="<?php echo $mapping_partner[0]['total_mut_gt']; ?>" class="form-control" placeholder="Total MUT GT" required>
              </div>
              <br>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                  <input type="number" max="900" name="total_mut_ic" value="<?php echo $mapping_partner[0]['total_mut_ic']; ?>" class="form-control" placeholder="Total MUT IC" required>
              </div>
              <br>
		</div>

		  <fieldset style="text-align:right;">
                
                <button class="btn btn-block btn-primary" name="submit" id="submit"><i class="fa fa-database"></i> Save</button>
            </fieldset> 
		
		<input name="id_mapping_partner" type="hidden" value="<?php echo $id_mapping_partner; ?>">
	</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	function tampil_area(){ 
     kddi = document.getElementById("region").value; 
     $.ajax({
         url:"<?php echo  base_url().ADD_INDEXPHP;?>location/pilih_area/"+kddi+"",
         success: function(response){
         $("#area").html(response);
         },
         dataType:"html"
     });
     return false;
    }

	function SetSelected(elem, val){
        $('#'+elem+' option').each(function(i,d){
        //  console.log('searching match for '+ elem + '  ' + d.value + ' equal to '+ val);
            if($.trim(d.value).toLowerCase() == $.trim(val).toLowerCase()){
        //      console.log('found match for '+ elem + '  ' + d.value);
                $('#'+elem).prop('selectedIndex', i);
            }
        });
    }
</script>
