<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>404 Page Not Found</title>
	<link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqElEQVRYR+2WYQ6AIAiF8W7cq7oXd6v5I2eYAw2nbfivYq+vtwcUgB1EPPNbRBR4Tby2qivErYRvaEnPAdyB5AAi7gCwvSUeAA4iis/TkcKl1csBHu3HQXg7KgBUegVA7UW9AJKeA6znQKULoDcDkt46bahdHtZ1Por/54B2xmuz0uwA3wFfd0Y3gDTjhzvgANMdkGb8yAyY/ro1d4H2y7R1DuAOTHfgAn2CtjCe07uwAAAAAElFTkSuQmCC">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,700italic">
	<link rel="stylesheet" href="<?php echo config_item('base_url'); ?>/assets/frameworks/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo config_item('base_url'); ?>/assets/frameworks/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo config_item('base_url'); ?>/assets/frameworks/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo config_item('base_url'); ?>/assets/frameworks/adminlte/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo config_item('base_url'); ?>/assets/frameworks/adminlte/css/skins/skin-blue.min.css">
</head>
<body>
	<div class="content-wrapper" style="margin-left: 0;">
		<!-- Main content -->
		<section class="content">
			<div class="error-page">
				<h2 class="headline text-yellow"> 404</h2>
				<div class="error-content" style="padding-top: 20px;">
					<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
					<p>
						We could not find the page you were looking for.
						Meanwhile, you may <a href="<?php echo config_item('base_url'); ?>">return to dashboard</a>.
					</p>
				</div>
				<!-- /.error-content -->
			</div>
			<!-- /.error-page -->
		</section>
		<!-- /.content -->
	</div>
</body>
</html>