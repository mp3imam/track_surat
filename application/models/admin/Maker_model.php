<?php
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('DatatableModel');

class Maker_model extends Core_model implements DatatableModel {
    
    protected $tables = array();
    
    public function __construct()
    {
        parent::__construct();
        
        // initialize db tables data
        $this->tables = array(
            'mst_maker' => 'mst_maker'
        );
    }

    /**
    * @ return
     *      Expressions / Columns to append to the select created by the Datatable library
     */
    public function appendToSelectStr() {
        //_protect_identifiers needs to be FALSE in the database.php when using custom expresions to avoid db errors.
        //CI is putting `` around the expression instead of just the column names
            // return array(
            //   'city_name' => 'b.city_name',
            //  );
            return NULL;
    }
    
    public function fromTableStr() {
        return $this->tables['mst_maker'].' a';
    }
    
    /**
        * @return
        *     Associative array of joins.  Return NULL or empty array  when not joining
        */
    public function joinArray(){
        // return array(
        // $this->tables['mst_maker_groups'].' b|inner' => 'b.user_id = a.id'
        // );
        return NULL;
    }
    
    /**
    * 
    *@return
    *  Static where clause to be appended to all search queries.  Return NULL or empty array
    * when not filtering by additional criteria
    */
    public function whereClauseArray($filter){
        return $filter;
    }

    public function get_edit($id_) {
        $query = $this->db->query("
            SELECT 
                id as 'id_update', nameuser
            FROM
                mst_maker a
            WHERE
                id = '$id_'
            ")->result_array();
        return $query;
    }

    public function reset_password() {
        $data = array(
            'id_mk' => '',
            'id_sg' => '',
            'id_tl' => '',
            'tgl_mk' => '',
            'tgl_sg' => '',
            'tgl_tl' => ''
        );
        $this->db->where('id',$id_);
        $this->db->update('mst_maker',$data);
    }

    public function update_login($id_) {
        $id_update  = $this->input->post('id_update');
        $nameuser = $this->input->post('nameuser');

        $data = array('nameuser' => $nameuser);
        $this->db->where('id',$id_);
        $this->db->update('mst_maker',$data);
    }

    public function get_export_data_user_mut($filter) {
        $this->db->select("b.mut_type_name AS 'Type MUT'");
        $this->db->select("a.mut_name AS 'MUT Name'");
        $this->db->select("a.mut_code AS 'MUT Code'");
        $this->db->select("a.passwd AS 'MUT Password'");
        $this->db->select("a.mut_telp AS 'MUT Telp'");
        $this->db->select("a.mut_email AS 'MUT Email'");
        $this->db->from("mst_maker a");
        $this->db->join("mst_maker_groups b", "a.id_mut_type = b.id_");
        $this->db->where($filter['equals']);
        return $this->db->get();
    }

}
