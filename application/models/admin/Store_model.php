<?php
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('DatatableModel');

class Store_model extends Core_model implements DatatableModel {
	
	protected $tables = array();
	
	public function __construct()
	{
		parent::__construct();
		
		// initialize db tables data
		$this->tables = array(
            'mst_store'             => 'mst_store',
            'mst_mut_type'          => 'mst_mut_type',
            'mst_cluster'           => 'mst_cluster',
            'mst_distributor'       => 'mst_distributor',
            'mst_account'           => 'mst_account',
            'mst_qrcode'            => 'mst_qrcode',
            'mst_mapping_bpartner'  => 'mst_mapping_bpartner',
            'mst_bpartner'          => 'mst_bpartner',
            'mst_area'              => 'mst_area',
            'tbl_hist_qrcode'       => 'tbl_hist_qrcode'
		);
    }
    
	/**
    * @ return
		 * 		Expressions / Columns to append to the select created by the Datatable library
		 */
		public function appendToSelectStr() {
			//_protect_identifiers needs to be FALSE in the database.php when using custom expresions to avoid db errors.
			//CI is putting `` around the expression instead of just the column names
				// return array(
				//  );
				return NULL;
		}
		
		public function fromTableStr() {
			return $this->tables['mst_store'].' a';
		}
		
		/**
			* @return
			*     Associative array of joins.  Return NULL or empty array  when not joining
			*/
		public function joinArray(){
			return array(
            $this->tables['mst_mut_type'].' b|inner'            => ' b.id_ = a.id_mut_type',
            $this->tables['mst_cluster'].' c|inner'             => ' c.id_ = a.id_cluster',
            $this->tables['mst_distributor'].' d|inner'         => ' d.id_ = a.id_dist',
            $this->tables['mst_account'].' e|inner'             => ' e.id_ = a.id_account',
            $this->tables['mst_qrcode'].' f|left'               => ' f.id_ = a.id_qrcode',
            $this->tables['mst_mapping_bpartner'].' g|inner'    => ' g.id_ = d.id_mapping_bpartner',
            $this->tables['mst_bpartner'].' h|inner'            => ' h.id_ = g.id_bp',
            $this->tables['mst_area'].' i|inner'                => ' i.id_ = d.id_area',
            $this->tables['tbl_hist_qrcode'].' j|left'          => ' j.id_store = a.id_'
			);
			return NULL;
		}
		
		/**
		* 
		*@return
		*  Static where clause to be appended to all search queries.  Return NULL or empty array
		* when not filtering by additional criteria
		*/
		public function whereClauseArray($filter){
			return $filter;
        }
        
        public function get_store_edit($id)
        {
            $data = $this->db->query("
            SELECT ms.id_
            , mr.region_name
            , ma.area_name            
            , ms.id_qrcode
            , ms.id_mut_type
            , mm.mut_type_name
            , ms.id_cluster
            , mc.cluster_name
            , md.dist_name
            , ms.id_account
            , ms.store_name
            , ms.outlet_code
            , ms.store_residence
            , ms.store_address
            FROM mst_store ms
            INNER JOIN mst_cluster mc
            ON mc.id_ = ms.id_cluster
            INNER JOIN mst_distributor md
            ON md.id_ = ms.id_dist
            INNER JOIN mst_mut_type mm
            ON mm.id_ = ms.id_mut_type
            INNER JOIN mst_region mr
            ON mr.id_ = md.id_region
            INNER JOIN mst_area ma
            ON ma.id_ = md.id_area
            WHERE ms.id_ = '$id'
            ")->result_array();
            
            return $data;
        }

        public function get_store_view($id)
        {
            $data = $this->db->query("
            SELECT 
                a.id_store,
                d.store_name,
                b.dist_name,
                c.area_name,
                last_qrcode,
                tgl_reset,
                reset_by
            FROM
                tbl_hist_qrcode a
                    INNER JOIN
                mst_distributor b ON b.id_ = a.id_dist
                    INNER JOIN
                mst_area c ON c.id_ = a.id_area
                    INNER JOIN
                mst_store d ON d.id_ = a.id_store
            WHERE
                id_store = $id 
            order by id_store desc limit 10
            ")->result_array();
            
            return $data;
        }

        public function history_qrcode($id_store) {
            $query = $this->db->query("
                SELECT * from tbl_hist_qrcode where id_store = $id_store order by tgl_reset desc limit 10
                ")->result_array(); 
            return $query;       
        }  

        public function store_save($id) {
        
            if ($id == 0) {
                $this->db->trans_begin(); # Begin Transaction
    
                $id_mut_type 		= $this->input->post('id_mut_type');
                $id_cluster 		= $this->input->post('id_cluster');
                $id_dist 			= $this->input->post('id_dist');
                $id_account 		= 1;
                $store_name 		= strtoupper($this->input->post('store_name'));
                $outlet_code 		= $this->input->post('outlet_code');
                $store_residence 	= $this->input->post('store_residence');
                $store_address 		= strtoupper($this->input->post('store_address'));
                $username			= $this->session->userdata('username');
                $tanggal			= date('Y-m-d H:i:s');
    
                $data=array(
                    'id_mut_type'		=>$id_mut_type,
                    'id_cluster'		=>$id_cluster,
                    'id_dist'			=>$id_dist,
                    'id_account'		=>$id_account,
                    'store_name'		=>$store_name,
                    'outlet_code'		=>$outlet_code,
                    'store_residence'	=>$store_residence,
                    'store_address'		=>$store_address,
                    'created_by'		=>$username,
                    'created_date'		=>$tanggal
                );
                $this->db->insert('mst_store', $data); 
    
                /* Optional */
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    return false;
                }
                else {
                    $this->db->trans_commit();
                    return true;
                }
            } else {
                $this->db->trans_begin(); # Begin Transactionç
    
                $id_update = $this->input->post('id_store');
                $id_cluster = $this->input->post('cluster');
                $store_name = $this->input->post('store_name');
                $outlet_code = $this->input->post('outlet_code');
                $store_residence = $this->input->post('store_residence');
                $store_addr = $this->input->post('store_address');
        
                $data=array(
                    'id_cluster'      =>$id_cluster,
                    'store_name'      =>$store_name,
                    'outlet_code'     =>$outlet_code,
                    'store_residence' =>$store_residence,
                    'store_address'   =>$store_addr
                );
                //print_r($data); die();
                $this->db->where('id_',$id_update);
                $this->db->update('mst_store', $data);
    
                /* Optional */
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    return false;
                }
                else {
                    $this->db->trans_commit();
                    return true;
                }
            }
        }    

        public function reset_qrcode($id_store,$id_dist,$id_area,$last_qrcode) {
            $tanggal     = date("Y-m-d H:i:s");
            $username    = $this->session->userdata('username');

            // Mengisi tbl_hist_qrcode
            $data = array(
                'id_store'      => $id_store,
                'id_dist'       => $id_dist,
                'id_area'       => $id_area,
                'last_qrcode'   => $last_qrcode,
                'tgl_reset'     => $tanggal,
                'reset_by'      => $username
            );
               
            $this->db->trans_begin(); # Begin Transactionç

            $this->db->insert('tbl_hist_qrcode', $data); 
            $data = '';

            // Ubah Status Store ke Nilai 0 atau menjadi status "belum terdaftar"
            $data = array('id_qrcode'=> null );
            $this->db->where('id_',$id_store);  
            $this->db->update('mst_store', $data);

            // Ubah id store dari tbl_qrcode menjadi null atau 0
            $data = '';
            $data = array('id_qrcode' => null );
            $this->db->where('id_qrcode',$last_qrcode); 
            $this->db->update('mst_qrcode', $data);

            /* Optional */
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            }
            else {
                $this->db->trans_commit();
                return true;
            }

        }

        public function store_id_delete($action){
            $tanggal     = date("Y-m-d H:i:s");
            $username    = $this->session->userdata('username');
            $id_store    = $this->input->post('id_store'.$action);
            $id_store    = explode(',', $id_store);

            if($action == 0){
                $data = array('is_deleted' => 1);
            }else{
                $data = array('is_deleted' => null);
            }

            //echo $action; die();
            $this->db->trans_begin(); # Begin Transactionç
            $this->db->where_in('id_',$id_store);
            $this->db->update('mst_store', $data);

            /* Optional */
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            }
            else {
                $this->db->trans_commit();
                return true;
            }
        }

        public function get_export_data($filter) {
            $this->db->select("a.id_ 'Store ID'");
            $this->db->select("h.bp_code AS 'BP Code'");
            $this->db->select("b.mut_type_name");
            $this->db->select("c.cluster_name");
            $this->db->select("e.account_name");
            $this->db->select("f.id_qrcode");
            $this->db->select("a.store_name");
            $this->db->select("a.outlet_code");
            $this->db->select("a.store_residence");
            $this->db->select("a.store_address");
            $this->db->from("mst_store a");
            $this->db->join('mst_mut_type b', 'b.id_ = a.id_mut_type', 'inner');
            $this->db->join('mst_cluster c', 'c.id_ = a.id_cluster', 'inner');
            $this->db->join('mst_distributor d', 'd.id_ = a.id_dist', 'inner');
            $this->db->join('mst_account e', 'e.id_ = a.id_account', 'inner');
            $this->db->join('mst_qrcode f', 'f.id_ = a.id_qrcode', 'left');
            $this->db->join('mst_mapping_bpartner g', 'g.id_ = d.id_mapping_bpartner', 'inner');
            $this->db->join('mst_bpartner h', 'h.id_ = g.id_bp', 'inner');
            $this->db->join('mst_area i', 'i.id_ = d.id_area', 'inner');
            $this->db->join('tbl_hist_qrcode j', 'j.id_store = a.id_', 'left');
            $this->db->where('is_deleted');
            if (isset($filter['equals']))
                $this->db->where($filter['equals']);

            if (isset($filter['contains']))
                $this->db->like($filter['contains']);

            return $this->db->get();
        }
}
