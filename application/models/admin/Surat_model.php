<?php
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('DatatableModel');

class Surat_model extends Core_model implements DatatableModel {
    
    protected $tables = array();
    
    public function __construct()
    {
        parent::__construct();
        
        // initialize db tables data
        $this->tables = array(
            'mst_surat' => 'mst_surat',
            'mst_maker' => 'mst_maker',
            'idn_users' => 'idn_users'
        );
    }

    /**
    * @ return
     *      Expressions / Columns to append to the select created by the Datatable library
     */
    public function appendToSelectStr() {
        //_protect_identifiers needs to be FALSE in the database.php when using custom expresions to avoid db errors.
        //CI is putting `` around the expression instead of just the column names
            // return array(
            //   'city_name' => 'b.city_name',
            //  );
            return NULL;
    }
    
    public function fromTableStr() {
        return $this->tables['mst_surat'].' a';
    }
    
    /**
        * @return
        *     Associative array of joins.  Return NULL or empty array  when not joining
        */
    public function joinArray(){
        return array(
            $this->tables['mst_maker'].' b|left' => 'b.id_surat = a.id',
            $this->tables['idn_users'].' c|left' => 'b.id_mk = c.id'
        );
        return NULL;
    }
    
    /**
    * 
    *@return
    *  Static where clause to be appended to all search queries.  Return NULL or empty array
    * when not filtering by additional criteria
    */
    public function whereClauseArray($filter){
        return $filter;
    }

    public function surat_save(){
        $query = $this->db->query("select count(id) 'no' from mst_surat")->row()->no+1;
        $no_reg          = "REG".date("Y").sprintf("%04s", $query);
        $dari_divisi     = $this->input->post('dari_divisi');
        $nama_rekening   = $this->input->post('nama_rekening');
        $no_rekening     = $this->input->post('no_rekening');
        $keterangan      = $this->input->post('keterangan');
        $created_date    = date('Y-m-d');
       
        $data=array(
            'no_reg'           =>$no_reg,
            'divisi_bagian'    =>$dari_divisi,
            'nama_rek'         =>$nama_rekening,
            'no_rek'           =>$no_rekening,
            'keterangan'       =>$keterangan,
            'created_date'     =>$created_date
        );

            $this->db->trans_begin(); # Begin Transaction

            $this->db->insert('mst_surat', $data);

            /* Optional */
            if ($this->db->trans_status() === false) {
                # Something when wrong.
                $this->db->trans_rollback();
                return false;
            }
            else {
                # Everything is perfect
                # Commiting data to the database
                $this->db->trans_commit();
                return true;
            }
        
        return false;
    }

    public function disposisi_save($id_surat,$id_mk){
        $nm_mk = $this->db->query("select nameuser from idn_users where id = '$id_mk'")->row()->nameuser;
        $user_mk = $this->db->query("select username from idn_users where id = '$id_mk'")->row()->username;

        $data=array(
            'id_surat'  =>$id_surat,
            'id_mk'     =>$id_mk,
            'user_mk'   =>$user_mk,
            'nm_mk'     =>$nm_mk
        );

            $this->db->trans_begin(); # Begin Transaction

            $this->db->insert('mst_maker', $data);

            /* Optional */
            if ($this->db->trans_status() === false) {
                # Something when wrong.
                $this->db->trans_rollback();
                return false;
            }
            else {
                # Everything is perfect
                # Commiting data to the database
                $this->db->trans_commit();
                return true;
            }
        
        return false;
    }

    public function cheker_approve($id_surat,$id_sg){
        $id_maker = $this->db->query("select * from mst_maker where id = '$id_sg'")->row()->id;
        $nm_sg    = $this->db->query("select * from idn_users where id = '$id_sg'")->row()->nameuser;

        $data=array(
            'id_sg'     =>$id_sg,
            'nm_sg'     =>$nm_sg,
            'tgl_sg'    =>$id_mk
        );

            $this->db->trans_begin(); # Begin Transaction

            $this->db->where('id',$id_surat);
            $this->db->update('mst_maker',$data);


            /* Optional */
            if ($this->db->trans_status() === false) {
                # Something when wrong.
                $this->db->trans_rollback();
                return false;
            }
            else {
                # Everything is perfect
                # Commiting data to the database
                $this->db->trans_commit();
                return true;
            }
        
        return false;
    }

    public function cheker_reject($id_surat,$id_reject,$nm_reject, $alasan) {
        $data = array(
            'reject_by'     => $id_reject,
            'nm_mk'     => null,
            'tgl_mk'    => null,
            'id_bk'     => null,
            'nm_bk'     => null,
            'tgl_bk'    => null,
            'id_sg'     => null,
            'nm_sg'     => null,
            'tgl_sg'    => null,
            'id_tl'     => null,
            'nm_tl'     => null,
            'tgl_tl'    => null
        );

            $this->db->trans_begin(); # Begin Transaction

            $this->db->where('id',$id_surat);
            $this->db->update('mst_maker',$data);


            /* Optional */
            if ($this->db->trans_status() === false) {
                # Something when wrong.
                $this->db->trans_rollback();
                return false;
            }
            else {
                # Everything is perfect
                # Commiting data to the database
                $this->db->trans_commit();
                return true;
            }
        
        return false;

    }

    public function get_edit($id_) {
        $query = $this->db->query("
            SELECT 
                id as 'id_update', nameuser
            FROM
                mst_surat a
            WHERE
                id = '$id_'
            ")->result_array();
        return $query;
    }

    public function update_login($id_) {
        $id_update  = $this->input->post('id_update');
        $nameuser = $this->input->post('nameuser');

        $data = array('nameuser' => $nameuser);
        $this->db->where('id',$id_);
        $this->db->update('mst_surat',$data);
    }

    public function surat($id) {
        return $query = $this->db->query("
            SELECT *
            FROM
                mst_surat a
            WHERE
            id = '$id'
            ")->result_array();
    }

    public function makers() {
        return $query = $this->db->query("
            select 0 id, '========= Pilih Maker =========' as text union
            SELECT 
                id, concat(username,' ',nameuser) as text
            FROM
                idn_users a
            WHERE
            last_name = 'MK'
            ")->result_array();
    }

    public function get_makers() {
        return $query = $this->db->query("
            SELECT 
                id, concat(username,' ',nameuser) as text
            FROM
                idn_users a
            WHERE
            last_name = 'MK'
            ")->result_array();
    }

    public function get_export_data_user_mut($filter) {
        $this->db->select("b.mut_type_name AS 'Type MUT'");
        $this->db->select("a.mut_name AS 'MUT Name'");
        $this->db->select("a.mut_code AS 'MUT Code'");
        $this->db->select("a.passwd AS 'MUT Password'");
        $this->db->select("a.mut_telp AS 'MUT Telp'");
        $this->db->select("a.mut_email AS 'MUT Email'");
        $this->db->from("mst_surat a");
        $this->db->join("mst_surat_groups b", "a.id_mut_type = b.id_");
        $this->db->where($filter['equals']);
        return $this->db->get();
    }

}
