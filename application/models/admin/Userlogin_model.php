<?php
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('DatatableModel');

class Userlogin_model extends Core_model implements DatatableModel {
    
    protected $tables = array();
    
    public function __construct()
    {
        parent::__construct();
        
        // initialize db tables data
        $this->tables = array(
            'idn_users' => 'idn_users'
        );
    }

    /**
    * @ return
     *      Expressions / Columns to append to the select created by the Datatable library
     */
    public function appendToSelectStr() {
        //_protect_identifiers needs to be FALSE in the database.php when using custom expresions to avoid db errors.
        //CI is putting `` around the expression instead of just the column names
            // return array(
            //   'city_name' => 'b.city_name',
            //  );
            return NULL;
    }
    
    public function fromTableStr() {
        return $this->tables['idn_users'].' a';
    }
    
    /**
        * @return
        *     Associative array of joins.  Return NULL or empty array  when not joining
        */
    public function joinArray(){
        // return array(
        // $this->tables['idn_users_groups'].' b|inner' => 'b.user_id = a.id'
        // );
        return NULL;
    }
    
    /**
    * 
    *@return
    *  Static where clause to be appended to all search queries.  Return NULL or empty array
    * when not filtering by additional criteria
    */
    public function whereClauseArray($filter){
        return $filter;
    }

    public function get_edit($id_) {
        $query = $this->db->query("
            SELECT 
                id as 'id_update', nameuser
            FROM
                idn_users a
            WHERE
                id = '$id_'
            ")->result_array();
        return $query;
    }

    public function reset_password() {
        $data = array('password' => '$2y$08$P1JQb7.6Csohabc3Ncw.Tu0HrYPsEFwsBOnHXSzVu1iGN8Pb.WXkW');
        $this->db->where('id',$id_);
        $this->db->update('idn_users',$data);
    }

    public function update_login($id_) {
        $id_update  = $this->input->post('id_update');
        $nameuser = $this->input->post('nameuser');

        $data = array('nameuser' => $nameuser);
        $this->db->where('id',$id_);
        $this->db->update('idn_users',$data);
    }

    public function user_save() {
        $type      = $this->input->post('type');
        $nameuser  = $this->input->post('nameuser');
        $query = $this->db->query('select * from idn_users where username like "%'.$type.'%" order by id desc limit 1')->result_array();
        $first_name = $query[0]['first_name'];
        $last_name  = $query[0]['last_name'];

        $no = substr($query[0]['username'],-2);
        $no = intval($no)+1;

        $no < 10 ? $no = "0".$no : $no;

        $username = $type.$no;

        $data = array(
            'ip_address'    => '127.0.0.1',
            'username'      => $username,
            'password'      => '$2y$08$P1JQb7.6Csohabc3Ncw.Tu0HrYPsEFwsBOnHXSzVu1iGN8Pb.WXkW',
            'created_on'    => '1268889823',
            'last_login'    => '1587458846',
            'first_name'    => 'ADMIN',
            'last_name'     => $last_name,
            'nameuser'      => $nameuser,
            'jabatan'       => $type
        );

        $this->db->insert('idn_users', $data);

        switch ($type) {
          case "SuperAdmin":
                $group_id = 1;
            break;
          case "Maker":
                $group_id = 3;
            break;
          case "Cheker":
                $group_id = 4;
            break;
          default:
            $group_id = 5;
        }

        $data_groups = array(
            'user_id'   => $this->db->insert_id(),
            'group_id'  => $group_id
        );

        $this->db->insert('idn_users_groups', $data_groups);
    }

    public function get_export_data_user_mut($filter) {
        $this->db->select("b.mut_type_name AS 'Type MUT'");
        $this->db->select("a.mut_name AS 'MUT Name'");
        $this->db->select("a.mut_code AS 'MUT Code'");
        $this->db->select("a.passwd AS 'MUT Password'");
        $this->db->select("a.mut_telp AS 'MUT Telp'");
        $this->db->select("a.mut_email AS 'MUT Email'");
        $this->db->from("idn_users a");
        $this->db->join("idn_users_groups b", "a.id_mut_type = b.id_");
        $this->db->where($filter['equals']);
        return $this->db->get();
    }

}
