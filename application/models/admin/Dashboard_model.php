<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_count_record($table)
    {
        $query = $this->db->count_all($table);

        return $query;
    }


    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
	}
	
	public function get_vendor($username)
    {
        $data = $this->db->query("SELECT id_ from mst_bpartner WHERE bp_code = '$username'")->row()->id_;
        return $data;  
    }

    public function get_alokasi_graph()
    {
        $data = $this->db->query("SELECT SUM(total_mut_gt) AS total_mut_gt
                                    , SUM(total_mut_fg) AS total_mut_fg
                                    , SUM(total_mut_ic) AS total_mut_ic
                                    FROM mst_mapping_bpartner")->row();
        return $data;
    }

    public function get_current_graph()
    {
        $data = $this->db->query("SELECT COALESCE(SUM(id_mut_type = 1), 0) AS total_current_fg
                                        , COALESCE(SUM(id_mut_type = 2), 0) AS total_current_gt
                                        , COALESCE(SUM(id_mut_type = 3), 0) AS total_current_ic
                                    FROM mst_user_mut")->row();
        return $data;
    }

    public function get_alokasi_area_graph($id_mut_type)
    {
        switch ($id_mut_type)
        {
            case 1: // FG
                $data = $this->db->query("SELECT CONCAT(t1.bp_code,' ', t2.area_name) AS bp_area
                                            , SUM(t.total_mut_fg) AS total_mut_fg
                                            , CASE WHEN tt.total_current_fg IS NULL THEN 0 ELSE tt.total_current_fg END AS total_current_fg
                                            FROM mst_mapping_bpartner t
                                            LEFT JOIN (SELECT tt1.id_ AS id_mapping_bp
                                                        , COALESCE(SUM(tt2.id_mut_type = 1), 0) AS total_current_fg
                                                        FROM mst_mapping_bpartner tt1
                                                        INNER JOIN mst_user_mut tt2
                                                        INNER JOIN mst_area tt3
                                                        ON tt2.id_mapping_bp = tt1.id_
                                                        AND tt3.id_ = tt1.id_area
                                                        GROUP BY tt1.id_, tt3.id_) tt
                                            ON tt.id_mapping_bp = t.id_
                                            INNER JOIN mst_bpartner t1
                                            INNER JOIN mst_area t2
                                            ON t.id_bp = t1.id_
                                            AND t.id_area = t2.id_
                                            GROUP BY t.id_bp, t2.id_")->result_array();
                break;
            case 2: // GT
                $data = $this->db->query("SELECT CONCAT(t1.bp_code,' ', t2.area_name) AS bp_area
                                            , SUM(t.total_mut_gt) AS total_mut_gt
                                            , CASE WHEN tt.total_current_gt IS NULL THEN 0 ELSE tt.total_current_gt END AS total_current_gt
                                            FROM mst_mapping_bpartner t
                                            LEFT JOIN (SELECT tt1.id_ AS id_mapping_bp
                                                        , COALESCE(SUM(tt2.id_mut_type = 2), 0) AS total_current_gt
                                                        FROM mst_mapping_bpartner tt1
                                                        INNER JOIN mst_user_mut tt2
                                                        INNER JOIN mst_area tt3
                                                        ON tt2.id_mapping_bp = tt1.id_
                                                        AND tt3.id_ = tt1.id_area
                                                        GROUP BY tt1.id_, tt3.id_) tt
                                            ON tt.id_mapping_bp = t.id_
                                            INNER JOIN mst_bpartner t1
                                            INNER JOIN mst_area t2
                                            ON t.id_bp = t1.id_
                                            AND t.id_area = t2.id_
                                            GROUP BY t.id_bp, t2.id_")->result_array();
                break;
            default: // IC
                $data = $this->db->query("SELECT CONCAT(t1.bp_code,' ', t2.area_name) AS bp_area
                                            , SUM(t.total_mut_ic) AS total_mut_ic
                                            , CASE WHEN tt.total_current_ic IS NULL THEN 0 ELSE tt.total_current_ic END AS total_current_ic
                                            FROM mst_mapping_bpartner t
                                            LEFT JOIN (SELECT tt1.id_ AS id_mapping_bp
                                                        , COALESCE(SUM(tt2.id_mut_type = 3), 0) AS total_current_ic
                                                        FROM mst_mapping_bpartner tt1
                                                        INNER JOIN mst_user_mut tt2
                                                        INNER JOIN mst_area tt3
                                                        ON tt2.id_mapping_bp = tt1.id_
                                                        AND tt3.id_ = tt1.id_area
                                                        GROUP BY tt1.id_, tt3.id_) tt
                                            ON tt.id_mapping_bp = t.id_
                                            INNER JOIN mst_bpartner t1
                                            INNER JOIN mst_area t2
                                            ON t.id_bp = t1.id_
                                            AND t.id_area = t2.id_
                                            GROUP BY t.id_bp, t2.id_")->result_array();
                break;
        }
        return $data;
    }

    public function download_alokasi_area_graph($id_mut_type)
    {
        switch ($id_mut_type)
        {
            case 1: // FG
                $data = $this->db->query("SELECT CONCAT(t1.bp_code,' ', t2.area_name) AS 'BP Area'
                                            , SUM(t.total_mut_fg) AS 'Alokasi'
                                            , CASE WHEN tt.total_current_fg IS NULL THEN 0 ELSE tt.total_current_fg END AS 'Current'
                                            FROM mst_mapping_bpartner t
                                            LEFT JOIN (SELECT tt1.id_ AS id_mapping_bp
                                                        , COALESCE(SUM(tt2.id_mut_type = 1), 0) AS total_current_fg
                                                        FROM mst_mapping_bpartner tt1
                                                        INNER JOIN mst_user_mut tt2
                                                        INNER JOIN mst_area tt3
                                                        ON tt2.id_mapping_bp = tt1.id_
                                                        AND tt3.id_ = tt1.id_area
                                                        GROUP BY tt1.id_, tt3.id_) tt
                                            ON tt.id_mapping_bp = t.id_
                                            INNER JOIN mst_bpartner t1
                                            INNER JOIN mst_area t2
                                            ON t.id_bp = t1.id_
                                            AND t.id_area = t2.id_
                                            GROUP BY t.id_bp, t2.id_");
                break;
            case 2: // GT
                $data = $this->db->query("SELECT CONCAT(t1.bp_code,' ', t2.area_name) AS 'BP Area'
                                            , SUM(t.total_mut_gt) AS 'Alokasi'
                                            , CASE WHEN tt.total_current_gt IS NULL THEN 0 ELSE tt.total_current_gt END AS 'Current'
                                            FROM mst_mapping_bpartner t
                                            LEFT JOIN (SELECT tt1.id_ AS id_mapping_bp
                                                        , COALESCE(SUM(tt2.id_mut_type = 2), 0) AS total_current_gt
                                                        FROM mst_mapping_bpartner tt1
                                                        INNER JOIN mst_user_mut tt2
                                                        INNER JOIN mst_area tt3
                                                        ON tt2.id_mapping_bp = tt1.id_
                                                        AND tt3.id_ = tt1.id_area
                                                        GROUP BY tt1.id_, tt3.id_) tt
                                            ON tt.id_mapping_bp = t.id_
                                            INNER JOIN mst_bpartner t1
                                            INNER JOIN mst_area t2
                                            ON t.id_bp = t1.id_
                                            AND t.id_area = t2.id_
                                            GROUP BY t.id_bp, t2.id_");
                break;
            default: // IC
                $data = $this->db->query("SELECT CONCAT(t1.bp_code,' ', t2.area_name) AS 'BP Area'
                                            , SUM(t.total_mut_ic) AS 'Alokasi'
                                            , CASE WHEN tt.total_current_ic IS NULL THEN 0 ELSE tt.total_current_ic END AS 'Current'
                                            FROM mst_mapping_bpartner t
                                            LEFT JOIN (SELECT tt1.id_ AS id_mapping_bp
                                                        , COALESCE(SUM(tt2.id_mut_type = 3), 0) AS total_current_ic
                                                        FROM mst_mapping_bpartner tt1
                                                        INNER JOIN mst_user_mut tt2
                                                        INNER JOIN mst_area tt3
                                                        ON tt2.id_mapping_bp = tt1.id_
                                                        AND tt3.id_ = tt1.id_area
                                                        GROUP BY tt1.id_, tt3.id_) tt
                                            ON tt.id_mapping_bp = t.id_
                                            INNER JOIN mst_bpartner t1
                                            INNER JOIN mst_area t2
                                            ON t.id_bp = t1.id_
                                            AND t.id_area = t2.id_
                                            GROUP BY t.id_bp, t2.id_");
                break;
        }
        return $data;
    }

    public function get_regis_uncomply_graph($idmuttype, $idarea, $regisdate)
    {
        $filtermonth = substr($regisdate,0,2);
        $filteryear = substr($regisdate,2,4); 
        if (($idmuttype != null || $idmuttype != "") && ($idarea != null || $idarea != ""))
        {
            $data = $this->db->query("SELECT COUNT(*) AS total_uncomply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NULL AND a.id_mut_type IN($idmuttype) 
                                        AND c.id_ IN($idarea) ")->row();
        }
        else if (($idmuttype != null || $idmuttype != "") && ($idarea == null || $idarea == ""))
        {
            $data = $this->db->query("SELECT COUNT(*) AS total_uncomply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NULL AND a.id_mut_type IN($idmuttype)")->row();
        }
        else if (($idmuttype == null || $idmuttype == "") && ($idarea != null || $idarea != ""))
        {
            $data = $this->db->query("SELECT COUNT(*) AS total_uncomply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NULL AND c.id_ IN($idarea)")->row();
        } else {
            $data = $this->db->query("SELECT COUNT(*) AS total_uncomply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS  NULL")->row();
        }
        return $data;
    }

    
    public function get_regis_comply_graph($idmuttype, $idarea, $regisdate)
    {
        $filtermonth = substr($regisdate,0,2);
        $filteryear = substr($regisdate,2,4); 
        if (($idmuttype != null || $idmuttype != "") && ($idarea != null || $idarea != ""))
        {
            $data = $this->db->query("SELECT COUNT(*) AS total_comply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NOT NULL AND a.id_mut_type IN($idmuttype) 
                                        AND c.id_ IN($idarea) ")->row();
        }
        else if (($idmuttype != null || $idmuttype != "") && ($idarea == null || $idarea == ""))
        {
            $data = $this->db->query("SELECT COUNT(*) AS total_comply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NOT NULL AND a.id_mut_type IN($idmuttype)")->row();
        }
        else if (($idmuttype == null || $idmuttype == "") && ($idarea != null || $idarea != ""))
        {
            $data = $this->db->query("SELECT COUNT(*) AS total_comply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NOT NULL AND c.id_ IN($idarea)")->row();
        } else {
            $data = $this->db->query("SELECT COUNT(*) AS total_comply
                                        FROM mst_store a
                                        INNER JOIN mst_distributor b
                                        INNER JOIN mst_area c
                                        ON b.id_ = a.id_dist
                                        AND c.id_ = b.id_area
                                        WHERE a.id_qrcode IS NOT NULL")->row();
        }
        return $data;
    }

    public function get_mut_type_graph()
    {
        $data = $this->db->query("SELECT id_ AS id, CONCAT('MUT ', mut_type_code) AS text 
                                    FROM mst_mut_type")->result_array();
        return $data;
    }

    public function get_bp_area_graph()
    {
        $data = $this->db->query("SELECT t2.id_ AS id, CONCAT(t1.bp_code,' ', t2.area_name) AS text
                                    FROM mst_mapping_bpartner t
                                    INNER JOIN mst_bpartner t1
                                    INNER JOIN mst_area t2
                                    ON t.id_bp = t1.id_
                                    AND t.id_area = t2.id_
                                    GROUP BY t.id_bp, t2.id_")->result_array();
        return $data;
    }

    public function get_activity_graph($idmuttype, $idarea, $regisdate)
    {
        $filtermonth = substr($regisdate,0,2);
        $filteryear = substr($regisdate,2,4); 
        if (($idmuttype != null || $idmuttype != "") && ($idarea != null || $idarea != ""))
        {
            $data = $this->db->query("SELECT (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS uncomply
                                        , (100-(
      (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
     )
    )comply
                                        , (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS libur
                                        , (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS meeting
                                        , (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS toko_tutup
                                        , (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS izin
                                        , (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS persiapan_visit
                                        , (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS cuti
                                        FROM trx_checkin_checkout a
                                        INNER JOIN trx_mapping_assignment_dtl b
                                        INNER JOIN trx_mapping_assignment c
                                        ON (b.id_ = a.id_mapping_assignment_dtl
                                        AND c.id_ = b.id_mapping_assignment)
                                        INNER JOIN mst_store d
                                        ON d.id_ = b.id_store
                                        INNER JOIN mst_mut_type e
                                        INNER JOIN mst_distributor f
                                        ON (e.id_ = d.id_mut_type
                                        AND f.id_ = d.id_dist)
                                        INNER JOIN mst_area g
                                        ON g.id_ = f.id_area
                                        WHERE e.id_ IN($idmuttype) 
                                        AND g.id_ IN($idarea) AND YEAR(a.tanggal_login) = $filteryear 
                                        AND MONTH(a.tanggal_login) = $filtermonth")->row();
        }
        else if (($idmuttype != null || $idmuttype != "") && ($idarea == null || $idarea == ""))
        {
            $data = $this->db->query("SELECT (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS uncomply
                                        , (100-(
      (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
     )
    )comply
                                        , (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS libur
                                        , (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS meeting
                                        , (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS toko_tutup
                                        , (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS izin
                                        , (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS persiapan_visit
                                        , (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS cuti
                                        FROM trx_checkin_checkout a
                                        INNER JOIN trx_mapping_assignment_dtl b
                                        INNER JOIN trx_mapping_assignment c
                                        ON (b.id_ = a.id_mapping_assignment_dtl
                                        AND c.id_ = b.id_mapping_assignment)
                                        INNER JOIN mst_store d
                                        ON d.id_ = b.id_store
                                        INNER JOIN mst_mut_type e
                                        INNER JOIN mst_distributor f
                                        ON (e.id_ = d.id_mut_type
                                        AND f.id_ = d.id_dist)
                                        INNER JOIN mst_area g
                                        ON g.id_ = f.id_area
                                        WHERE e.id_ IN($idmuttype)
                                        AND YEAR(a.tanggal_login) = $filteryear 
                                        AND MONTH(a.tanggal_login) = $filtermonth")->row();
        }
        else if (($idmuttype == null || $idmuttype == "") && ($idarea != null || $idarea != ""))
        {
            $data = $this->db->query("SELECT (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS uncomply
                                        , (100-(
      (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
     )
    )comply
                                        , (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS libur
                                        , (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS meeting
                                        , (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS toko_tutup
                                        , (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS izin
                                        , (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS persiapan_visit
                                        , (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS cuti
                                        FROM trx_checkin_checkout a
                                        INNER JOIN trx_mapping_assignment_dtl b
                                        INNER JOIN trx_mapping_assignment c
                                        ON (b.id_ = a.id_mapping_assignment_dtl
                                        AND c.id_ = b.id_mapping_assignment)
                                        INNER JOIN mst_store d
                                        ON d.id_ = b.id_store
                                        INNER JOIN mst_mut_type e
                                        INNER JOIN mst_distributor f
                                        ON (e.id_ = d.id_mut_type
                                        AND f.id_ = d.id_dist)
                                        INNER JOIN mst_area g
                                        ON g.id_ = f.id_area
                                        WHERE g.id_ IN($idarea)
                                        AND YEAR(a.tanggal_login) = $filteryear 
                                        AND MONTH(a.tanggal_login) = $filtermonth")->row();
        }
        else {
            $data = $this->db->query("SELECT (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS uncomply
                                        , (100-(
      (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
    + (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 
     )
    )comply
                                        , (IFNULL(SUM(IF(a.remarks = 0, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS libur
                                        , (IFNULL(SUM(IF(a.remarks = 2, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS meeting
                                        , (IFNULL(SUM(IF(a.remarks = 3, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS toko_tutup
                                        , (IFNULL(SUM(IF(a.remarks = 4, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS izin
                                        , (IFNULL(SUM(IF(a.remarks = 5, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS persiapan_visit
                                        , (IFNULL(SUM(IF(a.remarks = 6, 1, 0)),0) / (IFNULL(SUM(IF(d.id_qrcode IS NULL, 1, 0)),0) + IFNULL(SUM(IF(d.id_qrcode IS NOT NULL, 1, 0)),0))) * 100 AS cuti
                                        FROM trx_checkin_checkout a
                                        INNER JOIN trx_mapping_assignment_dtl b
                                        INNER JOIN trx_mapping_assignment c
                                        ON (b.id_ = a.id_mapping_assignment_dtl
                                        AND c.id_ = b.id_mapping_assignment)
                                        INNER JOIN mst_store d
                                        ON d.id_ = b.id_store
                                        INNER JOIN mst_mut_type e
                                        INNER JOIN mst_distributor f
                                        ON (e.id_ = d.id_mut_type
                                        AND f.id_ = d.id_dist)
                                        INNER JOIN mst_area g
                                        ON g.id_ = f.id_area
                                        WHERE YEAR(a.tanggal_login) = $filteryear 
                                        AND MONTH(a.tanggal_login) = $filtermonth")->row();
        }
        return $data;
    }

    public function get_pie_download_excel()
    {
        
    }

    public function get_month_graph()
    {
        $data = array();
        $months = array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli ',
            'Augustus',
            'September',
            'Oktober',
            'Nopember',
            'Desember'
        );

        for($i = 1; $i <= 12; $i++)
        {
            $check_month = $i <= 9 ? '0'.$i : $i;
            if (date('m') == '0'.$check_month)
                $data[] = array('id'=>$check_month, 'text'=>date('F',strtotime('01.'.$i.'.2001')), 'selected'=>true);
            else $data[] = array('id'=>$check_month, 'text'=>date('F',strtotime('01.'.$i.'.2001')));
        }
        return $data;
    }

    public function get_year_graph()
    {
        $data = array();
        $data[] = array('id'=>date('Y'), 'text'=>date('Y'), 'selected'=>true);
        $data[] = array('id'=>date('Y')-1, 'text'=>date('Y')-1);
        $data[] = array('id'=>date('Y')-2, 'text'=>date('Y')-2);
        return $data;
    }

    public function get_export_dashboard()
    {
        $data = $this->db->query("
            SELECT 
                x.id_area 'Area ID',
                x1.area_name 'Area Name',
                x1.area_code 'Area Code',
                x1.mut_gt 'MUT GT',
                x.store_gt 'Store GT',
                x.regis_gt 'Register GT',
                x.non_regis_gt 'Non Register GT',
                SUM((x.regis_gt / x.store_gt) * 100) '% Registrasi Toko GT',
                x1.mut_fg 'MUT FG',
                x.store_fg 'Store FG',
                x.regis_fg 'Register FG',
                x.non_regis_fg 'Non Register FG',
                SUM((x.regis_fg / x.store_fg) * 100) '% Registrasi Toko FG',
                x1.mut_ic 'MUT IC',
                x.store_ic 'Store IC',
                x.regis_ic 'Register IC',
                x.non_regis_ic 'Non Register IC',
                SUM((x.regis_ic / x.store_ic) * 100) '% Registrasi Toko IC',
                x1.mut_nasional 'MUT Nasional',
                x.store_nasional 'Store Nasional',
                x.regis_nasional 'Register Nasional',
                x.non_regis_nasional 'Non Register Nasional',
                SUM((x.regis_nasional / x.store_nasional) * 100) '% Registrasi Toko Nasional'
            FROM
                (SELECT 
                    t1.id_area,
                        COALESCE(SUM(t.id_ AND t.id_mut_type = 2)) store_gt,
                        COALESCE(SUM(t.id_qrcode IS NOT NULL
                            AND t.id_mut_type = 2)) regis_gt,
                        COALESCE(SUM(t.id_qrcode IS NULL
                            AND t.id_mut_type = 2)) non_regis_gt,
                        COALESCE(SUM(t.id_ AND t.id_mut_type = 1)) store_fg,
                        COALESCE(SUM(t.id_qrcode IS NOT NULL
                            AND t.id_mut_type = 1)) regis_fg,
                        COALESCE(SUM(t.id_qrcode IS NULL
                            AND t.id_mut_type = 1)) non_regis_fg,
                        COALESCE(SUM(t.id_ AND t.id_mut_type = 3)) store_ic,
                        COALESCE(SUM(t.id_qrcode IS NOT NULL
                            AND t.id_mut_type = 3)) regis_ic,
                        COALESCE(SUM(t.id_qrcode IS NULL
                            AND t.id_mut_type = 3)) non_regis_ic,
                        COALESCE(COUNT(t.id_)) store_nasional,
                        COALESCE(SUM(t.id_qrcode IS NOT NULL)) regis_nasional,
                        COALESCE(SUM(t.id_qrcode IS NULL)) non_regis_nasional
                FROM
                    mst_store t
                INNER JOIN mst_distributor t1 ON t.id_dist = t1.id_
                GROUP BY t1.id_area) x
                    INNER JOIN
                (SELECT 
                    t.id_area,
                        t1.area_name,
                        t1.area_code,
                        SUM(t.total_mut_gt) mut_gt,
                        SUM(t.total_mut_fg) mut_fg,
                        SUM(t.total_mut_ic) mut_ic,
                        SUM(t.total_mut_gt + t.total_mut_fg + t.total_mut_ic) mut_nasional
                FROM
                    mst_mapping_bpartner t
                INNER JOIN mst_area t1 ON t.id_area = t1.id_
                GROUP BY t.id_area) x1 ON x.id_area = x1.id_area
            GROUP BY x.id_area
        ");
        return $data;
    }

}
