<?php 
class Model_store extends CI_Model {

    function tampilkan_store_reg($lmt,$offset) { 
		
			$cari_bp = $_SESSION['identity'];
			$bp_id = substr($cari_bp,3,2);
			$id_bp = ltrim($bp_id,'0');
			$cek_admin =  substr($cari_bp,0,3);
			$bp_id ="";
			
			if($cek_admin != "ULI"){
					$bp_id = "and t3.id_ = ".$id_bp; 
				}
	
	 	$connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");

        $data =  mysqli_query($connection,"SELECT * FROM(select t.id_, t.store_name, t.code_outlet, t.golongan, t.id_dist, 
											t1.dist_name, t1.id_bp, t3.bp_name, t1.id_area, t2.area_name 
											from tbl_store t
											inner join tbl_distributor t1
											inner join tbl_area t2
											inner join tbl_business_partner t3
											on t.id_dist = t1.id_
											and t1.id_area = t2.id_
											and t1.id_bp = t3.id_
											and t.status_store = 1 
											$bp_id
											limit $lmt offset $offset)t4
                                            inner join tbl_qrcode
                                            on t4.id_ = tbl_qrcode.id_store");
        return $data;

	}
	
    function show_store_reg($lmt,$offset) { 
		
        $cari_bp = $_SESSION['identity'];
        $bp_id = substr($cari_bp,3,2);
        $id_bp = ltrim($bp_id,'0');
        $cek_admin =  substr($cari_bp,0,3);
        $bp_id ="";
        
        if($cek_admin != "ULI"){
                $bp_id = "id_bp = ".$id_bp; 
            }
	
	 	$connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");

        //echo $data = "
        $data =  mysqli_query($connection,"
                    SELECT 
                        t3.*
                    FROM
                        (SELECT 
                            t2.*, tbl_qrcode.id_qrcode 'QrCode'
                        FROM
                            (SELECT 
                            s.id_,
                                s.store_name 'Store Name',
                                s.estimation_time 'Estimation Time',
                                s.pasar_residential 'Pasar Residential',
                                s.alamat 'Alamat',
                                s.golongan 'Golongan',
                                c.cluster_name 'Cluster Name',
                                s.code_outlet 'Code Outlet',
                                IF(s.status_store = '1', 'Register', 'Not Register') 'Status Store',
                                d.id_area,
					            d.area_name 'Area Name',
                                d.id_bp,
                                d.id_ 'Id Distributor',
                                d.bp_name 'Bussiness Partner',
                                d.dist_name 'Distributor Name',
                                ts.type_store_name 'Type Store Name'
                        FROM
                            tbl_store s
                        LEFT JOIN tbl_cluster c ON c.id_ = s.id_cluster
                        LEFT JOIN tbl_type_store ts ON ts.id_ = s.id_type_store
                        LEFT JOIN (SELECT 
                            tbl_distributor.*,
                                tbl_area.area_name,
                                tbl_business_partner.bp_name
                        FROM
                            tbl_distributor
                        LEFT JOIN tbl_area ON tbl_area.id_ = tbl_distributor.id_area
                        LEFT JOIN tbl_business_partner ON tbl_business_partner.id_ = tbl_distributor.id_bp) d ON s.id_dist = d.id_) t2
                        LEFT JOIN (SELECT 
                            *
                        FROM
                            tbl_qrcode
                        WHERE
                            id_store IS NOT NULL AND id_store != 0
                        GROUP BY id_store) tbl_qrcode ON tbl_qrcode.id_store = t2.id_) t3
                    WHERE
                    $bp_id limit $lmt offset $offset  
						    "); //die();
        return $data;
	}

	function show_store_reg_cari($lmt,$offset,$search_field,$search_text) { 		
	$cari_bp = $_SESSION['identity'];
	$bp_id = substr($cari_bp,3,2);
	$id_bp = ltrim($bp_id,'0');
	$cek_admin =  substr($cari_bp,0,3);
	$bp_id ="";
	
	if($cek_admin != "ULI"){
			$bp_id = "id_bp = ".$id_bp; 
		}

 	$connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");

	if (empty($search_text)) {$search_text  = $this->uri->segment(5);}
	if (empty($search_field)){$search_field = $this->uri->segment(4);}
	$search_text = str_replace('%20',' ', $search_text);
	if($search_field == 'id_qrcode'){
		$search_qrcode = "AND QrCode = '$search_text'";
	}else{
		if ($search_field == 's.id_'){
			$search_all = "AND $search_field = '$search_text'";
		}else{
			$search_all	= "AND $search_field LIKE '%$search_text%'";
			$search_qrcode = "";
		}
	}

        $data =  mysqli_query($connection,"
                    SELECT 
                        t3.*
                    FROM
                        (SELECT 
                            t2.*, tbl_qrcode.id_qrcode 'QrCode'
                        FROM
                            (SELECT 
                            s.id_,
                                s.store_name 'Store Name',
                                s.estimation_time 'Estimation Time',
                                s.pasar_residential 'Pasar Residential',
                                s.alamat 'Alamat',
                                s.golongan 'Golongan',
                                c.cluster_name 'Cluster Name',
                                s.code_outlet 'Code Outlet',
                                IF(s.status_store = '1', 'Register', 'Not Register') 'Status Store',
                                d.id_area,
                                d.area_name 'Area Name',
                                d.id_bp,
                                d.id_ 'Id Distributor',
                                d.bp_name 'Bussiness Partner',
                                d.dist_name 'Distributor Name',
                                ts.type_store_name 'Type Store Name'
                        FROM
                            tbl_store s
                        LEFT JOIN tbl_cluster c ON c.id_ = s.id_cluster
                        LEFT JOIN tbl_type_store ts ON ts.id_ = s.id_type_store
                        LEFT JOIN (SELECT 
                            tbl_distributor.*,
                                tbl_area.area_name,
                                tbl_business_partner.bp_name
                        FROM
                            tbl_distributor
                        LEFT JOIN tbl_area ON tbl_area.id_ = tbl_distributor.id_area
                        LEFT JOIN tbl_business_partner ON tbl_business_partner.id_ = tbl_distributor.id_bp) d ON s.id_dist = d.id_ $search_all) t2
                        LEFT JOIN (SELECT 
                            *
                        FROM
                            tbl_qrcode
                        WHERE
                            id_store IS NOT NULL AND id_store != 0
                        GROUP BY id_store) tbl_qrcode ON tbl_qrcode.id_store = t2.id_) t3
                    WHERE $bp_id $search_qrcode
			limit $lmt offset $offset
	");// die(); 
    return $data;
	}

	function tampilkan_checkin_out($lmt,$offset) { 		
		$cari_bp = $_SESSION['identity'];
		$bp_id = substr($cari_bp,3,2);
		$id_bp = ltrim($bp_id,'0');
		$cek_admin =  substr($cari_bp,0,3);
		$bp_id ="";
		
		if($cek_admin != "ULI"){
				$bp_id = "and y.id_bp = ".$id_bp; 
			}
				

	 	$connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");

        $data =  mysqli_query($connection,"
        										SELECT 
												    y.*, y1.bp_name, y2.name_mut, y2.type_mut, y3.area_name
												FROM
												    (SELECT 
												        *, TRIM(LEADING '0' FROM id_vendor) id_bp
												    FROM
												        (SELECT 
													        t.tanggal_login,
												            t.username,
												            t.id_qrcode,
												            t1.store_name,
												            t.days,
												            t.weeks,
												            t3.cluster_name,
												            t.jam_checkin,
												            t.jam_checkout,
												            timediff(t.jam_checkout,t.jam_checkin) 'Durasi Kerja',
												            t1.id_dist,
												            t2.id_area,
												            t.week_in_year,
												            SUBSTR(t.username, 4, 2) id_vendor
													    FROM
													        tbl_report_checkin_checkout t
													    INNER JOIN tbl_store t1
													    INNER JOIN tbl_distributor t2 
													    INNER JOIN tbl_cluster t3
													    ON t.id_store = t1.id_
													        AND t1.id_dist = t2.id_
															AND t1.id_cluster = t3.id_
													    ORDER BY t.tanggal_login ASC) x) y
												        INNER JOIN
												    tbl_business_partner y1
												        INNER JOIN
												    tbl_mut y2
												        INNER JOIN
												    tbl_area y3 ON y.id_bp = y1.id_
												        AND y.username = y2.code_mut
												        AND y.id_area = y3.id_
												        $bp_id
												        where y.tanggal_login BETWEEN SUBDATE(CURDATE(), INTERVAL 15 DAY)  AND CURDATE()
												        order by tanggal_login desc, jam_checkin desc, store_name asc
												        $dt_limit
											limit $lmt offset $offset
											"); //die();
        return $data;
	}

	function tampilkan_checkin_out_cari($lmt,$offset,$pencarian_data) 
	{
		$cari_bp = $_SESSION['identity'];
		$bp_id = substr($cari_bp,3,2);
		$id_bp = ltrim($bp_id,'0');
		$cek_admin =  substr($cari_bp,0,3);
		$bp_id ="";
		if($cek_admin != "ULI"){$bp_id = "and y.id_bp = ".$id_bp;}

		$pencarian_data = str_replace("%20"," ", $pencarian_data);
	 	$connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");

        //echo $data = "
        $data =  mysqli_query($connection,"
			SELECT 
			    y.*, y1.bp_name, y2.name_mut, y2.type_mut, y3.area_name
			FROM
			    (SELECT 
			        *, TRIM(LEADING '0' FROM id_vendor) id_bp
			    FROM
			        (SELECT 
			        t.tanggal_login,
			            t.username,
			            t.id_qrcode,
			            t1.store_name,
			            t.days,
			            t.weeks,
			            t3.cluster_name,
			            t.jam_checkin,
			            t.jam_checkout,
			            TIMEDIFF(t.jam_checkout, t.jam_checkin) 'Durasi Kerja',
			            mid(t.username,6,3) id_area,
			            t.week_in_year,
			            SUBSTR(t.username, 4, 2) id_vendor
			    FROM
			        tbl_report_checkin_checkout t
			    INNER JOIN tbl_store t1
			    INNER JOIN tbl_cluster t3 ON t.id_store = t1.id_
			        AND t1.id_cluster = t3.id_
			    ORDER BY t.tanggal_login ASC) x) y
			        INNER JOIN
			    tbl_business_partner y1
			        INNER JOIN
			    tbl_mut y2
			        INNER JOIN
			    tbl_area y3 ON y.id_bp = y1.id_
			        AND y.username = y2.code_mut
			        AND y.id_area = y3.code_area
		    $bp_id
			$pencarian_data
		    order by tanggal_login desc, jam_checkin desc, store_name asc 
			limit $lmt offset $offset
		"); //die();
        return $data;
	}
	

}