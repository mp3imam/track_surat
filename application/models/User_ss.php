<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_ss extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->dbutil();
        $this->lang->load('admin/database');

        /* Title Page :: Common */
        $this->page_title->push('Supervisor');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'List User SS', 'admin/user_ss');
        $this->load->library('grocery_CRUD');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */  
            $this->grocery_crud->unset_jquery();
            $this->grocery_crud->unset_print();     /* GROCERY CRUD PRINT OFF */ 
            $this->grocery_crud->set_table('tbl_ss');
            $this->grocery_crud->set_theme('datatables');

            $cari_bp = $_SESSION['identity'];
            $admin = substr($cari_bp,0,3);
			$admin_ao = substr($cari_bp,0,5);
			$id_bp = substr($cari_bp,5,3);
			$query_area = $this->db->query("SELECT tbl_area.id_ FROM tbl_area INNER JOIN m_city ON tbl_area.id_city = m_city.id_ AND m_city.code_city = '$id_bp'");
			foreach ($query_area->result_array() as $row) {
					$id_area = $row['id_'];
			}	

            if($admin == 'BPR'){
                $id_bp = ltrim(substr($cari_bp,3,2),'0');
                $this->grocery_crud->where('id_bp',$id_bp);
            }else if($admin_ao == 'ULI01'){
				$this->grocery_crud->where('id_area',$id_area);
            }

            $this->grocery_crud->columns('id_region','id_area','id_bp','name_ss','code_ss','password','telp_ss','email_ss','type_ss');
            $this->grocery_crud->fields('id_region','id_area','id_bp','name_ss','code_ss','password','telp_ss','email_ss','type_ss');

            $this->grocery_crud->set_relation('id_region','tbl_region','region_name');
            //$this->grocery_crud->set_relation('id_city','m_city','city');
            $this->grocery_crud->set_relation('id_area','tbl_area','area_name');
            $this->grocery_crud->set_relation('id_bp','tbl_business_partner','bp_name');

            $this->grocery_crud->display_as('id_region','Region Name');
            //$this->grocery_crud->display_as('id_city','City Name');
            $this->grocery_crud->display_as('id_area','Area Name');
            $this->grocery_crud->display_as('id_bp','Business Partner Name');
            $this->grocery_crud->display_as('name_ss','Supervisor Name');
            $this->grocery_crud->display_as('code_ss','Supervisor Code');
            $this->grocery_crud->display_as('password','Password');
            $this->grocery_crud->display_as('telp_ss','Telp');
            $this->grocery_crud->display_as('email_ss','E-Mail');
            $this->grocery_crud->display_as('type_ss','Type Supervisor');

            $this->grocery_crud->unset_add();  
            $this->grocery_crud->unset_edit();  
            $this->grocery_crud->add_action('Edit', '/mut_webadmin/assets/grocery_crud/themes/flexigrid/css/images/edit.png', '','ui-icon-image',array($this,'form_editSS'));


            

            $this->data['output'] = $this->grocery_crud->render();
            //$this->data['list_tables'] = '';

            /* Load Template */
            $this->template->admin_render('admin/blanks/index_user_ss', $this->data);
        }
	}

     function form_editSS($primary_key , $row)
    {
        return site_url('admin/User_ss/edit_ss/').$row->id_;   
    }

    public function add_ss(){
        $this->grocery_crud->set_model('Model_area');
        $this->grocery_crud->set_model('Model_user_ss');
        if(isset($_POST['submit'])) {
            $this->Model_user_ss->tambah_ss();
            redirect('admin/User_ss');          
        } 
        else
        {
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['output']   = $this->uri->segment(4);
            $this->data['region']   = $this->Model_area->ambil_region();
            $this->data['cluster']   = $this->Model_area->ambil_cluster();
            $this->data['type_store']   = $this->Model_area->ambil_type_store();
            $this->template->admin_render('admin/blanks/add_ss',$this->data);
        }
    }

    function edit_ss() {
        $this->grocery_crud->set_model('Model_area');
        $this->grocery_crud->set_model('Model_user_ss');
        
        if(isset($_POST['submit'])) {
            $this->Model_user_ss->update_ss();
            redirect('admin/User_ss');          
        } 
        else
        {
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['output']   = $this->uri->segment(4);
            $this->data['id_ss']  = $this->uri->segment(4);
            $this->data['tampung']   = $this->Model_user_ss->tampung_ss($this->uri->segment(4));
            $this->data['region']   = $this->Model_area->ambil_region();
            $this->data['cluster']   = $this->Model_area->ambil_cluster();
            $this->data['type_store']   = $this->Model_area->ambil_type_store();
            $this->template->admin_render('admin/blanks/edit_ss',$this->data);
        }
    }

    public function code_spv() {
        $this->grocery_crud->set_model('Model_user_ss');
        $this->data['code_spv']=$this->Model_user_ss->create_code_spv($this->uri->segment(4));
        $this->load->view('admin/blanks/v_auto_code_spv',$this->data);           
    }


}
