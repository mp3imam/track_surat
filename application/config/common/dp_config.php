<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['assets_dir']     = 'assets';
$config['frameworks_dir'] = $config['assets_dir'] . '/frameworks';
$config['plugins_dir']    = $config['assets_dir'] . '/plugins';
$config['images_dir']     = $config['assets_dir'] . '/images';
$config['store_dir']      = "http://mut-hp.pinepic.com/mut/photos/";
$config['hp_url']           = "http://mut-hp.pinepic.com/";
$config['hp_cico_ho']  		= $config['hp_url'].'mut/hp_cico_ho/';
$config['hp_cico_bp']  		= $config['hp_url'].'mut/hp_cico_bp/';
$config['hp_stock_detail']  = $config['hp_url'].'mut/get_stock_detail/';

$config['upload_dir']     = 'upload';
$config['avatar_dir']     = $config['upload_dir'] . '/avatar';
$config['hp_holiday']  = $config['hp_url'].'mut/holiday/';

