<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepala_Bagian extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
		
		/* Load :: Common */
		$this->load->helper('number');
		$this->load->model('admin/KepalaBagian_model');
		$this->load->model('ion_auth_model');
	}

	public function index()
	{
		/* Title Page */
		$this->page_title->push(lang('menu_surat'));
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs */
		$this->breadcrumbs->unshift(1, 'Master Disposisi Surat', 'menu_surat');
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Load Template */
		if ($this->ion_auth->is_adminkb())
			$this->template->adminkb_render('kepala_bagian/kb_list', $this->data);
	}

	public function get_kb_list()
	{
		$filter = $this->set_kb_filter();

		$this->load->library('Datatable', array('model' => 'KepalaBagian_model'));
		$mylib = new datatable(array('model' => 'KepalaBagian_model', 'rowIdCol' => 'a.id', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	private function set_kb_filter()
	{
		$filter = array("equals" => array(), "contains" => array());

		return $filter;
	}

	public function reset_password()
	{
      	$this->KepalaBagian_model->reset_password();
	  	redirect('kepala_bagian');  
	}

	public function kb_edit($id)
	{
		/* Title Page */
		$this->page_title->push("Edit kb MUT");
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		/* Model get Editnya */
		$this->data['id_edit'] = $this->KepalaBagian_model->get_edit($id);

		/* Load Template */
		if ($this->ion_auth->is_adminkb())
			$this->template->adminkb_render('kepala_bagian/kb_edit', $this->data);
	}

	public function post_edit($id_)
	{
      	$this->KepalaBagian_model->update_login($id_);
	  	redirect('kepala_bagian');  
	}

	public function export_excel()
	{
		$filter = $this->set_kepala_bagian_filter();
		$query = $this->KepalaBagian_model->get_export_data($filter);
		if(!$query)
            return false;
		
		try
		{
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("kbname");

			$fields = $query->list_fields();
			$type = array();
			$col = 0;
			foreach ($fields as $field)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				switch ($field){
					default:
						array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
						break;
				}
				$col++;
			}

			$row = 2;
			foreach($query->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
					$col++;
				}
				$row++;
			}

			// column size
			foreach(range('A','Z') as $columnID) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}
					
			$filename = "kbname ".date('Y-m-d H-i-s').".xlsx";
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');

			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}
		catch(Exception $e)
		{
			//alert the kb.
			var_dump($e->getMessage());
		}
	}

}
