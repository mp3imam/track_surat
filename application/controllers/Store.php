<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		
		/* Load :: Common */
		$this->load->helper('number');
		$this->load->helper('url');
		$this->load->model('admin/Area_model');
		$this->load->model('admin/Region_model');
		$this->load->model('admin/Account_model');
		$this->load->model('admin/BusinessPartner_model');
		$this->load->model('admin/Distributor_model');
		$this->load->model('admin/Cluster_model');
		$this->load->model('admin/Store_model');
	}

	public function account_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_account'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data Account', 'store/account_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/account_list', $this->data);
		}
	}

	public function account_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Add account");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/account_add', $this->data);
		}
	}

	public function post_account_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Account_model->account_save();
		  	redirect('store/account_list');  
        }		
	}

	public function account_edit()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Edit Account");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get category vendor */
			$this->data['temp'] = $this->Account_model->get_account_edit();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/account_edit', $this->data);
		}
	}

	public function post_account_edit(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Account_model->account_update();
		  	redirect('store/account_list');  
        }		
	}

	public function dist_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_distributor'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data Distributor', 'store/dist_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/dist_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('store/dist_list', $this->data);
		}
	}

	public function dist_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Add dist");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get category vendor */
			$this->data['temp'] = $this->Area_model->get_region_area();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/dist_add', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('store/dist_add', $this->data);
		}
	}

	public function get_viewQrcode($id_store){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
        	$id_store = $this->uri->segment(3);
	        $get_store_view = $this->Store_model->get_store_view($id_store);
			$jsonArray = array('result_viewQrcode'=>$get_store_view);
			
			$this->output->set_header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache");
			$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
		}
	}

    public function reset_qrcode() {
        $id_store 		= $this->uri->segment(3);
        $id_dist 		= $this->uri->segment(4);
        $id_area 		= $this->uri->segment(5);
        $last_qrcode 	= $this->uri->segment(6);
        $this->Store_model->reset_qrcode($id_store,$id_dist,$id_area,$last_qrcode);
		redirect('store/store_list');  
    }

	public function post_dist_add(){
		if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Distributor_model->dist_save();
		  	redirect('store/dist_list');  
        }		
	}

	public function dist_edit()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Edit Distributor");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get category vendor */
			$this->data['temp'] = $this->Distributor_model->get_dist_edit();

			/* Model get BP_Name */
			$this->data['bpname'] = $this->BusinessPartner_model->get_id_mp($this->data['temp'][0]['id_area'],$this->data['temp'][0]['id_mp']);

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/dist_edit', $this->data);
		}
	}

	public function post_dist_edit(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Distributor_model->dist_update();
		  	redirect('store/dist_list');  
        }		
	}

    public function pilih_bp() {
        $this->data['area']=$this->Area_model->ambil_bp($this->uri->segment(3));
        $this->load->view('dropdown/v_drop_down',$this->data);
    }

    public function pilih_cluster() {
        $this->data['area']=$this->Area_model->ambil_cluster($this->uri->segment(3));
        $this->load->view('dropdown/v_drop_down',$this->data);
    }

	public function cluster_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_cluster'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data Cluster', 'store/cluster_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/cluster_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('store/cluster_list', $this->data);
		}
	}

	public function cluster_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Add cluster");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get category vendor */
			$this->data['temp'] = $this->Cluster_model->get_type_cluster();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/cluster_add', $this->data);
		}
	}

	public function post_cluster_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Cluster_model->cluster_save();
		  	redirect('store/cluster_list');  
        }		
	}

	public function store_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_store'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data Store', 'store/store_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('store/store_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('store/store_list', $this->data);
		}
	}

	public function store_is_delete()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Delete And Restore Store");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Load Template */
			if ($this->ion_auth->is_adminho()){
				$this->data['region'] = $this->Region_model->get_region();
				$this->template->adminho_render('store/store_is_delete', $this->data);
			}
		}
	}

	public function post_store_is_delete(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Store_model->store_id_delete($this->uri->segment(3));
		  	redirect('store/store_list');  
        }		
	}

	public function store_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Add store");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get Type Cluster */
			$this->data['temp'] = $this->Cluster_model->get_type_cluster();

			/* Load Template */
			if ($this->ion_auth->is_adminho()){
				$this->data['region'] = $this->Region_model->get_region();
				$this->template->adminho_render('store/store_add', $this->data);
			}

			if ($this->ion_auth->is_adminbp()){
				$bp_code = substr($this->session->userdata('username'),3,3);
				$this->data['region'] = $this->Region_model->get_region_bp($bp_code);
				$this->template->adminbp_render('store/store_add', $this->data);
			}
		}
	}

	public function post_store_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Store_model->store_save(0);
		  	redirect('store/store_list');  
        }		
	}

	public function store_edit($id)
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Edit store");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get Type Cluster */
			$this->data['type_clusters'] = $this->Cluster_model->get_type_cluster();

			/* Model get store mut */
			$this->data['stores'] = $this->Store_model->get_store_edit($id);

			/* Model get Cluster */
			$this->data['clusters'] = $this->Cluster_model->get_cluster($this->data['stores'][0]['id_mut_type']);

			/* Load Template */
			if ($this->ion_auth->is_adminho()){
				$bp_code = substr($this->session->userdata('username'),3,3);
				$this->data['region'] = $this->Region_model->get_region_bp($bp_code);
				$this->template->adminho_render('store/store_edit', $this->data);
			}

			if ($this->ion_auth->is_adminbp()){
				$bp_code = substr($this->session->userdata('username'),3,3);
				$this->data['region'] = $this->Region_model->get_region_bp($bp_code);
				$this->template->adminbp_render('store/store_edit', $this->data);
			}
		}
	}

	public function post_store_edit(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Store_model->store_save(1);
		  	redirect('store/store_list'); 
        }		
	}

	public function get_account_list()
	{
		$filter = $this->set_account_filter();

		$this->load->library('Datatable', array('model' => 'Account_model'));
		$mylib = new datatable(array('model' => 'Account_model', 'rowIdCol' => 'id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	public function get_dist_list()
	{
		$filter = $this->set_dist_filter();

		$this->load->library('Datatable', array('model' => 'Distributor_model'));
		$mylib = new datatable(array('model' => 'Distributor_model', 'rowIdCol' => 'a.id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	public function get_cluster_list()
	{
		$filter = $this->set_cluster_filter();

		$this->load->library('Datatable', array('model' => 'Cluster_model'));
		$mylib = new datatable(array('model' => 'Cluster_model', 'rowIdCol' => 'a.id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	public function get_store_list()
	{
		$filter = $this->set_store_filter();

		$this->load->library('Datatable', array('model' => 'Store_model'));
		$mylib = new datatable(array('model' => 'Store_model', 'rowIdCol' => 'a.id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	private function set_account_filter()
	{
		$filter = array("equals" => array(), "contains" => array());
		return $filter;
	}

	private function set_dist_filter()
	{
		$filter = array("equals" => array(), "contains" => array());
		if ($this->ion_auth->is_adminbp())
		{
			$filter["equals"]["a.user_mk"] = $this->session->userdata('username');
		}
		return $filter;
	}

	private function set_cluster_filter()
	{
		$filter = array("equals" => array(), "contains" => array());
		if ($this->ion_auth->is_adminbp())
		{
			$filter["equals"]["a.created_by"] = $this->session->userdata('username');
		}
		return $filter;
	}

	private function set_store_filter()
	{
		$filter = array("equals" => array(), "contains" => array());
		if ($this->ion_auth->is_adminbp())
		{
			$filter["equals"]["h.bp_code"] = substr($this->session->userdata('username'),3,3);
		}

		if ($this->input->post('muttype') !== "" && $this->input->post('muttype') !== null)
			$filter["equals"]["b.id_"] = $this->input->post('muttype');
		if ($this->input->post('distname') !== "" && $this->input->post('distname') !== null)
			$filter["contains"]["d.dist_name"] = $this->input->post('distname');
		if ($this->input->post('storename') !== "" && $this->input->post('storename') !== null)
			$filter["contains"]["a.store_name"] = $this->input->post('storename');
		if ($this->input->post('qrcode') !== "" && $this->input->post('qrcode') !== null)
			$filter["contains"]["a.id_qrcode"] = $this->input->post('qrcode');

		$filter["equals"]["is_deleted"] = NULL;

		return $filter;
	}

	public function pilih_area() {
        $this->load->model('Area_model');
        if ($this->ion_auth->is_adminho()){
        	$this->data['area']=$this->Area_model->ambil_area($this->uri->segment(3));
        	$this->load->view('dropdown/v_drop_down',$this->data);
        }
        if ($this->ion_auth->is_adminbp()){
        	$bp_code = substr($this->session->userdata('username'),3,3);
        	$this->data['area']=$this->Area_model->ambil_area_bp($this->uri->segment(3),$bp_code);
        	$this->load->view('dropdown/v_drop_down',$this->data);
        }
    }

    public function pilih_distributor() {
        $this->load->model('Area_model');
        if ($this->ion_auth->is_adminho()){
	        $this->data['area']=$this->Distributor_model->ambil_distributor($this->uri->segment(3));
	        $this->load->view('dropdown/v_drop_down',$this->data);
	    }
	    if ($this->ion_auth->is_adminbp()){
	    	$bp_code = substr($this->session->userdata('username'),3,3);
	        $this->data['area']=$this->Distributor_model->ambil_distributor_bp($this->uri->segment(3),$bp_code);
	        $this->load->view('dropdown/v_drop_down',$this->data);
	    }
    }

	public function export_excel_account() {
        $filter = $this->set_account_filter();
		$query = $this->Account_model->get_export_account($filter);
		if(!$query)
            return false;
        
        $this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Account");

		$fields = $query->list_fields();
		$type = array();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			switch ($field){
				default:
					array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
					break;
			}
            $col++;
        }

		$row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
                $col++;
            }
            $row++;
        }

		// column size
		foreach(range('A','Z') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}
        
		$filename = "Account ".date('Y-m-d H-i-s').".xlsx";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}

	public function export_excel_cluster() {
        $filter = $this->set_cluster_filter();
		$query = $this->Cluster_model->get_export_cluster($filter);
		if(!$query)
            return false;
        
        $this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Cluster");

		$fields = $query->list_fields();
		$type = array();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			switch ($field){
				default:
					array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
					break;
			}
            $col++;
        }

		$row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
                $col++;
            }
            $row++;
        }

		// column size
		foreach(range('A','Z') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}
        
		$filename = "Cluster ".date('Y-m-d H-i-s').".xlsx";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}

	public function export_excel_dist() {
        $filter = $this->set_dist_filter();
		$query = $this->Distributor_model->get_export_dist($filter);
		if(!$query)
            return false;
        
        $this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Distributor");

		$fields = $query->list_fields();
		$type = array();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			switch ($field){
				default:
					array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
					break;
			}
            $col++;
        }

		$row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
                $col++;
            }
            $row++;
        }

		// column size
		foreach(range('A','Z') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}
        
		$filename = "Distributor ".date('Y-m-d H-i-s').".xlsx";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}

	public function export_excel_store() {
		$filter = array("equals" => array(), "contains" => array());
		$currentURL = current_url();
		$paramVal0   = $this->input->get('mtype');
		$paramVal1   = $this->input->get('dist');
		$paramVal2   = $this->input->get('store');

		$filter = array();
		if ($this->ion_auth->is_adminbp())
		{
			$filter["contains"]["bp_code"] = substr($this->session->userdata('username'),3,3);
		}

		if(!empty($paramVal0) || $paramVal0 != null)
			$filter["equals"]["b.id_"] = $paramVal0;

		if (!empty($paramVal1) || $paramVal1 != null)
			$filter["contains"]['d.dist_name'] = $paramVal1;

		if (!empty($paramVal2) || $paramVal2 != null)
			$filter["contains"]['a.store_name'] = $paramVal2;

		if ($this->ion_auth->is_adminho())
		{
			if ((empty($paramVal2) || $paramVal2 == null) && 
				(empty($paramVal1) || $paramVal1 == null) && 
				(empty($paramVal0) || $paramVal0 == null))
            redirect($this->data['hp_url'] . "mut/store");
    	}

		$query = $this->Store_model->get_export_data($filter);
		if(!$query)
            return false;
		
		try
		{
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$activeSheet = $objPHPExcel->getActiveSheet();
			$activeSheet->setTitle("Store");

			$fields = $query->list_fields();
			$type = array();
			$col = 0;
			foreach ($fields as $field)
			{
				$activeSheet->setCellValueByColumnAndRow($col, 1, $field);
				switch ($field){
					default:
						array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
						break;
				}
				$col++;
			}

			$row = 2;
			ini_set('memory_limit', '2048M');
			foreach($query->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$activeSheet->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
					$col++;
				}
				$row++;
			}

			// column size
			foreach(range('A','Z') as $columnID) {
				$activeSheet->getColumnDimension($columnID)
					->setAutoSize(true);
			}
			
			$filename = "Store ".date('Y-m-d H-i-s').".xlsx";

			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}
		catch(Exception $e)
		{
			var_dump($e->getMessage());
		}
	}
}
