<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct()
    {
        parent::__construct();
		if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/Dashboard_model');
	}
	
	public function index()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_dashboard'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminsu())
				$this->template->adminsu_render('dashboard/index', $this->data);
			if ($this->ion_auth->is_adminkb())
				$this->template->adminkb_render('dashboard/index', $this->data);
			if ($this->ion_auth->is_adminmk())
				$this->template->adminmk_render('dashboard/index', $this->data);
			if ($this->ion_auth->is_adminsg())
				$this->template->adminsg_render('dashboard/index', $this->data);
			if ($this->ion_auth->is_admintl())
				$this->template->admintl_render('dashboard/index', $this->data);
		}
	}

}
