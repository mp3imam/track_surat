<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_login extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
		
		/* Load :: Common */
		$this->load->helper('number');
		$this->load->model('admin/Userlogin_model');
		$this->load->model('ion_auth_model');
	}

	public function index()
	{
		/* Title Page */
		$this->page_title->push(lang('menu_users'));
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs */
		$this->breadcrumbs->unshift(1, 'Master Data User Login', 'user_login');
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Load Template */
		if ($this->ion_auth->is_adminsu())
			$this->template->adminsu_render('user_login/user_list', $this->data);
	}

	public function get_user_list()
	{
		$filter = $this->set_user_filter();

		$this->load->library('Datatable', array('model' => 'Userlogin_model'));
		$mylib = new datatable(array('model' => 'Userlogin_model', 'rowIdCol' => 'a.id', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	private function set_user_filter()
	{
		$filter = array("equals" => array(), "contains" => array());

		return $filter;
	}

	public function reset_password()
	{
      	$this->Userlogin_model->reset_password();
	  	redirect('user_login');  
	}

	public function user_edit($id)
	{
		/* Title Page */
		$this->page_title->push("Edit User MUT");
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		/* Model get Editnya */
		$this->data['id_edit'] = $this->Userlogin_model->get_edit($id);

		/* Load Template */
		if ($this->ion_auth->is_adminsu())
			$this->template->adminsu_render('user_login/user_login_edit', $this->data);
	}


	public function user_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("User Add");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Load Template */
			if ($this->ion_auth->is_adminsu())
				$this->template->adminsu_render('user_login/user_login_add', $this->data);
		}
	}

	public function post_user_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Userlogin_model->user_save();
		  	redirect('user_login');  
        }	
	}

	public function post_edit($id_)
	{
      	$this->Userlogin_model->update_login($id_);
	  	redirect('user_login');  
	}

	public function export_excel()
	{
		$filter = $this->set_user_login_filter();
		$query = $this->Userlogin_model->get_export_data($filter);
		if(!$query)
            return false;
		
		try
		{
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("Username");

			$fields = $query->list_fields();
			$type = array();
			$col = 0;
			foreach ($fields as $field)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				switch ($field){
					default:
						array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
						break;
				}
				$col++;
			}

			$row = 2;
			foreach($query->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
					$col++;
				}
				$row++;
			}

			// column size
			foreach(range('A','Z') as $columnID) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}
					
			$filename = "Username ".date('Y-m-d H-i-s').".xlsx";
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');

			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}
		catch(Exception $e)
		{
			//alert the user.
			var_dump($e->getMessage());
		}
	}

}
