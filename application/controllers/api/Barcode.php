<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Barcode extends REST_Controller {

    public function __construct()
    {
        parent::__construct();

    }


    public function index_get()
    {
      //$this->response($this->db->query("SELECT * FROM questioner")->result());
      $data = array('status'=>'invalid');
      $this->response($data);
    }

    public function index_post()
    {
      $data = array('status'=>'invalid');
      $this->response($data);
    }


    public function check_get()
    {
      $data = array('status'=>'invalid');
      $this->response($data);
    }

    public function check_post()
    {
      if($this->post('bcode')){
        $query = $this->db->get_where('barcodes', array(
            'barcode' => $this->post('bcode'),
            //'isclaimed'=>0
        ));

        if($query->num_rows() === 0) {
          $data = array('status'=>'invalid', 'err'=>'Kode barcode yang anda gunakan tidak dapat digunakan untuk program ini.');
        } else {
          $isclaimed = $this->db->query("SELECT * FROM barcodes WHERE barcode='".$this->post('bcode')."'")->row()->isclaimed;
          if($isclaimed === '1') {
            $data = array('status'=>'invalid', 'err'=>'Kode barcode yang anda scan, sudah digunakan');
          } else {
            $product_code = substr($this->post('bcode'), 0, 5);
            $query = $this->db->query("SELECT q.question_id, p.product_code, q.batch, q.question FROM questioner AS q
                JOIN product AS p ON (q.product_code = p.id)
                WHERE p.product_code = '".$product_code."'
                ORDER BY q.date_created ASC");
            $questions = $query->result();
            foreach ($questions as $row) {
              $queryOpt = $this->db->query("SELECT * FROM question_option WHERE question_id=".$row->question_id);
              $rowOpt = $queryOpt->result_array();

              $row->options = $rowOpt;
            }
            $data = array('status'=>'success', 'barcode'=> $this->post('bcode'), 'product_code'=>$product_code, 'questions'=>$questions);  
          }
          
        }
        $this->response($data);
      } else {
        $data = array('status'=>'invalid', 'err'=>'Terjadi kesalahan, ulangi kembali atau gunakan barcode yang lain.');
        $this->response($data);
      }
    }




}
