<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Members extends REST_Controller {

    public function __construct()
    {
        parent::__construct();

    }


    public function index_get()
    {
      //$this->response($this->db->query("SELECT * FROM questioner")->result());
      $data = array('status'=>'invalid');
      $this->response($data);
    }

    public function index_post()
    {
      $data = array('status'=>'invalid');
      $this->response($data);
    }

    public function login_get()
    {
        $data = array('status'=>'invalid');
        $this->response($data);
    }

    public function login_post()
    {
      if($this->post('phone') && $this->post('password')){
        $this->db->select('*');
        $this->db->from('user_apps');
        $this->db->where('mobile_no', $this->post('phone'));
        $this->db->where('password', $this->post('password'));
        if($this->db->count_all_results() > 0) {
          $token = $this->db->get_where('user_apps', array('mobile_no' => $this->post('phone'), 'password'=>$this->post('password')))->row()->token;
          $isactivated = $this->db->get_where('user_apps', array('mobile_no' => $this->post('phone'), 'password'=>$this->post('password')))->row()->isactivated;
          $confirm_code = $this->db->get_where('user_apps', array('mobile_no' => $this->post('phone'), 'password'=>$this->post('password')))->row()->confirm_code;
          $data = array('status'=>'success', 'retdata'=> array('db_token'=>$token, 'phone'=>$this->post('phone'), 'isactivated'=>$isactivated, 'confirm_code'=>$confirm_code));
          $this->response($data);
        } else {
          $data = array('status'=>'invalid', 'err'=>'Nomor telepon atau password Anda salah, silahkan coba lagi.');
          $this->response($data);
        }
      } else {
        $data = array('status'=>'invalid');
        $this->response($data);
      }
    }

    public function changepass_post()
    {
      if($this->post('db_token') && $this->post('curr_password') && $this->post('new_password')){
        $this->db->select('*'); 
        $this->db->from('user_apps');
        $this->db->where('token', $this->post('db_token'));
        $this->db->where('password', $this->post('curr_password'));
        if($this->db->count_all_results() > 0) {

          $dataUpdatePass = array(
              'password' => $this->post('new_password')
          );
          $this->db->where('token', $this->post('db_token'));
          $this->db->where('password', $this->post('curr_password'));
          $this->db->update('user_apps', $dataUpdatePass);
          
          $data = array('status'=>'success', 'retdata'=>'Update password berhasil');
          $this->response($data);
        } else {
          $data = array('status'=>'invalid', 'err'=>'Kesalahan pada input password sebelumnya');
          $this->response($data);  
        }

      } else {
        $data = array('status'=>'invalid', 'err'=>'User not found');
        $this->response($data);
      } 
    }

    public function register_get()
    {
      $data = array('status'=>'invalid');
      $this->response($data);
    }

    public function register_post()
    {
      if($this->post('nama') && $this->post('phone') && $this->post('password')){

        $this->db->select('mobile_no');
        $this->db->from('user_apps');
        $this->db->where('mobile_no', $this->post('phone'));

        if($this->db->count_all_results() > 0) {
          $data = array('status'=>'invalid', 'err'=>'Nomor handphone sudah pernah didaftarkan, gunakan nomor lain untuk pendaftaran.');
        } else {
          $dataInsert = array(
            'user_name' => $this->post('nama'),
            'mobile_no' => $this->post('phone'),
            'password' => $this->post('password'),
            'location_lat' => $this->post('latitude'),
            'location_long' => $this->post('longitude'),
            'isactivated' => '1'
          );
          $this->db->insert('user_apps', $dataInsert);
          $last_id = $this->db->insert_id();
          $token = $this->db->get_where('user_apps', array('id' => $last_id))->row()->token;
          $confirm_code = $this->db->get_where('user_apps', array('id' => $last_id))->row()->confirm_code;

          $this->kirimSMS($this->post('phone'), 'Kode konfirmasi ULI Reward Anda adalah: '.$confirm_code);

          $data = array('status'=>'success', 'retdata'=>array(
            'name'=>$this->post('nama'),
            'phone'=>$this->post('phone'),
            'password'=>$this->post('password'),
            'location_lat' => $this->post('latitude'),
            'location_long' => $this->post('longitude'),
            'confirm_code'=>$confirm_code,
            'token'=>$token
          ));
        }
        $this->response($data);
      } else {
        $data = array('status'=>'invalid', 'err'=>'Terjadi kesalahan, silahkan ulangi pendaftaran.');
        $this->response($data);
      }
    }


    public function setnotifid_post()
    {
      if($this->post('db_token')){
        $cofirm_code = $this->db->get_where('user_apps', array('token' => $this->post('db_token')))->row()->cofirm_code;
        $phone = $this->db->get_where('user_apps', array('token' => $this->post('db_token')))->row()->mobile_no;
        //$this->sendMessage($this->post('db_token'), 'ULI Reward Activation Code', 'Activation code Anda adalah: '.$cofirm_code);

        $data = array('status'=>'success', 'confirm_code'=>$cofirm_code);
        $this->response($data);
      } else {
        $data = array('status'=>'invalid', 'err'=>'Register notif failed');
        $this->response($data);
      }
    }

    public function getreport_post()
    {
      if($this->post('db_token')){
        $retdata = $this->db->query('SELECT t.*, p.product_name, u.user_name, u.mobile_no FROM `transaction` as t
                      LEFT JOIN product as p ON p.product_code=t.product_code
                      LEFT JOIN user_apps as u ON u.id=t.user_id
                      WHERE t.user_id='.$this->getUserID($this->post('db_token')).' ORDER BY t.datecreated DESC');
        $data = array('status'=>'success', 'retdata'=>$retdata->result());
        $this->response($data);
      } else {
        $data = array('status'=>'invalid', 'err'=>'Register notif failed');
        $this->response($data);
      }
    }

    public function forgot_post()
    {
      if($this->post('phone')){
        $this->db->select('*');
        $this->db->from('user_apps');
        $this->db->where('mobile_no', $this->post('phone'));
        if($this->db->count_all_results() > 0) {

          $phone = $this->db->get_where('user_apps', array('mobile_no' => $this->post('phone')))->row()->mobile_no;
          $password = $this->db->get_where('user_apps', array('mobile_no' => $this->post('phone')))->row()->password;
          if($phone){
            $this->kirimSMS($phone, 'Password ULI Reward Anda adalah: '.$password);
            $data = array('status'=>'success', 'retdata'=>'Send Password Berhasil');
            $this->response($data);
          } else {
            $data = array('status'=>'invalid', 'err'=>'Nomor tidak terdaftar');
            $this->response($data);
          }
        } else {
          $data = array('status'=>'invalid', 'err'=>'Nomor tidak terdaftar');
          $this->response($data);
        }
        
      }
    }

    public function getpromo_post()
    {
      $retdata = $this->db->query('SELECT * FROM promo ORDER BY id DESC');
      $data = array('status'=>'success', 'retdata'=>$retdata->result());
      $this->response($data);
    }

    public function setactivate_post()
    {

    }

    public function sendconfirmcode_post()
    {
      if($this->post('db_token')){
        $phone = $this->db->get_where('user_apps', array('token' => $this->post('db_token')))->row()->mobile_no;
        $confirm_code = $this->db->get_where('user_apps', array('token' => $this->post('db_token')))->row()->confirm_code;
        $this->kirimSMS($phone, 'Kode konfirmasi ULI Reward Anda adalah: '.$confirm_code);
      }
    }

    public function saveanswers_post() //script baru
    {
        if($this->post('db_token')){

          $dataInsert = array(
            'user_id' => $this->getUserID($this->post('db_token')),
            'datecreated' => date("Y-m-d H:i:s"),
            'product_code' => $this->post('product_code'),
            'barcode' => $this->post('barcode'),
            'pulse' => 5000,
            'status' => 1
          );
          $this->db->insert('transaction', $dataInsert);
          $last_id = $this->db->insert_id();

          
          if($this->post('memberAnswers')){
            $arr = $this->post('memberAnswers');
          
            foreach($arr as $item) {
                $answer = explode("||", $item);
                $dataInsertAnswer = array(
                  'user_id' => $this->getUserID($this->post('db_token')),
                  'question_id' => $answer[0],
                  'option_id' => $answer[2],
                  'answer' => $answer[3],
                  'transaction_id' => $last_id
                );
                $this->db->insert('answer', $dataInsertAnswer);
            }
            
          }
          
          $dataUpdateBarcode = array(
             'isclaimed' => 1,
             'pulsa' => 5000,
             'claim_user_id' => $this->getUserID($this->post('db_token')),
             'claim_timestamp' => date("Y-m-d H:i:s")
          );
          $this->db->where('barcode', $this->post('barcode'));
          $this->db->update('barcodes', $dataUpdateBarcode);
          

          $phone = $this->db->get_where('user_apps', array('token' => $this->post('db_token')))->row()->mobile_no;

          $this->kirimPulsa($phone);

          $this->sendMessage($this->post('db_token'), 'Reward Pulsa Dari ULI Reward', 'Anda mendapatkan reward pulsa Rp. 5000 ke nomor '.$phone.', pulsa Anda akan otomatis bertambah dalam 1x24 jam');

          $data = array('status'=>'success', 'retdata'=>$dataInsert);
          $this->response($data);
        } else {
          $data = array('status'=>'invalid', 'err'=>'Register notif failed');
          $this->response($data);
        }
    }
    
    //                                                       script lama
    //public function saveanswers_post()  
    //{
    //    if($this->post('db_token')){

    //      $dataInsert = array(
    //        'user_id' => $this->getUserID($this->post('db_token')),
    //        'datecreated' => date("Y-m-d H:i:s"),
    //        'product_code' => $this->post('product_code'),
    //        'barcode' => $this->post('barcode'),
    //        'pulse' => 5000,
    //        'status' => 1
    //      );
    //      $this->db->insert('transaction', $dataInsert);

    //      $dataUpdateBarcode = array(
    //         'isclaimed' => 1,
    //         'pulsa' => 5000,
    //         'claim_user_id' => $this->getUserID($this->post('db_token')),
    //         'claim_timestamp' => date("Y-m-d H:i:s")
    //      );
    //      $this->db->where('barcode', $this->post('barcode'));
    //      $this->db->update('barcodes', $dataUpdateBarcode);

    //      $phone = $this->db->get_where('user_apps', array('token' => $this->post('db_token')))->row()->mobile_no;

    //      $this->kirimPulsa($phone);

    //      $this->sendMessage($this->post('db_token'), 'Reward Pulsa Dari ULI Reward', 'Anda mendapatkan reward pulsa Rp. 5000 ke nomor '.$phone.', pulsa Anda akan otomatis bertambah dalam 1x24 jam');

    //     $data = array('status'=>'success', 'retdata'=>$dataInsert);
    //      $this->response($data);
    //    } else {
    //      $data = array('status'=>'invalid', 'err'=>'Register notif failed');
    //      $this->response($data);
    //    }
    //}

    function getUserID($db_token){
      $userId = $this->db->get_where('user_apps', array('token' => $db_token))->row()->id;
      return $userId;
    }


    function kirimSMS($no_tujuan, $message){
      if($no_tujuan){
        /*
          ORIGINAL VALUE
          'registeredmsisdn'   => '6285697261314',
          'pin'                => '72643572',
        */
        $data = array(
          'message'            => $message,
          'targetmsisdn'       => $no_tujuan,
          'registeredmsisdn'   => '6281319262327',
          'pin'                => '72643577',
          'smsref'             => time()
        );

        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode( $data ),
            'header'=>  "Content-Type: application/json"
            )
        );

        $context  = stream_context_create( $options );
        $result = file_get_contents( 'http://119.235.253.14:9980/saipul-jaxrs-smsgw/rest/sms/sendSms', false, $context );
  		  $return = json_encode($result);
  		}
  	}

    function sendMessage($db_token, $title, $message){
      if($db_token){
  			$content = array(
  	      "en" => $message
  	      );

  	    $headings = array(
  	        "en" => $title
  	    );

  	    $tags = array(
  	      array("key" => "db_token", "relation" => "=", "value" => $db_token),
	      );

  	    $fields = array(
  	      'app_id' => "827c29da-2986-4c4e-8bd6-c5e706148a89",
  	      'included_segments' => array('All'),
  	      'contents' => $content,
  	      'headings' => $headings,
  	      'tags'=>$tags
  	    );

  			$fields = json_encode($fields);

  			$ch = curl_init();
  	    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
  	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
  	                           'Authorization: Basic ZTY1MjM5Y2QtMjEzYi00MWQ2LTgzMDUtMTQ1NGZiNDg2NGI5'));
  	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
  	    curl_setopt($ch, CURLOPT_POST, TRUE);
  	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

  	    $response = curl_exec($ch);
  	    curl_close($ch);

  		  //$response = sendMessage();
  		  $return["allresponses"] = $response;
  		  $return = json_encode( $return);
  		}
  	}

    function kirimPulsa($no_tujuan){
      /*
        ORIGINAL VALUE
        'registeredmsisdn'   => '6285697261314',
        'pin'                => '72643572',
      */
      
      if($no_tujuan){
        $thisCounter = $this->getCounterPulsa($no_tujuan);
        if($thisCounter >= 0){
          $data = array(
            'nominal'            => (int)5000,
            'targetmsisdn'       => (string)$no_tujuan,
            'registeredmsisdn'   => '6281319262327',
            'pin'                => '72643577',
            'trxref'             => time(),
            'counter'            => $thisCounter
          );

          $options = array(
            'http' => array(
              'method'  => 'POST',
              'content' => json_encode( $data ),
              'header'=>  "Content-Type: application/json"
              )
          );

          $context  = stream_context_create( $options );
          $result = file_get_contents( 'http://119.235.253.14:9980/saipul-jaxrs-gw/rest/topup/doTopUp', false, $context );  
        }
  		  //$return = json_encode($result);
  		}
  	}

    function getCounterPulsa($no_tujuan){
      $q = $this->db->query("SELECT COUNT(t.user_id) as transCount FROM transaction as t 
                    LEFT JOIN user_apps as u ON u.id = t.user_id
                    WHERE u.mobile_no = '". $no_tujuan ."' AND t.datecreated >= CURRENT_DATE")->row()->transCount;
      return (int)$q + 1;
    }

    public function topup_post()
    {

    }

}
