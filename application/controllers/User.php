<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		
		/* Load :: Common */
		$this->load->helper('number');
		$this->load->model('admin/GraphSummary_model');
		$this->load->model('admin/UserMut_model');
		$this->load->model('admin/UserLeader_model');
	}

	public function user_mut_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_user_mut'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data User MUT', 'user/user_mut_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

            // Get data for select2
			if ($this->ion_auth->is_adminbp())
                $this->data['areas'] = json_encode($this->GraphSummary_model->get_bp_area_graph());

            if ($this->ion_auth->is_adminao())
                $this->data['areas'] = json_encode($this->GraphSummary_model->get_ao_area_graph());

            if ($this->ion_auth->is_adminho())
                $this->data['areas'] = json_encode($this->GraphSummary_model->get_ho_area_graph());

            $this->data['regions'] 	 = json_encode($this->GraphSummary_model->region_name());
            $this->data['bps']       = json_encode($this->GraphSummary_model->bp_name());

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('user/user_mut_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('user/user_mut_list', $this->data);
		}
	}

	public function user_leader_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_leader'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data MUT Leader', 'user/user_leader_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

            // Get data for select2
			if ($this->ion_auth->is_adminbp())
                $this->data['areas'] = json_encode($this->GraphSummary_model->get_bp_area_graph());

            if ($this->ion_auth->is_adminao())
                $this->data['areas'] = json_encode($this->GraphSummary_model->get_ao_area_graph());

            if ($this->ion_auth->is_adminho())
                $this->data['areas'] = json_encode($this->GraphSummary_model->get_ho_area_graph());

            $this->data['regions'] 	 = json_encode($this->GraphSummary_model->region_name());
            $this->data['bps']       = json_encode($this->GraphSummary_model->bp_name());

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('user/user_leader_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('user/user_leader_list', $this->data);
		}
	}

	public function get_user_mut_list()
	{
		$filter = $this->set_user_mut_filter();

		$this->load->library('Datatable', array('model' => 'UserMut_model'));
		$mylib = new datatable(array('model' => 'UserMut_model', 'rowIdCol' => 'a.id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	public function get_user_leader_list()
	{
		$filter = $this->set_user_leader_filter();

		$this->load->library('Datatable', array('model' => 'UserLeader_model'));
		$mylib = new datatable(array('model' => 'UserLeader_model', 'rowIdCol' => 'a.id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	private function set_user_mut_filter()
	{
		$filter = array("equals" => array(), "contains" => array());
		if ($this->ion_auth->is_adminbp())
		{
			$filter['equals']['bp_code'] = substr($this->session->userdata('username'),3,3);
		}

		$id_area_awal = null;
		if ($this->ion_auth->is_adminao())
		{
			$username = $this->session->userdata('username');
			$id_area_awal = $this->GraphSummary_model->get_ao_area($username);
			foreach($id_area_awal as $key => $value) {
				$arr_area[$key] = $value['id_area'];
			}
			$id_area_awal = implode(",",(array)$arr_area);
		}

		// Array to string conversion for Area
		$idarea = $this->input->post("area");
		if ($this->input->post("area") != null || $this->input->post("area") != "")
		{
			foreach($this->input->post("area") as $key => $value) {
				$arr_area[$key] = intval($value);
			}
			$idarea = implode(",",(array)$arr_area);
		}

		$filter['where_in']['id_area'] = $idarea == "" ? $id_area_awal : $idarea;

		if ($this->input->post('bp') !== "" && $this->input->post('bp') !== null)
			$filter["where_in"]["id_bp"] = $this->input->post('bp');

		if ($this->input->post('area') !== "" && $this->input->post('area') !== null)
			$filter["where_in"]["id_area"] = $this->input->post('area');
		
		if ($this->input->post('region') !== "" && $this->input->post('region') !== null)
			$filter["where_in"]["c.id_region"] = $this->input->post('region');

		if ($this->input->post('mut_name') !== "" && $this->input->post('mut_name') !== null)
			$filter["contains"]["mut_name"] = $this->input->post('mut_name');
				
		return $filter;
	}

	private function set_user_leader_filter()
	{
		$filter = array("equals" => array(), "contains" => array());
		if ($this->ion_auth->is_adminbp())
		{
			$filter['equals']['bp_code'] = substr($this->session->userdata('username'),3,3);
		}

		$id_area_awal = null;
		if ($this->ion_auth->is_adminao())
		{
			$username = $this->session->userdata('username');
			$id_area_awal = $this->GraphSummary_model->get_ao_area($username);
			foreach($id_area_awal as $key => $value) {
				$arr_area[$key] = $value['id_area'];
			}
			$id_area_awal = implode(",",(array)$arr_area);
		}

		// Array to string conversion for Area
		$idarea = $this->input->post("area");
		if ($this->input->post("area") != null || $this->input->post("area") != "")
		{
			foreach($this->input->post("area") as $key => $value) {
				$arr_area[$key] = intval($value);
			}
			$idarea = implode(",",(array)$arr_area);
		}

		$filter['where_in']['id_area'] = $idarea == "" ? $id_area_awal : $idarea;

		if ($this->input->post('bp') !== "" && $this->input->post('bp') !== null)
			$filter["where_in"]["id_bp"] = $this->input->post('bp');

		if ($this->input->post('area') !== "" && $this->input->post('area') !== null)
			$filter["where_in"]["id_area"] = $this->input->post('area');
		
		if ($this->input->post('region') !== "" && $this->input->post('region') !== null)
			$filter["where_in"]["b.id_region"] = $this->input->post('region');

		if ($this->input->post('leader_name') !== "" && $this->input->post('leader_name') !== null)
			$filter["contains"]["leader_name"] = $this->input->post('leader_name');
		$filter['where_in']['id_mut_type'] = '0,1,2';
				
		return $filter;
	}

	public function user_leader_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("MUT Leader");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			$user_bp = substr($this->session->userdata('username'), 3, 3);
			$code_bp = $user_bp;
			$this->data['area_bp'] = $this->UserLeader_model->get_bp($code_bp);
            $this->data['tipe']    = $this->GraphSummary_model->get_mut_type_leader();

			/* Load Template */
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('user/user_leader_add', $this->data);
		}
	}

	public function post_leader_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->UserLeader_model->leader_save();
		  	redirect('user/user_leader_list');  
        }	
	}

	public function pilih_mut() {
		$user_bp = substr($this->session->userdata('username'), 3, 3);
		$total_user_mut = $this->UserMut_model->get_total_mut($this->uri->segment(3), $this->uri->segment(4), $user_bp);
		$total_current_user_mut = $this->UserMut_model->get_total_current_mut($total_user_mut[0]['id_'], $this->uri->segment(4));
		$user_mut = "Total MUT: ".$total_user_mut[0]['total_mut']." = Total Current MUT: ".$total_current_user_mut[0]['total_current_mut'];
		$a = $total_user_mut[0]['total_mut'];
		$b = $total_current_user_mut[0]['total_current_mut'];
		$cek = ($b < $a) ? 1 : 0;
		echo $user_mut."<input type = 'hidden' name='cek' value = ".$cek."><input type = 'hidden' name='mapping' value = ".$total_user_mut[0]['id_'].">";
	}
	
	public function user_mut_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("User MUT");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			$user_bp = substr($this->session->userdata('username'), 3, 3);
			$code_bp = $user_bp;
			$this->data['area_bp'] = $this->UserLeader_model->get_bp($code_bp);
			$this->data['mut_type'] = $this->UserMut_model->get_mut_type($code_bp);

			/* Load Template */
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('user/user_mut_add', $this->data);
		}
	}

	public function post_mut_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			$result = $this->UserMut_model->mut_save();
			if ($result) {
				redirect('user/user_mut_list');
			}
			else {
				/* Title Page */
				$this->page_title->push("User MUT");
				$this->data['pagetitle'] = $this->page_title->show();

				/* Breadcrumbs */
				$this->data['breadcrumb'] = $this->breadcrumbs->show();
				$this->data['message'] = "Alokasi MUT sudah sesuai dengan alokasi!";
				$this->template->adminbp_render('user/user_mut_alert', $this->data);
			}
        }	
	}

	public function user_mut_edit($id)
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Edit User MUT");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get user mut */
			$this->data['temp'] = $this->UserMut_model->get_mut_edit($id);

			/* Load Template */
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('user/user_mut_edit', $this->data);
		}
	}

	public function post_mut_edit(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->UserMut_model->mut_update();
		  	redirect('user/user_mut_list');  
        }		
	}

	public function usermut_delete($id)
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			$result = $this->UserMut_model->usermut_delete($id);

			$this->output->set_header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache");
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
	}

	public function user_leader_edit()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Edit Leader");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Model get category vendor */
			$this->data['temp'] = $this->UserLeader_model->get_leader_edit();
            $this->data['tipe'] = $this->GraphSummary_model->get_mut_type_leader();

			/* Load Template */
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('user/user_leader_edit', $this->data);
		}
	}

	public function post_leader_edit(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->UserLeader_model->leader_update();
		  	redirect('user/user_leader_list');  
        }		
	}

	public function userleader_delete($id)
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			$result = $this->UserLeader_model->leader_delete($id);

			$this->output->set_header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache");
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
	}

	public function export_excel_leader() {
		$currentURL = current_url();
		$paramVal2   = $this->input->get('region');
		$paramVal0   = $this->input->get('bp');
		$paramVal1   = $this->input->get('area');
		$paramVal3   = $this->input->get('leader_name');

		$filter = array();

		if ($this->ion_auth->is_adminbp())
		{
			$filter['equals']['bp_code'] = substr($this->session->userdata('username'),3,3);
		}

        if (!$this->ion_auth->is_adminbp()){
			if($paramVal0 != 'null')
	        {
	            $bp = explode(',', $paramVal0);
	            $filter['where_in']['id_bp'] = $bp;
	        }        	
        }

		$id_area_awal = null;
        if ($paramVal1 != 'null'){
            $areas = explode(',', $paramVal1);
            $filter['where_in']['id_area'] = $areas;			
        }

        if (!empty($paramVal2) || $paramVal2 != null)
            $filter['where_in']['id_region'] = $paramVal2;
        if (!empty($paramVal3) || $paramVal3 != null)
            $filter['contains']['leader_name'] = $paramVal3;

        $tipe = '0,1,2';
        $tp = explode(',', $tipe);
		$filter['where_in']['id_mut_type'] = $tp;

		$query = $this->UserLeader_model->get_export_data($filter);
		if(!$query)
            return false;
		
		try
		{
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("User Leader");

			$fields = $query->list_fields();
			$type = array();
			$col = 0;
			foreach ($fields as $field)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				switch ($field){
					default:
						array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
						break;
				}
				$col++;
			}

			$row = 2;
			foreach($query->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
					$col++;
				}
				$row++;
			}

			// column size
			foreach(range('A','Z') as $columnID) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}
					
			$filename = "User Leader ".date('Y-m-d H-i-s').".xlsx";
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');

			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}
		catch(Exception $e)
		{
			//alert the user.
			var_dump($e->getMessage());
		}
	}

	public function export_excel_user_mut() {
		$currentURL = current_url();
		$paramVal2   = $this->input->get('region');
		$paramVal0   = $this->input->get('bp');
		$paramVal1   = $this->input->get('area');
		$paramVal3   = $this->input->get('mut_name');

		$filter = array();

		if ($this->ion_auth->is_adminbp())
		{
			$filter['equals']['bp_code'] = substr($this->session->userdata('username'),3,3);
		}

        if($paramVal0 != 'null')
        {
            $bp = explode(',', $paramVal0);
            $filter['where_in']['id_bp'] = $bp;
        }

		$id_area_awal = null;
        if ($paramVal1 != 'null'){
            $areas = explode(',', $paramVal1);
            $filter['where_in']['id_area'] = $areas;			
        }else{
			if (!$this->ion_auth->is_adminho())
			{
				$username = $this->session->userdata('username');
				$id_area_awal = $this->GraphSummary_model->get_ao_area($username);
				foreach($id_area_awal as $key => $value) {
					$arr_area[$key] = $value['id_area'];
				}
				$filter["where_in"]["id_area"] = $arr_area;
			}
        }

        if (!empty($paramVal2) || $paramVal2 != null)
            $filter['where_in']['id_region'] = $paramVal2;
        if (!empty($paramVal3) || $paramVal3 != null)
            $filter['contains']['mut_name'] = $paramVal3;

		$query = $this->UserMut_model->get_export_data_user_mut($filter);
		if(!$query)
            return false;
        
        $this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("User MUT");

		$fields = $query->list_fields();
		$type = array();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			switch ($field){
				default:
					array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
					break;
			}
            $col++;
        }

		$row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
                $col++;
            }
            $row++;
        }

		// column size
		foreach(range('A','Z') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}
        
		$filename = "User MUT ".date('Y-m-d H-i-s').".xlsx";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
}
