<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business_partner extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		
		/* Load :: Common */
		$this->load->helper('number');
		$this->load->model('admin/Area_model');
		$this->load->model('admin/BusinessPartner_model');
		$this->load->model('admin/MappingPartner_model');
	}

	public function partner_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_partner'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data Business Partner', 'business_partner/partner_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('business_partner/partner_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('business_partner/partner_list', $this->data);
		}
	}

	public function mapping_partner_list()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_mapping'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, 'Master Data Mapping Business Partner', 'business_partner/mapping_partner_list');
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('business_partner/mapping_partner_list', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('business_partner/mapping_partner_list', $this->data);
		}
	}

	public function get_partner_list()
	{
		$filter = array("equals" => array(), "contains" => array());
		$filter = $this->set_partner_filter($filter);

		$this->load->library('Datatable', array('model' => 'BusinessPartner_model'));
		$mylib = new datatable(array('model' => 'BusinessPartner_model', 'rowIdCol' => 'id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	public function get_mapping_partner_list()
	{
		$filter = array("equals" => array(), "contains" => array());
		$filter = $this->set_mapping_partner_filter($filter);

		$this->load->library('Datatable', array('model' => 'MappingPartner_model'));
		$mylib = new datatable(array('model' => 'MappingPartner_model', 'rowIdCol' => 'a.id_', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	private function set_partner_filter($filter)
	{
		return $filter;
	}

	private function set_mapping_partner_filter($filter)
	{
		return $filter;
	}

	public function partner_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Add Partner");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			
			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('business_partner/partner_add', $this->data);
		}
	}

	public function partner_edit($id)
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Edit Partner");
			$this->data['pagetitle'] = $this->page_title->show();
			$this->data['id_partner'] = $id;

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Model get Mapping Partner */
			$this->data['partner'] = $this->BusinessPartner_model->get_partner($id);
			
			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('business_partner/partner_edit', $this->data);
				if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('business_partner/partner_edit', $this->data);
		}
	}

	public function post_partner_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			$modified_by = $this->session->userdata('username');
			$modified_date = date('Y-m-d H:i:s');

	      	$this->BusinessPartner_model->partner_save(0, $modified_by, $modified_date);
		  	redirect('business_partner/partner_list');  
        }		
	}

	public function post_partner_edit(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			$id_partner = $this->input->post('id_partner');
			$modified_by = $this->session->userdata('username');
			$modified_date = date('Y-m-d H:i:s');
			
			$result = $this->BusinessPartner_model->partner_save($id_partner, $modified_by, $modified_date);
			if ($result)
			{
				redirect('business_partner/partner_list');	
			}  
		  	redirect('business_partner/partner_edit');
        }		
	}

	public function mapping_partner_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Mapping Partner");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Model get Region */
			$this->data['temp'] = $this->Area_model->get_region_area();

			/* Model get Business Partner */
			$this->data['temp_bp'] = $this->BusinessPartner_model->get_business_partner();
			
			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('business_partner/mapping_partner_add', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('business_partner/mapping_partner_add', $this->data);
		}
	}

	public function post_mapping_partner_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->BusinessPartner_model->mapping_partner_save();
		  	redirect('business_partner/mapping_partner_list');  
        }		
	}

	public function mapping_partner_edit($id)
	{
		if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Mapping Partner");
			$this->data['pagetitle'] = $this->page_title->show();
			$this->data['id_mapping_partner'] = $id;

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Model get Mapping Partner */
			$this->data['mapping_partner'] = $this->MappingPartner_model->get_mapping_partner($id);

			/* Model get Region */
			$this->data['temp'] = $this->Area_model->get_region_area();

			/* Model get Area */
			$this->data['temp_area'] = $this->Area_model->get_area($this->data['mapping_partner'][0]['id_region']);

			/* Model get Business Partner */
			$this->data['temp_bp'] = $this->BusinessPartner_model->get_business_partner();
			
			/* Load Template */
			if ($this->ion_auth->is_adminho())
				$this->template->adminho_render('business_partner/mapping_partner_edit', $this->data);
			if ($this->ion_auth->is_adminbp())
				$this->template->adminbp_render('business_partner/mapping_partner_edit', $this->data);
		}
	}

	public function post_mapping_partner_edit()
	{
		if (!$this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			$id_mapping_partner = $this->input->post('id_mapping_partner');
			$modified_by = $this->session->userdata('username');
			$modified_date = date('Y-m-d H:i:s');

	      	$result = $this->MappingPartner_model->mapping_partner_save($id_mapping_partner, $modified_by, $modified_date);
			if ($result) {
				redirect('business_partner/mapping_partner_list');
			}
			redirect('business_partner/mapping_partner_edit');
        }
	}
}
