<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Answer extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push('Answers');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Manage Answer', 'admin/answer/addanswer');
        $this->load->library('grocery_CRUD');
    }


	public function addanswer($id=null)
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            if(!$id){
                redirect('admin/questionnaire', 'refresh');
            }
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */
            $this->grocery_crud->unset_jquery();
            $this->grocery_crud->set_table('answer');
            $this->grocery_crud->set_theme('datatables');
                /*
            $this->grocery_crud->add_action('More', '', 'admin/books/add_pages','ui-icon-plus');
            $this->grocery_crud->set_field_upload('book_cover','assets/uploads/books');
            $this->grocery_crud->columns('book_title','book_author','book_pagenum','book_cover');
            $this->grocery_crud->fields('book_title','book_author','book_pagenum','book_cover');
            */

            $this->data['output'] = $this->grocery_crud->render();
            //$this->data['list_tables'] = '';

            /* Load Template */
            $this->template->admin_render('admin/blanks/index', $this->data);
        }
	}

}
