<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business_partner extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->dbutil();
        $this->lang->load('admin/database');

        /* Title Page :: Common */
        $this->page_title->push('Business Partner');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Masterdata Business Partner', 'admin/business_partner');
        $this->load->library('grocery_CRUD');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */  
            $this->grocery_crud->unset_jquery();
            $this->grocery_crud->unset_print();
            $this->grocery_crud->set_table('tbl_business_partner');
            $this->grocery_crud->set_theme('datatables');

            $cari_bp = $_SESSION['identity'];
            $admin = substr($cari_bp,0,3);
			$admin_ao = substr($cari_bp,0,5);
			$id_bp = substr($cari_bp,5,3);
			$query_area = $this->db->query("SELECT tbl_area.id_ FROM tbl_area INNER JOIN m_city ON tbl_area.id_city = m_city.id_ AND m_city.code_city = '$id_bp'");
			foreach ($query_area->result_array() as $row) {
					$id_area = $row['id_'];
			}

            if($admin == 'BPR'){
                $id_bp = substr($cari_bp,5,3);
                $this->grocery_crud->where('bp_code',$id_bp);
				$this->grocery_crud->unset_add();
            }else if($admin_ao == 'ULI01'){
				$this->grocery_crud->unset_add();
				$this->grocery_crud->unset_edit();
				$this->grocery_crud->unset_delete();
            }



            $this->grocery_crud->columns('bp_name');
            $this->grocery_crud->fields('bp_name');

            $this->grocery_crud->display_as('bp_name','Business Partner Name');
            
            $this->grocery_crud->unset_add();
            $this->grocery_crud->unset_edit();
            $this->grocery_crud->unset_delete();

            $this->data['output'] = $this->grocery_crud->render();
            //$this->data['list_tables'] = '';

            /* Load Template */
            $this->template->admin_render('admin/blanks/index', $this->data);
        }
	}
}
