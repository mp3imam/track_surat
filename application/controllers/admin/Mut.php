<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mut extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->dbutil();
        $this->lang->load('admin/database');

        /* Title Page :: Common */
        $this->page_title->push('MUT');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Masterdata MUT', 'admin/mut');
        $this->load->library('grocery_CRUD');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */  
            $this->grocery_crud->unset_jquery();
            $this->grocery_crud->unset_print();     /* GROCERY CRUD PRINT OFF */ 
            $this->grocery_crud->set_table('tbl_mut');
            $this->grocery_crud->set_theme('datatables');
            $this->grocery_crud->columns('id_','id_ss','name_mut','code_mut','password','telp_mut','email_mut','type_mut','level_mut');
            //$this->grocery_crud->display_as('id_','No');
            //$this->grocery_crud->display_as('region_name','Region Name');
            

            /*
            $this->grocery_crud->add_action('More', '', 'admin/books/add_pages','ui-icon-plus');
            $this->grocery_crud->set_field_upload('book_cover','assets/uploads/books');
            $this->grocery_crud->columns('book_title','book_author','book_pagenum','book_cover');
            $this->grocery_crud->fields('book_title','book_author','book_pagenum','book_cover');
            */
            $this->grocery_crud->unset_add();
            $this->grocery_crud->unset_edit();
            $this->grocery_crud->unset_delete();

            $this->data['output'] = $this->grocery_crud->render();
            //$this->data['list_tables'] = '';

            /* Load Template */
            $this->template->admin_render('admin/blanks/index', $this->data);
        }
	}
}
