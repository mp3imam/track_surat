<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/Dashboard_model');
	}
	
	public function index()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push(lang('menu_dashboard'));
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();


			/* Load Template */
			if ($this->ion_auth->is_adminus())
				$this->template->adminus_render('dashboard/graph', $this->data);
			if ($this->ion_auth->is_adminpe())
				$this->template->adminpe_render('dashboard/index', $this->data);
		}
	}


}
