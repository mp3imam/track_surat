<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->dbutil();
        $this->lang->load('admin/database');
        
        /* Title Page :: Common */
        $this->page_title->push('Store');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'List Store', 'admin/admin_au/store');
    }


    public function index()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $cari_bp = $_SESSION['identity'];
            $bp_id = substr($cari_bp,3,2);
            $id_bp = ltrim($bp_id,'0');
            $cek_admin =  substr($cari_bp,0,3);
            $bp_id ="";
            
            if($cek_admin != "ULI"){
                    $bp_id = "id_bp = ".$id_bp; 
                }

            $connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");
            
            $this->load->library('pagination');
            $this->load->library('table');
            $total_records = $this->uri->segment(4);
            //echo $total_records; die();
            if($total_records == ""){                
                    //echo $query = " 
                    $query = mysqli_query($connection,"
                            SELECT 
                                t3.*
                            FROM
                                (SELECT 
                                    t2.*, tbl_qrcode.id_qrcode 'QrCode'
                                FROM
                                    (SELECT 
                                    s.id_,
                                        s.store_name 'Store Name',
                                        s.estimation_time 'Estimation Time',
                                        s.pasar_residential 'Pasar Residential',
                                        s.alamat 'Alamat',
                                        s.golongan 'Golongan',
                                        c.cluster_name 'Cluster Name',
                                        s.code_outlet 'Code Outlet',
                                        IF(s.status_store = '1', 'Register', 'Not Register') 'Status Store',
                                        d.id_area,
                                        d.area_name 'Area Name',
                                        d.id_bp,
                                        d.id_ 'Id Distributor',
                                        d.bp_name 'Bussiness Partner',
                                        d.dist_name 'Distributor Name',
                                        ts.type_store_name 'Type Store Name'
                                FROM
                                    tbl_store s
                                LEFT JOIN tbl_cluster c ON c.id_ = s.id_cluster
                                LEFT JOIN tbl_type_store ts ON ts.id_ = s.id_type_store
                                LEFT JOIN (SELECT 
                                    tbl_distributor.*,
                                        tbl_area.area_name,
                                        tbl_business_partner.bp_name
                                FROM
                                    tbl_distributor
                                LEFT JOIN tbl_area ON tbl_area.id_ = tbl_distributor.id_area
                                LEFT JOIN tbl_business_partner ON tbl_business_partner.id_ = tbl_distributor.id_bp) d ON s.id_dist = d.id_) t2
                                LEFT JOIN (SELECT 
                                    *
                                FROM
                                    tbl_qrcode
                                WHERE
                                    id_store IS NOT NULL AND id_store != 0
                                GROUP BY id_store) tbl_qrcode ON tbl_qrcode.id_store = t2.id_) t3
                            WHERE
                            $bp_id
                    "); //die();
                $total_records = mysqli_num_rows($query);
            }

            $config['base_url'] = site_url('admin/store/index/'.$total_records.'/');
            if($this->uri->segment(3) =='index'){$config['total_rows'] = $total_records;}else{$config['total_rows'] = 90;}

            $config['per_page'] = 20;
            $config['num_links'] = 4;
            
            $config['uri_segment'] = 5; //3 merupakan posisi pagination dalam url
            $config['first_tag_open'] = '<div id="pagination">';
            $config['cur_tag_open'] = '<b>';
            $config['cur_tag_close'] = '</b>';
            $config['first_tag_close'] = '</div>';

            $this->pagination->initialize($config);
            
            $off = $this->uri->segment(5);
            if($off == 0 || $off==""){$offset=0;}else{$offset = $off;}

            $this->load->model('Model_store');
            $this->data['records']          = $this->Model_store->show_store_reg($config['per_page'], $offset);  
            $this->data['search_text']      = $search_text;
            $this->data['search_field']     = $search_field;
            $this->data['total_records']    = $total_records;
            $this->data['breadcrumb']       = $this->breadcrumbs->show();
            $this->template->admin_render('admin/blanks/index_store',$this->data);
        }
    }

    public function cari_store()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {redirect('auth/login', 'refresh');}
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $cari_bp = $_SESSION['identity'];
            $bp_id = substr($cari_bp,3,2);
            $id_bp = ltrim($bp_id,'0');
            $cek_admin =  substr($cari_bp,0,3);
            $bp_id ="";
            
            if($cek_admin != "ULI"){
                    $bp_id = "id_bp = ".$id_bp; 
                }

            $search_text  = $this->input->post('search_text');
            $search_field  = $this->input->post('search_field');
            if ($search_text  == '') {$search_text  = $this->uri->segment(5);}
            if ($search_field == '') {$search_field = $this->uri->segment(4);}
            $search_text = str_replace('%20',' ', $search_text); 
            if($search_field == 'id_qrcode'){
                $search_qrcode = "AND QrCode = '$search_text'";
            }else{
                if ($search_field == 's.id_'){
                    $search_all = "AND $search_field = '$search_text'";
                }else{
                    $search_all = "AND $search_field LIKE '%$search_text%'";
                    $search_qrcode = "";
                }
            }
            $data_cari  = $search_text;
            $data_kolom = $search_field;

            $connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");
            
            $this->load->library('pagination');
            $this->load->library('table');
            $query = mysqli_query($connection,"
                    SELECT 
                        t3.*
                    FROM
                        (SELECT 
                            t2.*, tbl_qrcode.id_qrcode 'QrCode'
                        FROM
                            (SELECT 
                            s.id_,
                                s.store_name 'Store Name',
                                s.estimation_time 'Estimation Time',
                                s.pasar_residential 'Pasar Residential',
                                s.alamat 'Alamat',
                                s.golongan 'Golongan',
                                c.cluster_name 'Cluster Name',
                                s.code_outlet 'Code Outlet',
                                IF(s.status_store = '1', 'Register', 'Not Register') 'Status Store',
                                d.id_area,
                                d.area_name 'Area Name',
                                d.id_bp,
                                d.id_ 'Id Distributor',
                                d.bp_name 'Bussiness Partner',
                                d.dist_name 'Distributor Name',
                                ts.type_store_name 'Type Store Name'
                        FROM
                            tbl_store s
                        LEFT JOIN tbl_cluster c ON c.id_ = s.id_cluster
                        LEFT JOIN tbl_type_store ts ON ts.id_ = s.id_type_store
                        LEFT JOIN (SELECT 
                            tbl_distributor.*,
                                tbl_area.area_name,
                                tbl_business_partner.bp_name
                        FROM
                            tbl_distributor
                        LEFT JOIN tbl_area ON tbl_area.id_ = tbl_distributor.id_area
                        LEFT JOIN tbl_business_partner ON tbl_business_partner.id_ = tbl_distributor.id_bp) d ON s.id_dist = d.id_ $search_all) t2
                        LEFT JOIN (SELECT 
                            *
                        FROM
                            tbl_qrcode
                        WHERE
                            id_store IS NOT NULL AND id_store != 0
                        GROUP BY id_store) tbl_qrcode ON tbl_qrcode.id_store = t2.id_) t3
                    WHERE
                    $bp_id $search_qrcode
            ");
            $total_records = mysqli_num_rows($query);

            $config['base_url'] = site_url('admin/store/cari_store/'.$search_field.'/'.$search_text.'/');

            $config['total_rows'] = $total_records;

            $config['per_page'] = 20;
            $config['num_links'] = 5;
            
            $config['uri_segment'] = 6; //3 merupakan posisi pagination dalam url
            $config['first_tag_open'] = '<div id="pagination">';
            $config['cur_tag_open'] = '<b>';
            $config['cur_tag_close'] = '</b>';
            $config['first_tag_close'] = '</div>';

            $this->pagination->initialize($config);
            
            $off = $this->uri->segment(6);
            if($off == 0 || $off==""){
               $offset=0;
            }
            else{
               $offset = $off;
            }
            
            $this->load->model('Model_store');
            $this->load->model('Model_area');
            
            $this->data['records']          = $this->Model_store->show_store_reg_cari($config['per_page'],  $offset,$search_field,$search_text);
            $this->data['records_history']  = $this->Model_area->history_qrcode($id_store);
            $this->data['search_text']      = $search_text;
            $this->data['search_field']     = $search_field;
            $this->data['total_records']    = $total_records;
            $this->data['breadcrumb']       = $this->breadcrumbs->show();
            $this->template->admin_render('admin/blanks/index_store',$this->data);
        }
    }

    function edit_store() {
        $this->load->model('Model_area');
        
        if(isset($_POST['submit'])) {
            $this->Model_area->update_store();
            redirect('admin/store');
        } 
        else
        {
            $this->data['breadcrumb']= $this->breadcrumbs->show();
            $this->data['id_store']  = $this->uri->segment(4);
            $this->data['output']    = $this->uri->segment(4);
            $this->data['tampung']   = $this->Model_area->tampung_store($this->uri->segment(4));
            $this->data['region']    = $this->Model_area->ambil_region();
            $this->data['cluster']   = $this->Model_area->ambil_cluster();
            $this->data['type_store']   = $this->Model_area->ambil_type_store();
            $this->template->admin_render('admin/blanks/edit_store',$this->data);
        }
    }

    function hapus_store($id_store) {
        $this->load->model('Model_assignment');
        $this->load->model('Model_area');
        
        $id_store = $this->uri->segment(4);
        $query_assignment = $this->db->query("SELECT a.id_,s.store_name FROM tbl_assignment a INNER JOIN tbl_store s ON a.id_store = s.id_ AND s.id_ = '$id_store'");
        foreach ($query_assignment->result_array() as $row) {
                $id_assignment = $row['id_'];
                $store_name = $row['store_name'];
        }

        if($query_assignment->num_rows()>0){
            $data['message'] = $this->session->set_flashdata('message1','Sorry, You cant delete Store Name ');
            $data['message'] = $this->session->set_flashdata('id_store',$id_store);
            $data['message'] = $this->session->set_flashdata('store_name',$store_name);
            $data['message'] = $this->session->set_flashdata('message2',' Please Delete your Assignment with ID ');
            $data['message'] = $this->session->set_flashdata('id_assignment',$id_assignment);

            $this->data['breadcrumb']   = $this->breadcrumbs->show();
            $this->data['output']       = $this->uri->segment(4);
            redirect('admin/Assignment/cari_assignment/ass.id_/'.$id_assignment);
        } 
        else{
            $this->db->where('id_',$id_store);
            $this->db->delete('tbl_store'); 
            redirect('admin/store');
        }
    }

    function delete_store_assigment($id_assignment,$id_store) {
        $this->load->model('Model_assignment');
        
        $id_store       = $this->uri->segment(4);
        $id_assignment  = $this->uri->segment(5);

        // Hapus Assigment
        $this->db->where('id_',$id_assignment);
        $this->db->delete('tbl_assignment'); 

        // Hapus Store
        $this->db->where('id_',$id_store);
        $this->db->delete('tbl_store'); 
        redirect('admin/store');
    }

    function reset_qrcode() {
        $this->load->model('Model_area');
        $this->Model_area->reset_qrcode();
        redirect('admin/store');
    }

    public function tampil_history() {
        $this->load->model('Model_area');
        $id_store = $this->uri->segment(4);
        $this->data['records']=$this->Model_area->history_qrcode($id_store);
        $this->load->view('admin/blanks/hist_qrcode',$this->data);
    }

    public function tampil_city() {
        $this->load->model('Model_area');

        $this->data['area']=$this->Model_area->ambil_region($this->uri->segment(4));
        $this->load->view('admin/blanks/v_drop_down_area',$this->data);
    }

    public function add_store(){
        $this->load->model('Model_area');

        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        $this->data['output']   = $this->uri->segment(4);
        $this->data['region']   = $this->Model_area->ambil_region();
        $this->data['cluster']   = $this->Model_area->ambil_cluster();
        $this->data['type_store']   = $this->Model_area->ambil_type_store();
        $this->template->admin_render('admin/blanks/add_store',$this->data);   
    }

    public function pilih_area() {
        $this->load->model('Model_area');

        $this->data['area']=$this->Model_area->ambil_area($this->uri->segment(4));
        $this->load->view('admin/blanks/v_drop_down_area',$this->data);
    }

    public function pilih_code_area() {
        $this->load->model('Model_area');
//        echo $this->uri->segment(4); die();
        $this->data['s_area']=$this->Model_area->ambil_code_area($this->uri->segment(4));
        $this->load->view('admin/blanks/v_drop_down_code_area',$this->data);
    }

    public function pilih_bp() {
        $this->load->model('Model_area');

        $this->data['area']=$this->Model_area->ambil_bp($this->uri->segment(4));
        $this->load->view('admin/blanks/v_drop_down_area',$this->data);
    }

    public function pilih_dist() {
        $this->load->model('Model_area');

        $this->data['area']=$this->Model_area->ambil_dist($this->uri->segment(4),$this->uri->segment(5));
        $this->load->view('admin/blanks/v_drop_down_area',$this->data);
    }

    function simpan_store() {
        $this->load->model('Model_area');

        if(isset($_POST['submit'])) {
            $this->Model_area->add_store();
            redirect('admin/store');          
        } 
        else
        {
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['id_store'] = $this->uri->segment(4);
            $this->data['output']   = $this->uri->segment(4);
            $this->data['region']   = $this->Model_area->ambil_region();
            $this->data['cluster']   = $this->Model_area->ambil_cluster();
            $this->data['type_store']   = $this->Model_area->ambil_type_store();
            $this->template->admin_render('admin/blanks/add_store',$this->data);
        }
    } 
    

    function download_store_cari() {
        $tanggal = date('d-M-Y his');
        header("Content-type=appalication/vnd.ms-excell");
        header("content-disposition:attachment;filename=Store".$tanggal.".xls");
        
            $cari_bp = $_SESSION['identity'];
            $bp_id = substr($cari_bp,3,2);
            $id_bp = ltrim($bp_id,'0');
            $cek_admin =  substr($cari_bp,0,3);
            $bp_id ="";
            if($cek_admin != "ULI"){$bp_id = "id_bp = ".$id_bp;}

            $search_text  = $this->input->post('search_text');
            $search_field  = $this->input->post('search_field');
            if ($search_text  == '') {$search_text  = $this->uri->segment(5);}
            if ($search_field == '') {$search_field = $this->uri->segment(4);}
            $search_text = str_replace('%20',' ', $search_text); 
            if($search_field == 'id_qrcode'){
                $search_qrcode = "AND QrCode = '$search_text'";
            }else{
                if ($search_field == 's.id_'){
                    $search_all = "AND $search_field = '$search_text'";
                }else{
                    $search_all = "AND $search_field LIKE '%$search_text%'";
                    $search_qrcode = "";
                }
            }
            $data_cari  = $search_text;
            $data_kolom = $search_field;
        
            $connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");
            

        echo 
        'Id STORE'."\t"
        .'Area Name'."\t"
        .'Business Partner'."\t"
        .'ID Distributor'."\t"
        .'Cluster'."\t"
        .'Type Store'."\t"
        .'Store Name'."\t"
        .'Outlet Code'."\t"
        .'Estimation Time'."\t"
        .'Pasar Residential'."\t"
        .'Alamat'."\t"
        .'Golongan'."\t"
        .'QR CODE'."\t"
        .'Status Store'."\n";

        $connection = mysqli_connect("192.168.179.168", "root", "JangkrikBos#$", "new_mut_db");
        //$data = mysqli_query($connection,"
        echo $data = mysqli_query($connection,"
                    SELECT 
                        t3.*
                    FROM
                        (SELECT 
                            t2.*, tbl_qrcode.id_qrcode 'QrCode'
                        FROM
                            (SELECT 
                            s.id_,
                                s.store_name 'Store Name',
                                s.estimation_time 'Estimation Time',
                                s.pasar_residential 'Pasar Residential',
                                s.alamat 'Alamat',
                                s.golongan 'Golongan',
                                c.cluster_name 'Cluster Name',
                                s.code_outlet 'Code Outlet',
                                IF(s.status_store = '1', 'Register', 'Not Register') 'Status Store',
                                d.id_area,
                                d.area_name 'Area Name',
                                d.id_bp,
                                d.id_ 'Id Distributor',
                                d.bp_name 'Bussiness Partner',
                                d.dist_name 'Distributor Name',
                                ts.type_store_name 'Type Store Name'
                        FROM
                            tbl_store s
                        LEFT JOIN tbl_cluster c ON c.id_ = s.id_cluster
                        LEFT JOIN tbl_type_store ts ON ts.id_ = s.id_type_store
                        LEFT JOIN (SELECT 
                            tbl_distributor.*,
                                tbl_area.area_name,
                                tbl_business_partner.bp_name
                        FROM
                            tbl_distributor
                        LEFT JOIN tbl_area ON tbl_area.id_ = tbl_distributor.id_area
                        LEFT JOIN tbl_business_partner ON tbl_business_partner.id_ = tbl_distributor.id_bp) d ON s.id_dist = d.id_ $search_all) t2
                        LEFT JOIN (SELECT 
                            *
                        FROM
                            tbl_qrcode
                        WHERE
                            id_store IS NOT NULL AND id_store != 0
                        GROUP BY id_store) tbl_qrcode ON tbl_qrcode.id_store = t2.id_) t3
                    WHERE
                    $bp_id $search_qrcode
       "); //die();

        while($store = mysqli_fetch_assoc($data)){
            echo $store['id_']."\t"
            .$store['Area Name']."\t"
            .$store['Bussiness Partner']."\t"
            .$store['Id Distributor']."\t"
            .$store['Cluster Name']."\t"
            .$store['Type Store Name']."\t"
            .$store['Store Name']."\t'"
            .$store['Code Outlet']."\t"
            .$store['Estimation Time']."\t"
            .$store['Pasar Residential']."\t"
            .$store['Alamat']."\t"
            .$store['Golongan']."\t"
            .$store['QrCode']."\t"
            .$store['Status Store']."\n";
            }
    }

}