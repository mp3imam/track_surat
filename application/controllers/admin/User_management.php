<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_management extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push('User Management');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'User Management', 'admin/admin_au/user_managemet');
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        
        $this->lang->load('auth');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }

            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            /* Data */  
            //$this->grocery_crud->unset_jquery();
            //$this->grocery_crud->unset_print();     /* GROCERY CRUD PRINT OFF */ 
            //$this->grocery_crud->set_table('users');
            //$this->grocery_crud->set_theme('datatables');
            //$this->grocery_crud->columns('id_','region_name');
            //$this->grocery_crud->display_as('id_','No');
            //$this->grocery_crud->display_as('region_name','Region Name');
            

            /*
            $this->grocery_crud->add_action('More', '', 'admin/books/add_pages','ui-icon-plus');
            $this->grocery_crud->set_field_upload('book_cover','assets/uploads/books');
            $this->grocery_crud->columns('book_title','book_author','book_pagenum','book_cover');
            $this->grocery_crud->fields('book_title','book_author','book_pagenum','book_cover');
            */

            //$this->data['output'] = $this->grocery_crud->render();
            //$this->data['list_tables'] = '';

            /* Load Template */
            $this->template->admin_render('admin/blanks/index_user', $this->data);
        }
	}
}
