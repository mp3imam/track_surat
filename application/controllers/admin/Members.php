<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->dbutil();
        $this->lang->load('admin/database');

        /* Title Page :: Common */
        $this->page_title->push('Members');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Manage Members', 'admin/members');
        $this->load->library('grocery_CRUD');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */
            $this->grocery_crud->unset_jquery();    /* GROCERY CRUD Unset JQUERY */ 
            $this->grocery_crud->unset_print();     /* GROCERY CRUD PRINT OFF */         
            $this->grocery_crud->set_table('user_apps');
            $this->grocery_crud->set_theme('datatables');
            $this->grocery_crud->columns('user_name','mobile_no', 'Lokasi', 'token','confirm_code', 'isactivated');
            $this->grocery_crud->fields('user_name','mobile_no','location_long', 'location_lat', 'token','confirm_code', 'isactivated');
            $this->grocery_crud->display_as('user_name','User Name');
            $this->grocery_crud->display_as('mobile_no','Mobile Number');
            $this->grocery_crud->display_as('location_long','Location Longitude');
            $this->grocery_crud->display_as('location_lat','Location Latitude');
            $this->grocery_crud->display_as('token','Token');
            $this->grocery_crud->display_as('confirm_code','Confirm Code');
            $this->grocery_crud->display_as('isactivated','Is Activated');
            $this->grocery_crud->callback_column('Lokasi',array($this,'_callback_location'));
            /*
            $this->grocery_crud->add_action('More', '', 'admin/books/add_pages','ui-icon-plus');
            $this->grocery_crud->set_field_upload('book_cover','assets/uploads/books');
            $this->grocery_crud->columns('book_title','book_author','book_pagenum','book_cover');
            $this->grocery_crud->fields('book_title','book_author','book_pagenum','book_cover');
            */

            $this->data['output'] = $this->grocery_crud->render();
            //$this->data['list_tables'] = '';

            /* Load Template */
            $this->template->admin_render('admin/blanks/index', $this->data);
        }
	}

    public function _callback_location($value, $row)
    {
        $retVal = $this->getLocation($row->location_lat, $row->location_long);
        if($retVal){
            return $retVal[0]->formatted_address;
        } else {
            return '&nbsp;';
        }
        
    }

    function getLocation($lat=null, $long=null){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&sensor=true');
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        if($obj->results){
            return $obj->results;
        } else {
            return false;
        }
    }

  
}
