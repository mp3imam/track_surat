<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Surat extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
		
		/* Load :: Common */
		$this->load->helper('number');
		$this->load->model('admin/Surat_model');
		$this->load->model('admin/Maker_model');
		$this->load->model('ion_auth_model');
	}

	public function index()
	{
		/* Title Page */
		$this->page_title->push(lang('menu_surat'));
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs */
		$this->breadcrumbs->unshift(1, 'Master Arsip Surat', 'menu_surat');
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		$this->data['makers'] = $this->Surat_model->makers();
		$this->data['makers'] = json_encode($this->data['makers']);

		/* Load Template */
		if ($this->ion_auth->is_adminsu())
			$this->template->adminsu_render('admin_surat/surat_list', $this->data);
		if ($this->ion_auth->is_adminkb())
			$this->template->adminkb_render('Admin_Surat/surat_list', $this->data);
		if ($this->ion_auth->is_adminsg())
			$this->template->adminsg_render('Admin_Surat/surat_list', $this->data);
		if ($this->ion_auth->is_admintl())
			$this->template->admintl_render('Admin_Surat/surat_list', $this->data);
	}

	public function get_surat_list()
	{
		$filter = $this->set_surat_filter();

		$this->load->library('Datatable', array('model' => 'Surat_model'));
		$mylib = new datatable(array('model' => 'Surat_model', 'rowIdCol' => 'a.id', 'filter' => $filter));
		$jsonArray = $mylib->datatableJson(FALSE, TRUE);

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}

	public function get_disposisi($id)
	{
		$get_surat = $this->Surat_model->surat($id);
		$get_maker = $this->Surat_model->get_makers();
		$jsonArray = array('get_surat'=>0, 'get_maker'=>0);
		if ($get_surat != null && $get_maker != null)
		{
			$jsonArray = array('get_surat'=>$get_surat
								, 'get_maker'=>$get_maker);
		}

		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache");
		$this->output->set_content_type('application/json')->set_output(json_encode($jsonArray));
	}


	private function set_surat_filter()
	{
		$filter = array("equals" => array(), "contains" => array());

		return $filter;
	}

	public function surat_add()
	{
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
			/* Title Page */
			$this->page_title->push("Surat Add");
			$this->data['pagetitle'] = $this->page_title->show();

			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Load Template */
			if ($this->ion_auth->is_adminsu())
				$this->template->adminsu_render('Admin_Surat/Surat_add', $this->data);
		}
	}
	
	public function post_surat_add(){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Surat_model->surat_save();
		  	redirect('Admin_Surat');
        }		
	}

	public function post_disposisi_mk($id_surat,$id_mk){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Surat_model->disposisi_save($id_surat,$id_mk);
		  	redirect('Admin_Surat');
        }		
	}

	public function post_cheker_approve($id_surat,$id_sg){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Surat_model->cheker_approve($id_surat,$id_sg);
		  	redirect('Admin_Surat');
        }		
	}

	public function post_reject($id_surat,$id_reject,$alasan){
		if ( ! $this->ion_auth->logged_in() )
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
	      	$this->Surat_model->post_reject($id_surat,$id_reject
	      		,$alasan);
		  	redirect('Admin_Surat');
        }		
	}

	public function surat_reject()
	{
      	$this->Surat_model->surat_reject();
	  	redirect('surat');  
	}

	public function surat_edit($id)
	{
		/* Title Page */
		$this->page_title->push("Edit surat MUT");
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs */
		$this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		/* Model get Editnya */
		$this->data['id_edit'] = $this->Surat_model->get_edit($id);

		/* Load Template */
		if ($this->ion_auth->is_adminsu())
			$this->template->adminsu_render('admin_surat/surat_edit', $this->data);
	}

	public function post_edit($id_)
	{
      	$this->Surat_model->update_login($id_);
	  	redirect('surat');  
	}

	public function export_excel()
	{
		$filter = $this->set_surat_filter();
		$query = $this->Surat_model->get_export_data($filter);
		if(!$query)
            return false;
		
		try
		{
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("suratname");

			$fields = $query->list_fields();
			$type = array();
			$col = 0;
			foreach ($fields as $field)
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				switch ($field){
					default:
						array_push($type, PHPExcel_Cell_DataType::TYPE_STRING);
						break;
				}
				$col++;
			}

			$row = 2;
			foreach($query->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->setValueExplicit($data->$field, $type[$col]);
					$col++;
				}
				$row++;
			}

			// column size
			foreach(range('A','Z') as $columnID) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}
					
			$filename = "suratname ".date('Y-m-d H-i-s').".xlsx";
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');

			//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
		}
		catch(Exception $e)
		{
			//alert the surat.
			var_dump($e->getMessage());
		}
	}

}
